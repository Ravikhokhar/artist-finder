package com.artistfinder.dialog

import android.app.Dialog
import android.content.Context
import android.graphics.drawable.ColorDrawable
import android.view.Window
import android.view.WindowManager
import com.artistfinder.R


class CustomLoaderDialog(context: Context) {
    private var dialog: Dialog? = null

    val isShowing: Boolean?
        get() = dialog!!.isShowing

    init {
        //        dialog = new Dialog(context, R.style.DialogTheme);
        dialog = Dialog(context/*, R.style.DialogTheme*/)
        dialog!!.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog!!.window!!.clearFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND)
        dialog!!.window!!.setBackgroundDrawable(ColorDrawable(android.graphics.Color.TRANSPARENT))
    }

    /**
     * This method use for show progress dialog
     *
     * @param isCancelable set true if you set cancel progressDialog by user event
     */
    fun show(isCancelable: Boolean?) {

        dialog!!.setCancelable(isCancelable!!)
        dialog!!.setContentView(R.layout.progress_layout)
        dialog!!.show()
    }

    fun hide() {
        if (dialog != null) {
            dialog!!.cancel()
            dialog!!.dismiss()
        }
    }
}