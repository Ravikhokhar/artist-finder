package com.artistfinder.dialog


import android.app.Dialog
import android.content.Context
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.View
import android.view.Window
import android.widget.RelativeLayout
import com.artistfinder.R
import kotlinx.android.synthetic.main.dialog_confirmation.*
import java.util.*

abstract class ConfirmationDialog(context: Context) : Dialog(context) {


    override fun onCreate(savedInstanceState: Bundle) {
        super.onCreate(savedInstanceState)
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        Objects.requireNonNull(window).setLayout(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT)
        window!!.setBackgroundDrawable(ColorDrawable(android.graphics.Color.TRANSPARENT))
        setContentView(R.layout.dialog_confirmation)

        tvCancel.setOnClickListener(View.OnClickListener { onNo() })
        tvSend.setOnClickListener(View.OnClickListener { onYes() })
    }


    abstract fun onYes()

    abstract fun onNo()

}
