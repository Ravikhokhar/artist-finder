package com.artistfinder.utils

import android.Manifest
import android.app.Activity
import android.app.AlertDialog
import android.content.DialogInterface
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Build
import androidx.annotation.RequiresApi
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import com.artistfinder.R
import com.artistfinder.listners.OnMultiMediaListener
import com.kbeanie.multipicker.api.*
import com.kbeanie.multipicker.api.callbacks.ImagePickerCallback
import com.kbeanie.multipicker.api.callbacks.VideoPickerCallback
import com.kbeanie.multipicker.api.entity.ChosenImage
import com.kbeanie.multipicker.api.entity.ChosenVideo
import com.vincent.filepicker.Constant
import com.vincent.filepicker.activity.ImagePickActivity
import com.vincent.filepicker.activity.VideoPickActivity
import com.vincent.filepicker.filter.entity.ImageFile
import com.vincent.filepicker.filter.entity.VideoFile

class CameraUtils(
    val activity: Activity,
    val onCameraResult: OnCameraResult?,
    val onMultiMediaListener: OnMultiMediaListener
) :
    ImagePickerCallback, VideoPickerCallback {


    private var cameraPicker: CameraImagePicker? = null

    private var cameraPickerVideoPicker: CameraVideoPicker? = null

    private var imagePicker: ImagePicker? = null

    private var videoPicker: VideoPicker? = null

    private var pickerPath: String? = null

    private var isForImage: Boolean = true
    private var allowMultiple: Boolean = false
    private var isNightTheme: Boolean = false
    private var isMultiple: Boolean = false
    private var imageLimit: Int = 5
    private var videoLimit: Int = 2

    private fun checkPermission() {
        if (ContextCompat.checkSelfPermission(
                activity,
                Manifest.permission.CAMERA
            ) != PackageManager.PERMISSION_GRANTED || ContextCompat.checkSelfPermission(
                activity,
                Manifest.permission.WRITE_EXTERNAL_STORAGE
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(
                    activity,
                    Manifest.permission.CAMERA
                ) || ActivityCompat.shouldShowRequestPermissionRationale(
                    activity,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE
                )
            ) {
                alertPermissionRationale()
            } else {
                requestPermission()
            }
        } else {
            if (isMultiple) {
                if (isForImage) {
                    pickMultiImage(isNightTheme)
                } else {
                    pickMultiVideo(isNightTheme)
                }
            } else {
                if (isForImage) {
                    alertCameraGallery()
                } else {
                    alertVideoCameraGallery()
                }
            }

        }
    }


    private fun pickMultiImage(isNightTheme: Boolean) {
        val intent1 = Intent(activity, ImagePickActivity::class.java)
        intent1.putExtra(ImagePickActivity.IS_NEED_CAMERA, false)
        intent1.putExtra(Constant.MAX_NUMBER, imageLimit)
        intent1.putExtra(Constant.THEME, isNightTheme)
        activity.startActivityForResult(intent1, Constant.REQUEST_CODE_PICK_IMAGE)
    }

    private fun pickMultiVideo(isNightTheme: Boolean) {
        val intent1 = Intent(activity, VideoPickActivity::class.java)
        intent1.putExtra(ImagePickActivity.IS_NEED_CAMERA, false)
        intent1.putExtra(Constant.MAX_NUMBER, videoLimit)
        intent1.putExtra(Constant.THEME, isNightTheme)
        activity.startActivityForResult(intent1, Constant.REQUEST_CODE_PICK_VIDEO)
    }

    fun openCameraGallery(isForImage: Boolean, allowMultiple: Boolean) {
        this.isForImage = isForImage
        this.allowMultiple = allowMultiple
        this.isMultiple = false
        checkPermission()
    }

    fun openMultiMedia(isForImage: Boolean, isNightTheme: Boolean, mediaLimit: Int) {
        this.isForImage = isForImage
        this.isNightTheme = isNightTheme
        this.isMultiple = true

        if (this.isForImage) {
            this.imageLimit = mediaLimit
        }else{
            this.videoLimit = mediaLimit
        }
        checkPermission()
    }

    fun isAllowMultiple(): Boolean {
        return allowMultiple
    }

    private fun requestPermission() {
        ActivityCompat.requestPermissions(
            activity,
            arrayOf(Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE),
            PERMISSION_CODE
        )
    }

    private fun takePicture() {
        cameraPicker = CameraImagePicker(activity)
        cameraPicker!!.shouldGenerateMetadata(true)
        cameraPicker!!.shouldGenerateThumbnails(true)
        cameraPicker!!.setImagePickerCallback(this)
        pickerPath = cameraPicker!!.pickImage()
    }


    private fun takeVideo() {
        cameraPickerVideoPicker = CameraVideoPicker(activity)
        cameraPickerVideoPicker!!.shouldGenerateMetadata(true)
        cameraPickerVideoPicker!!.setVideoPickerCallback(this)
        pickerPath = cameraPickerVideoPicker!!.pickVideo()
    }

    private fun pickImageSingle() {
        imagePicker = ImagePicker(activity)
        imagePicker!!.shouldGenerateMetadata(true)
        imagePicker!!.shouldGenerateThumbnails(true)
        imagePicker!!.setImagePickerCallback(this)
        if (allowMultiple)
            imagePicker!!.allowMultiple()
        imagePicker!!.pickImage()
    }

    private fun pickVideo() {
        videoPicker = VideoPicker(activity)
        videoPicker!!.shouldGenerateMetadata(true)
        videoPicker!!.setVideoPickerCallback(this)
        if (allowMultiple)
            videoPicker!!.allowMultiple()
        videoPicker!!.pickVideo()

    }

    override fun onImagesChosen(list: List<ChosenImage>) {
        onCameraResult?.onSuccess(list)
    }

    override fun onVideosChosen(list: List<ChosenVideo>?) {
        onCameraResult?.onVideoSuccess(list!!)
    }

    override fun onError(s: String) {
        onCameraResult?.onError(s)
    }

    fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == Picker.PICK_IMAGE_DEVICE) {
                if (imagePicker == null) {
                    imagePicker = ImagePicker(activity)
                    imagePicker!!.setImagePickerCallback(this)
                }
                imagePicker!!.submit(data)
            } else if (requestCode == Picker.PICK_IMAGE_CAMERA) {
                if (cameraPicker == null) {
                    cameraPicker = CameraImagePicker(activity)
                    cameraPicker!!.setImagePickerCallback(this)
                    cameraPicker!!.reinitialize(pickerPath)
                }
                cameraPicker!!.submit(data)
            } else if (requestCode == Picker.PICK_VIDEO_DEVICE) {
                if (videoPicker == null) {
                    videoPicker = VideoPicker(activity)
                    videoPicker!!.setVideoPickerCallback(this)
                    videoPicker!!.reinitialize(pickerPath)
                }
                videoPicker!!.submit(data)
            } else if (requestCode == Picker.PICK_VIDEO_CAMERA) {
                if (cameraPickerVideoPicker == null) {
                    cameraPickerVideoPicker = CameraVideoPicker(activity)
                    cameraPickerVideoPicker!!.setVideoPickerCallback(this)
                    cameraPickerVideoPicker!!.reinitialize(pickerPath)
                }
                cameraPickerVideoPicker!!.submit(data)
            } else if (requestCode == Constant.REQUEST_CODE_PICK_IMAGE) {
                if (resultCode == Activity.RESULT_OK) {
                    val imageList: ArrayList<ImageFile> =
                        data!!.getParcelableArrayListExtra(Constant.RESULT_PICK_IMAGE)
                    onMultiMediaListener.onMultiImageListener(imageList)

                }
            } else if (requestCode == Constant.REQUEST_CODE_PICK_VIDEO) {
                if (resultCode == Activity.RESULT_OK) {
                    val videoList: ArrayList<VideoFile> =
                        data!!.getParcelableArrayListExtra(Constant.RESULT_PICK_VIDEO)
                    onMultiMediaListener.onMultiVideoListener(videoList)

                }
            }
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String>,
        grantResults: IntArray
    ) {
        when (requestCode) {
            11 -> if (grantResults.size > 0) {
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED && grantResults[1] == PackageManager.PERMISSION_GRANTED) {
                    if (isMultiple) {
                        if (isForImage) {
                            pickMultiImage(isNightTheme)
                        } else {
                            pickMultiVideo(isNightTheme)
                        }
                    } else {
                        if (isForImage) {
                            alertCameraGallery()
                        } else {
                            alertVideoCameraGallery()
                        }
                    }
                } else {
                    val showRationale_cam =
                        activity.shouldShowRequestPermissionRationale(permissions[0])
                    val showRationale_storage =
                        activity.shouldShowRequestPermissionRationale(permissions[1])
                    if (!showRationale_cam && !showRationale_storage) {
                        alertPermissionFromSetting()
                    }
                }
            }
        }
    }

    private fun alertPermissionFromSetting() {
        val builder = AlertDialog.Builder(activity, R.style.MyDialogTheme)
        builder.setMessage(R.string.msg_camera_storage_setting)
        builder.setPositiveButton(
            R.string.settings,
            DialogInterface.OnClickListener { dialog, which ->
                Utils.openApplicationSettings(
                    activity
                )
            })
        builder.setNegativeButton(
            R.string.cancel,
            DialogInterface.OnClickListener { dialog, which -> dialog.dismiss() })
        builder.show()
    }

    private fun alertPermissionRationale() {
        val builder = AlertDialog.Builder(activity, R.style.MyDialogTheme)
        builder.setMessage(R.string.msg_permission_rationale)
        builder.setPositiveButton(
            R.string.ok,
            DialogInterface.OnClickListener { dialog, which -> requestPermission() })
        builder.setNegativeButton(
            R.string.cancel,
            DialogInterface.OnClickListener { dialog, which -> dialog.dismiss() })
        builder.show()
    }


    private fun alertCameraGallery() {
        val colors = arrayOf<CharSequence>(
            activity.getString(R.string.take_photo),
            activity.getString(R.string.choose_from_gallery)
        )

        val builder = AlertDialog.Builder(activity, R.style.MyDialogTheme)
        builder.setTitle(R.string.choose_image)
        builder.setItems(colors) { dialog, which ->
            // the user clicked on colors[which]
            if (which == 0) {
                takePicture()
            } else {
                pickImageSingle()
            }
        }
        builder.setNegativeButton(
            R.string.cancel,
            DialogInterface.OnClickListener { dialog, which -> dialog.dismiss() })
        builder.show()
    }

    private fun alertVideoCameraGallery() {
        val colors = arrayOf<CharSequence>(
            activity.getString(R.string.take_video),
            activity.getString(R.string.choose_from_gallery)
        )

        val builder = AlertDialog.Builder(activity, R.style.MyDialogTheme)
        builder.setTitle(R.string.choose_video)
        builder.setItems(colors) { dialog, which ->
            // the user clicked on colors[which]
            if (which == 0) {
                takeVideo()
            } else {
                pickVideo()
            }
        }
        builder.setNegativeButton(
            R.string.cancel,
            DialogInterface.OnClickListener { dialog, which -> dialog.dismiss() })
        builder.show()
    }


    interface OnCameraResult {
        fun onSuccess(images: List<ChosenImage>)

        fun onVideoSuccess(images: List<ChosenVideo>)

        fun onError(error: String)
    }

    companion object {

        val PERMISSION_CODE = 11
    }
}
