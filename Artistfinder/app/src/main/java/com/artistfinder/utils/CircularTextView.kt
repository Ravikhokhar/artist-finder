package com.artistfinder.utils

import android.content.Context
import android.graphics.Canvas
import android.graphics.Paint
import android.util.AttributeSet
import android.widget.TextView
import androidx.core.content.ContextCompat
import com.artistfinder.R


class CircularTextView : TextView {

    constructor(
        context: Context?,
        attrs: AttributeSet?,
        defStyleAttr: Int,
        defStyleRes: Int
    ) : super(context, attrs, defStyleAttr, defStyleRes)

    constructor(
        context: Context?,
        attrs: AttributeSet?,
        defStyleAttr: Int
    ) : super(context, attrs, defStyleAttr)



    constructor(
        context: Context?
    ) : super(context)

    constructor(
        context: Context?,
        attrs: AttributeSet?
    ) : super(context, attrs)

    var isCircle: Boolean = false
    val strokeWidth: Int = 5

    override fun draw(canvas: Canvas?) {
        val circlePaint = Paint()
        circlePaint.setColor(ContextCompat.getColor(context,R.color.color_yellow))
        //circlePaint.setFlags(Paint.ANTI_ALIAS_FLAG)

        val strokePaint = Paint()
        strokePaint.setColor(ContextCompat.getColor(context,R.color.transparent))
        //strokePaint.setFlags(Paint.ANTI_ALIAS_FLAG)

        val h = this.height
        val w = this.width

        val diameter = if (h > w) h else w
        val radius = diameter / 2

        this.height = diameter
        this.width = diameter

        if (isCircle) {
            canvas?.drawCircle(
                (diameter / 2).toFloat(),
                (diameter / 2).toFloat(),
                radius.toFloat(),
                strokePaint
            )
            canvas?.drawCircle(
                (diameter / 2).toFloat(), (diameter / 2).toFloat(),
                (radius - strokeWidth).toFloat(), circlePaint
            )
        }


        super.draw(canvas)
    }

    fun setRoundBackground(boolean: Boolean) {
        isCircle = boolean
    }

}