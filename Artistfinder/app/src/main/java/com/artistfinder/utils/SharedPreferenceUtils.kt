package com.artistfinder.utils

import android.content.Context
import android.content.SharedPreferences
import com.google.gson.GsonBuilder


class SharedPreferenceUtils private constructor(protected var mContext: Context) {
    private val mSharedPreferences: SharedPreferences
    private val mSharedPreferencesEditor: SharedPreferences.Editor?
    private val gson = GsonBuilder().create()
    init {
        mSharedPreferences = mContext.getSharedPreferences("LineBullyPreferences", Context.MODE_PRIVATE)
        mSharedPreferencesEditor = mSharedPreferences.edit()
    }

    /**
     * Stores String value in preference
     *
     * @param key   key of preference
     * @param value value for that key
     */
    fun setValue(key: String, value: String) {
        mSharedPreferencesEditor!!.putString(key, value)
        mSharedPreferencesEditor.commit()
    }

    /**
     * Stores int value in preference
     *
     * @param key   key of preference
     * @param value value for that key
     */
    fun setValue(key: String, value: Int) {
        mSharedPreferencesEditor!!.putInt(key, value)
        mSharedPreferencesEditor.commit()
    }

    /**
     * Stores Double value in String format in preference
     *
     * @param key   key of preference
     * @param value value for that key
     */
    fun setValue(key: String, value: Double) {
        setValue(key, java.lang.Double.toString(value))
    }

    /**
     * Stores long value in preference
     *
     * @param key   key of preference
     * @param value value for that key
     */
    fun setValue(key: String, value: Long) {
        mSharedPreferencesEditor!!.putLong(key, value)
        mSharedPreferencesEditor.commit()
    }

    /**
     * Stores boolean value in preference
     *
     * @param key   key of preference
     * @param value value for that key
     */
    fun setValue(key: String, value: Boolean) {
        mSharedPreferencesEditor!!.putBoolean(key, value)
        mSharedPreferencesEditor.commit()
    }
    /**
     * Saves object into the Preferences.
     * Only the fields are stored. Methods, Inner classes, Nested classes and inner interfaces are not stored.
     **/
    fun <T> setObject(key: String, y: T) {
        //Convert object to JSON String.
        val inString = gson.toJson(y)
        //Save that String in SharedPreferences
        mSharedPreferencesEditor!!.putString(key, inString).commit()
    }

    /**
     * Saves collection of objects into the Preferences.
     * Only the fields are stored. Methods, Inner classes, Nested classes and inner interfaces are not stored.
     **/

    fun <T> getObject(key: String, c: Class<T>): T? {
        //We read JSON String which was saved.
        val value = mSharedPreferences.getString(key, null)
        if (value != null) {
            //JSON String was found which means object can be read.
            //We convert this JSON String to model object. Parameter "c" (of
           // type Class<T>" is used to cast.
            return gson.fromJson(value, c)
        } else {
            //No JSON String with this key was found which means key is invalid or object was not saved.
            throw IllegalArgumentException("No object with key: $key was saved")
        }
    }

    /**
     * Retrieves String value from preference
     *
     * @param key          key of preference
     * @param defaultValue default value if no key found
     */
    fun getStringValue(key: String, defaultValue: String): String? {
        return mSharedPreferences.getString(key, defaultValue)
    }

    /**
     * Retrieves int value from preference
     *
     * @param key          key of preference
     * @param defaultValue default value if no key found
     */
    fun getIntValue(key: String, defaultValue: Int): Int {
        return mSharedPreferences.getInt(key, defaultValue)
    }

    /**
     * Retrieves long value from preference
     *
     * @param key          key of preference
     * @param defaultValue default value if no key found
     */
    fun getLongValue(key: String, defaultValue: Long): Long {
        return mSharedPreferences.getLong(key, defaultValue)
    }

    /**
     * Retrieves boolean value from preference
     *
     * @param keyFlag      key of preference
     * @param defaultValue default value if no key found
     */
    fun getBoolanValue(keyFlag: String, defaultValue: Boolean): Boolean {
        return mSharedPreferences.getBoolean(keyFlag, defaultValue)
    }

    /**
     * Removes key from preference
     *
     * @param key key of preference that is to be deleted
     */
    fun removeKey(key: String) {
        if (mSharedPreferencesEditor != null) {
            mSharedPreferencesEditor.remove(key)
            mSharedPreferencesEditor.commit()
        }
    }


    /**
     * Clears all the preferences stored
     */
    fun clear() {

//        val email = SharedPreferenceUtils.getInstance(mContext).getStringValue(Constant.IS_REMEMBER, "")
        mSharedPreferencesEditor!!.clear().commit()
//        SharedPreferenceUtils.getInstance(mContext).setValue(Constant.IS_REMEMBER, email!!)
    }

    companion object {

        private var mSharedPreferenceUtils: SharedPreferenceUtils? = null

        /**
         * Creates single instance of SharedPreferenceUtils
         *
         * @param context context of Activity or Service
         * @return Returns instance of SharedPreferenceUtils
         */
        @Synchronized
        fun getInstance(context: Context): SharedPreferenceUtils {

            if (mSharedPreferenceUtils == null) {
                mSharedPreferenceUtils = SharedPreferenceUtils(context.applicationContext)
            }
            return mSharedPreferenceUtils as SharedPreferenceUtils
        }
    }
}
