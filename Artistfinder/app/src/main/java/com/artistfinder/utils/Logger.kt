package com.artistfinder.utils

import android.util.Log


object Logger {
    fun print(Tag: String, value: String) {
        Log.d(Tag, value)
    }

    fun print(value: String) {
        Log.d(">>", value)
    }

}
