package com.artistfinder.utils

import android.content.Context
import android.location.Address
import android.os.Bundle
import android.location.Geocoder
import android.os.Handler
import android.os.Message
import android.util.Log
import java.io.IOException
import java.util.*


class GeocodingLocation {

    private val TAG = "GeocodingLocation"

    fun getAddressFromLocation(
        locationAddress: String,
        context: Context, handler: Handler
    ) {
        val thread = object : Thread() {
            override fun run() {
                val geocoder = Geocoder(context, Locale.getDefault())
                var result: String? = null
                var lat : Double = 0.0
                var long : Double = 0.0
                try {
                    val addressList = geocoder.getFromLocationName(locationAddress, 1)
                    if (addressList != null && addressList.size > 0) {
                        val address = addressList[0] as Address
                        lat = address.latitude
                        long = address.longitude
//                        val sb = StringBuilder()
//                        sb.append(address.getLatitude()).append(",")
//                        sb.append(address.getLongitude()).append(",")
//                        result = sb.toString()
                    }
                } catch (e: IOException) {
                    Log.e(TAG, "Unable to connect to Geocoder", e)
                } finally {
                    val message = Message.obtain()
                    message.setTarget(handler)
                    message.what = 1
                    val bundle = Bundle()
                    bundle.putDouble("latittude",lat)
                    bundle.putDouble("longitude",long)
                    message.setData(bundle)

                    /*if (result != null) {
                        message.what = 1
                        val bundle = Bundle()

                        bundle.putString("address", result)
                        message.setData(bundle)
                    } else {
                        message.what = 1
                        val bundle = Bundle()
                        result = "Address: " + locationAddress +
                                "\n Unable to get Latitude and Longitude for this address location."
                        bundle.putString("address", result)
                        message.setData(bundle)
                    }*/
                    message.sendToTarget()
                }
            }
        }
        thread.start()
    }
}