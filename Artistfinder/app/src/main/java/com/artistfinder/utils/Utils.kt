package com.artistfinder.utils

import android.Manifest
import android.app.Activity
import android.app.AlertDialog
import android.app.DatePickerDialog
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.content.res.Configuration
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Point
import android.media.MediaPlayer
import android.net.ConnectivityManager
import android.net.Uri
import android.os.Build
import android.os.Environment
import android.provider.Settings

import android.util.Base64
import android.util.Log
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import android.widget.TextView
import android.widget.Toast
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.core.content.res.ResourcesCompat
import com.artistfinder.BuildConfig
import com.artistfinder.R
import com.artistfinder.global.Constant
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody

import java.io.ByteArrayOutputStream
import java.io.File
import java.io.FileInputStream
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.Calendar
import java.util.Date
import java.util.TimeZone


class Utils {


    /**
     * Method for show DatePicker
     *
     * @param mContext
     * @param datePickerListener instance of DatePickerListener
     */
    fun showDatePicker(mContext: Context, datePickerListener: DatePickerDialog.OnDateSetListener) {
        val cal = Calendar.getInstance(TimeZone.getDefault())
        val datePicker = DatePickerDialog(
            mContext,
            datePickerListener,
            cal.get(Calendar.YEAR),
            cal.get(Calendar.MONTH),
            cal.get(Calendar.DAY_OF_MONTH)
        )
        datePicker.setCancelable(false)
        datePicker.datePicker.calendarViewShown = false
        datePicker.datePicker.spinnersShown = true
        datePicker.show()
    }


    /**
     * Hide key board from current focus in activity.
     *
     * @param activity Current activity object.
     */
    fun hideKeyBoardFromCurrentFocus(activity: Activity, view: View) {
        try {
            val imm = activity.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            imm.hideSoftInputFromWindow(view.windowToken, InputMethodManager.RESULT_UNCHANGED_SHOWN)
        } catch (e: Exception) {
            e.printStackTrace()
        }

    }

    private fun getStringResourceByName(mContext: Context, mString: String): String {
        val packageName = mContext.packageName
        val resId = mContext.resources.getIdentifier(mString, "string", packageName)
        return mContext.getString(resId)
    }

    companion object {

        val MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE = 123
        val CAMERA_REQUEST_CODE = 1
        val LOCATION_CODE = 10
        val LOCATION_PERMISSION_CODE = 22
        val LOCATION_SETTING_REQUEST_CODE = 124
        val OPEN_SETTINGS_FOR_PERMISSION = 125

        /**
         * Method for check Internet connection, Is enable or not
         *
         * @param cContext instance of calling area context
         * @return Return true if internet is working else return false.
         */
        fun isOnline(cContext: Context): Boolean {
            try {
                val cm = cContext.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
                val netInfo = cm.activeNetworkInfo
                if (netInfo != null && netInfo.isConnectedOrConnecting) {
                    return true
                }
            } catch (ex: Exception) {
                ex.printStackTrace()
            }

            return false
        }


        /**
         * Method for Get current TimeStamp
         *
         * @return
         */
        val timeStamp: String
            get() = (System.currentTimeMillis() / 1000).toString()

        fun checkLocationPermission(activity: Activity): Boolean {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                if (ContextCompat.checkSelfPermission(
                        activity,
                        Manifest.permission.ACCESS_FINE_LOCATION
                    ) != PackageManager.PERMISSION_GRANTED
                ) {

                    if (ActivityCompat.shouldShowRequestPermissionRationale(
                            activity,
                            Manifest.permission.ACCESS_FINE_LOCATION
                        )
                    ) {
                        activity.requestPermissions(
                            arrayOf(Manifest.permission.ACCESS_FINE_LOCATION),
                            LOCATION_PERMISSION_CODE
                        )
                    } else {
                        activity.requestPermissions(
                            arrayOf(Manifest.permission.ACCESS_FINE_LOCATION),
                            LOCATION_PERMISSION_CODE
                        )
                    }
                    return false
                } else {
                    return true
                }
            } else {
                return true
            }
        }


        fun getLocationMode(activity: Activity): Int {
            var mode = 1
            try {
                mode = Settings.Secure.getInt(activity.contentResolver, Settings.Secure.LOCATION_MODE)
            } catch (e: Settings.SettingNotFoundException) {
                e.printStackTrace()
            }

            return mode
        }

        /*fun showAlertForPermission(permission: String, activity: Activity) {
            val alertDialog = AlertDialog.Builder(activity, R.style.MyDialogTheme)
                .setMessage("Please goto setting and grant $permission").setPositiveButton("Yes") { dialog, which ->
                val intent = Intent()
                intent.action = Settings.ACTION_APPLICATION_DETAILS_SETTINGS
                val uri = Uri.fromParts("package", activity.packageName, null)
                intent.data = uri
                activity.startActivityForResult(intent, OPEN_SETTINGS_FOR_PERMISSION) //code 333
            }.setNegativeButton("No") { dialog, which ->
                Toast.makeText(
                    activity,
                    "Without permission you can not use sharing functionality.",
                    Toast.LENGTH_SHORT
                ).show()
            }.create()
            alertDialog.show()

        }*/

        /**
         * Method for check is Tablet or not
         *
         * @param context
         * @return
         */
        fun isTablet(context: Context): Boolean {
            val xlarge = context.resources.configuration.screenLayout and Configuration.SCREENLAYOUT_SIZE_MASK == 4
            val large =
                context.resources.configuration.screenLayout and Configuration.SCREENLAYOUT_SIZE_MASK == Configuration.SCREENLAYOUT_SIZE_LARGE
            return xlarge || large
        }

        /*fun setFontTabLayout(context: Context, mCustomFontTab: TabLayout, resIdfont: Int) {

            val vg = mCustomFontTab.getChildAt(0) as ViewGroup
            val tabsCount = vg.childCount

            for (j in 0 until tabsCount) {
                val vgTab = vg.getChildAt(j) as ViewGroup

                val tabChildsCount = vgTab.childCount

                for (i in 0 until tabChildsCount) {
                    val tabViewChild = vgTab.getChildAt(i)
                    if (tabViewChild is TextView) {
                        //((TextView) tabViewChild).setTypeface(Typeface.createFromAsset(getAssets(), "Nosifer-Regular.ttf"));
                        val typeface = ResourcesCompat.getFont(context, resIdfont)
                        tabViewChild.typeface = typeface
                    }
                }
            }
        }*/

        fun prepareFilePart(context: Context, partName: String, file: File): MultipartBody.Part {
            // https://github.com/iPaulPro/aFileChooser/blob/master/aFileChooser/src/com/ipaulpro/afilechooser/utils/FileUtils.java
            // use the FileUtils to get the actual file by uri
            //        File file = FileUtils.getFile(this, fileUri);

            // create RequestBody instance from file
            //                        MediaType.parse(context.getContentResolver().getType(Uri.fromFile(file))),
            val requestFile = RequestBody.create(
                MediaType.parse("multipart/form-data"),
                file
            )
            // MultipartBody.Part is used to send also the actual file name
            return MultipartBody.Part.createFormData(partName, file.name, requestFile)
        }


        fun getDate(milliSeconds: Long, dateFormat: String): String {
            // Create a DateFormatter object for displaying date in specified format.
            val formatter = SimpleDateFormat(dateFormat)

            // Create a calendar object that will convert the date and time value in milliseconds to date.
            val calendar = Calendar.getInstance()
            calendar.timeInMillis = milliSeconds
            return formatter.format(calendar.time)
        }


        fun get12HourFormate(_24HourTime: String?): String {
            try {
                if (_24HourTime == null) {
                    return ""
                }

                val _24HourSDF = SimpleDateFormat("HH:mm:ss")
                val _12HourSDF = SimpleDateFormat("hh:mm a")
                val _24HourDt = _24HourSDF.parse(_24HourTime)
                println(_12HourSDF.format(_24HourDt))
                return _12HourSDF.format(_24HourDt)
            } catch (e: Exception) {
                e.printStackTrace()
            }

            return ""
        }


        fun get24HourFormate(_12HourTime: String): String {
            try {

                val _12HourSDF = SimpleDateFormat("hh:mm a")
                val _24HourSDF = SimpleDateFormat("HH:mm:ss")
                val _12HourDt = _12HourSDF.parse(_12HourTime)
                println(_24HourSDF.format(_12HourDt))
                return _24HourSDF.format(_12HourDt)
            } catch (e: Exception) {
                e.printStackTrace()
            }

            return ""
        }

        fun getMinutesFromHours(hours: Int, min: Int): Int {
            var totalHours = 0
            totalHours = hours * 60 + min
            return totalHours
        }


        fun getHourMinute(_24HourTime: String, i: Int): String {

            val seperated = _24HourTime.split(":".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()

            return seperated[i]
        }


        fun getConvertedDate(input: String): String {
            val inputFormant = SimpleDateFormat("yyyy-MM-dd")
            val outputFormate = SimpleDateFormat("dd MMMM,yyyy")

            var d: Date? = null
            try {
                d = inputFormant.parse(input)
            } catch (e: Exception) {
                e.printStackTrace()
            }

            val formatted = outputFormate.format(d)

            return formatted
        }

        /**
         * Method for get Display width
         *
         * @param mActivity
         * @return
         */
        fun getDisplayWidth(mActivity: Activity): Int {
            val display = mActivity.windowManager.defaultDisplay
            val size = Point()
            display.getSize(size)
            return size.x
        }

        fun getBase64String(f: File): String {
            /*InputStream inputStream = null;
        String encodedFile = "", lastVal;
        try {
            inputStream = new FileInputStream(f.getAbsolutePath());

            byte[] buffer = new byte[10240];//specify the size to allow
            int bytesRead;
            ByteArrayOutputStream output = new ByteArrayOutputStream();
            Base64OutputStream output64 = new Base64OutputStream(output, Base64.DEFAULT);

            while ((bytesRead = inputStream.read(buffer)) != -1) {
                output64.write(buffer, 0, bytesRead);
            }
            output64.close();
            encodedFile = output.toString();
        } catch (Exception e) {
            e.printStackTrace();
        }
        lastVal = encodedFile;
        Logger.print(lastVal);
        return lastVal;*/
            /*ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, stream);
        byte[] image = stream.toByteArray();
        String encodedImage = Base64.encode(image, Base64.DEFAULT);*/

            val bitmap = getResizedBitmap(BitmapFactory.decodeFile(f.absolutePath), 200)
            val byteArrayOS = ByteArrayOutputStream()
            bitmap.compress(Bitmap.CompressFormat.JPEG, 50, byteArrayOS)
            val encodeString = Base64.encodeToString(byteArrayOS.toByteArray(), Base64.DEFAULT)
            Logger.print("IMAGE==$encodeString")
            return encodeString

        }


        fun getResizedBitmap(image: Bitmap, maxSize: Int): Bitmap {
            var width = image.width
            var height = image.height

            val bitmapRatio = width.toFloat() / height.toFloat()
            if (bitmapRatio > 1) {
                width = maxSize
                height = (width / bitmapRatio).toInt()
            } else {
                height = maxSize
                width = (height * bitmapRatio).toInt()
            }
            return Bitmap.createScaledBitmap(image, width, height, true)
        }

        /**
         * Method for get Display Height
         *
         * @param mActivity
         * @return
         */
        fun getDisplayHeight(mActivity: Activity): Int {
            val display = mActivity.windowManager.defaultDisplay
            val size = Point()
            display.getSize(size)
            return size.y
        }

        fun checkString(string: String?): Boolean {
            try {
                return if (string == null || string.trim { it <= ' ' } == "" || string.equals(
                        "null",
                        ignoreCase = true
                    ) || string.trim { it <= ' ' }.length < 0) {
                    false
                } else {
                    true
                }
            } catch (e: Exception) {
                return false
            }

        }

        fun getDateFromTimeStamp(requiredFormat: String, value: Long): String {
            var string = ""
            try {
                val date = Date(value)
                val reqSimpleDateFormat = SimpleDateFormat(requiredFormat)
                string = reqSimpleDateFormat.format(date)
            } catch (e: Exception) {
                e.printStackTrace()
            }

            return string
        }

        fun getTimeStampFromDate(dateFormat: String, date: String): Long {
            var timestmp: Long = 0
            try {
                val reqSimpleDateFormat = SimpleDateFormat(dateFormat)
                val date1 = reqSimpleDateFormat.parse(date)
                timestmp = date1.time
            } catch (e: ParseException) {
                e.printStackTrace()
            }

            return timestmp
        }

        fun getAppVersionName(context: Context): String {
            var versionName = ""

            try {
                versionName = context.packageManager.getPackageInfo(context.packageName, 0).versionName
            } catch (ex: Exception) {
                Logger.print("Common Code: ", ex.message!!)
            }

            return versionName
        }


        fun isEmailValid(email: String?): Boolean {
            return email != null && android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches()
        }


        fun printToastAlertLong(context: Context, string: String) {
            Toast.makeText(context, "" + string, Toast.LENGTH_LONG).show()
        }

        fun printToastAlertShort(context: Context, string: String) {
            Toast.makeText(context, "" + string, Toast.LENGTH_SHORT).show()
        }

        fun is6digitPass(pass: String): Boolean {
            return if (pass.length < 6) {
                true
            } else false
        }

        fun getDirectoryPath(context: Context): String {
            try {
                val fileDir = File(Environment.getExternalStorageDirectory().path, context.packageName)
                //  fileDir.mkdirs();
                //            fileDir.createNewFile();
                //   Log.d(Constant.TAG, "DIR=" + fileDir.getPath());

                if (!fileDir.exists()) {
                    //  fileDir.mkdirs();
                    Logger.print("file", fileDir.mkdirs().toString() + "")
                }


                Log.d("mkd", fileDir.path)
                return fileDir.path
            } catch (e: Exception) {
                e.printStackTrace()
                Logger.print("error", e.message!!)
            }

            return ""
        }


        fun checkPermission(context: Context): Boolean {
            val currentAPIVersion = Build.VERSION.SDK_INT
            if (currentAPIVersion >= Build.VERSION_CODES.M) {
                if (ContextCompat.checkSelfPermission(
                        context,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE
                    ) != PackageManager.PERMISSION_GRANTED
                ) {

                    ActivityCompat.requestPermissions(
                        context as Activity,
                        arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE),
                        MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE
                    )

                    return false
                } else {
                    return true
                }
            } else {
                return true
            }

        }

        fun checkPermission(context: Context, permission: String): Boolean {
            return if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && context.checkSelfPermission(permission) != PackageManager.PERMISSION_GRANTED) {
                false
            } else {
                true
            }
        }

        /**
         * Method for ask permission
         *
         * @param activity    / Instance of activity for ask permission
         * @param permissions name of permission which you want to ask
         */
        fun askPermissions(activity: Activity, permissions: Array<String>) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                activity.requestPermissions(permissions, 11)
            }
        }


        fun checkPermissionForCamera(context: Context): Boolean {
            val currentAPIVersion = Build.VERSION.SDK_INT
            if (currentAPIVersion >= Build.VERSION_CODES.M) {

                if (ContextCompat.checkSelfPermission(
                        context,
                        Manifest.permission.CAMERA
                    ) != PackageManager.PERMISSION_GRANTED
                ) {

                    ActivityCompat.requestPermissions(
                        context as Activity,
                        arrayOf(Manifest.permission.CAMERA),
                        CAMERA_REQUEST_CODE
                    )

                    return false
                } else {

                    return true
                }
            } else {
                return true
            }

        }


        fun checkPermissionForLocation(context: Context): Boolean {
            Logger.print("locPermission")
            val currentAPIVersion = Build.VERSION.SDK_INT
            if (currentAPIVersion >= Build.VERSION_CODES.M) {

                if (ContextCompat.checkSelfPermission(
                        context,
                        Manifest.permission.ACCESS_FINE_LOCATION
                    ) != PackageManager.PERMISSION_GRANTED
                ) {

                    ActivityCompat.requestPermissions(
                        context as Activity,
                        arrayOf(Manifest.permission.ACCESS_FINE_LOCATION),
                        LOCATION_CODE
                    )
                    return false
                } else {
                    return true
                }
            } else {
                return true
            }

        }

        fun getLengthOfVideo(context: Context, path: String): Double {
            /* MediaMetadataRetriever retriever = new MediaMetadataRetriever();
//use one of overloaded setDataSource() functions to set your data source
        retriever.setDataSource(context, Uri.fromFile(new File(path)));
        String time = retriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_DURATION);
        long timeInMillisec = Long.parseLong(time);
        retriever.release();
        //convert into second
        Logger.print("media time==" + timeInMillisec + "");
        return timeInMillisec;*/

            val mp = MediaPlayer.create(context, Uri.fromFile(File(path)))
            val duration = mp.duration
            mp.release()
            Logger.print("media time==$duration")
            return duration.toDouble()
        }

        fun openApplicationSettings(activity: Activity) {
            val intent = Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS)
            val uri = Uri.fromParts("package", BuildConfig.APPLICATION_ID, null)
            intent.data = uri
            activity.startActivityForResult(intent, 0)
        }

        fun formatToYesterdayOrToday(date: String): String {
            try {
                val dateTime = SimpleDateFormat(Constant.FORMAT5).parse(date)
                val calendar = Calendar.getInstance()
                calendar.time = dateTime
                val today = Calendar.getInstance()
                val yesterday = Calendar.getInstance()
                yesterday.add(Calendar.DATE, -1)
                val timeFormatter = SimpleDateFormat("hh:mma")

                return if (calendar.get(Calendar.YEAR) == today.get(Calendar.YEAR) && calendar.get(Calendar.DAY_OF_YEAR) == today.get(
                        Calendar.DAY_OF_YEAR
                    )
                ) {
                    "Today " /*+ timeFormatter.format(dateTime)*/
                } else if (calendar.get(Calendar.YEAR) == yesterday.get(Calendar.YEAR) && calendar.get(Calendar.DAY_OF_YEAR) == yesterday.get(
                        Calendar.DAY_OF_YEAR
                    )
                ) {
                    "Yesterday " /*+ timeFormatter.format(dateTime)*/
                } else {
                    date
                }
            } catch (e: Exception) {
                e.printStackTrace()
            }

            return date
        }

        fun getExtensionFromFilePath(s: String): String {
            val file = s.substring(s.lastIndexOf("/"))
            val extension = file.substring(file.indexOf(".")) // .tar.gz
            return extension
        }

        fun getByteArrayFromFile(path: String): ByteArray {
            val file = File(path)
            //init array with file length
            val bytesArray = ByteArray(file.length().toInt())

            var fis: FileInputStream? = null
            try {
                fis = FileInputStream(file)
                fis.read(bytesArray) //read file into bytes[]
                fis.close()

            } catch (e: Exception) {
                e.printStackTrace()
            }

            return bytesArray
        }
        fun checkStringContainURL(string: String) : Boolean {
            if (string.startsWith("http://")
                || string.startsWith("https://")
                || string.startsWith("ftp://")){
                return true
            }
            return false
        }

    }
}
