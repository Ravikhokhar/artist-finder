package com.artistfinder.utils

import android.app.Application
import android.content.Context
import android.os.StrictMode
import androidx.appcompat.app.AppCompatDelegate
import com.artistfinder.global.Constant
import com.facebook.drawee.backends.pipeline.Fresco

class MyApplication : Application() {

    private var isNightModeEnabled = false
    override fun onCreate() {
        super.onCreate()
        context = applicationContext
        // Fabric.with(this, new Crashlytics());
        // to avoid nogut camera crash
        Fresco.initialize(this)
        val builder = StrictMode.VmPolicy.Builder()
        StrictMode.setVmPolicy(builder.build())
        isNightModeEnabled = SharedPreferenceUtils.getInstance(this).getBoolanValue(Constant.IS_DARK_MODE,false)
        if(isNightModeEnabled){
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES);
        }else{
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);
        }
    }


    fun isNightModeEnabled(): Boolean {
        return isNightModeEnabled
    }

    fun setIsNightModeEnabled(isNightModeEnabled: Boolean) {
        this.isNightModeEnabled = isNightModeEnabled
    }

    companion object {

        lateinit var context: Context
    }
}
