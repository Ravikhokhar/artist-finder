package com.artistfinder.activity

import android.content.Intent
import android.os.Bundle
import android.provider.Settings
import android.util.Log
import android.view.View
import com.artistfinder.R
import com.artistfinder.baseclass.BaseActivity
import com.artistfinder.dialog.CustomLoaderDialog
import com.artistfinder.global.Constant
import com.artistfinder.model.UserInfo
import com.artistfinder.model.UserProfile
import com.artistfinder.retrofitclient.RetroClient2
import com.artistfinder.utils.Logger
import com.artistfinder.utils.SharedPreferenceUtils
import com.artistfinder.utils.Utils
import kotlinx.android.synthetic.main.activity_verification.*
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.File
import kotlin.random.Random

class VerificationActivity : BaseActivity() {

    var data: UserProfile? = null
    private var loader: CustomLoaderDialog? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_verification)
        overridePendingTransition(R.anim.enter, R.anim.exit)
//        if (intent.hasExtra(Constant.DATA)) {
            //data = intent.getParcelableExtra(Constant.DATA)
//        }
        data = SharedPreferenceUtils.getInstance(this).getObject(Constant.SIGNUP_DATA,UserProfile::class.java)
        setListners()
        Log.e("Code","Otp -->  "+data!!.otp)
    }

    private fun setListners() {
        loader = CustomLoaderDialog(this)

        ivBack.setOnClickListener(this)
        txtResend.setOnClickListener(this)
        btnVerifyNow.setOnClickListener(this)
    }

    override fun onClick(v: View?) {
        super.onClick(v)
        when (v!!.id) {
            R.id.ivBack -> {
                finish()
            }

            R.id.txtResend -> {
                callForResendOtp()
            }

            R.id.btnVerifyNow -> {

                if(edCode!!.text.toString().length <= 0){
                    printToastAlert(this,"Please enter otp")
                }else if(!edCode!!.text.toString().equals(data!!.otp)){
                    edCode.setText("")
                    printToastAlert(this,"Please enter valid otp")
                }else{
                    callForSignUp()
                }

            }
        }
    }



    private fun callForSignUp() {

        loader!!.show(true)
        val map = HashMap<String, RequestBody>()

        val email = RequestBody.create(MultipartBody.FORM, data!!.email)
        val password = RequestBody.create(MultipartBody.FORM, data!!.password)
        val name = RequestBody.create(MultipartBody.FORM, data!!.name)
        val username = RequestBody.create(MultipartBody.FORM, data!!.userName)
        val gender = RequestBody.create(MultipartBody.FORM, "")
        val country = RequestBody.create(MultipartBody.FORM, data!!.country)
        val city = RequestBody.create(MultipartBody.FORM, data!!.city)
        val address = RequestBody.create(MultipartBody.FORM,data!!.address)
        val latlong = RequestBody.create(MultipartBody.FORM, "${data!!.lattitude},${data!!.longitude}")
        val phone = RequestBody.create(MultipartBody.FORM, data!!.mobileNumber)


        /*map["email"] = txtEmail.text.toString()
        map["password"] = txtCreatePassword.text.toString()
        map["name"] = txtname.text.toString()
        map["username"] = txtUsername.text.toString()
        map["gender"] = ""
        map["country"] = txtCountry.text.toString()
        map["city"] = txtCity.text.toString()
        map["address"] = txtAddress.text.toString()
        map["latlong"] = ""
        map["artistType"] = "2"
        map["phone"] = txtNumber.text.toString()*/
        var body: MultipartBody.Part? = null
        if (data!!.profileUrl != null) {

            val selectedFile : File = File(data!!.profileUrl)
            val ext = Utils!!.getExtensionFromFilePath(selectedFile!!.path)

            System.out.println("file name$ext")

            val reqFile = RequestBody.create(MediaType.parse("image/*"), selectedFile)
            body = MultipartBody.Part.createFormData(
                "profileImg",
                selectedFile!!.getName(),
                reqFile
            )

        }

        val artistType = RequestBody.create(MultipartBody.FORM, this.data!!.artistType)
        val availableSlot = RequestBody.create(MultipartBody.FORM, this.data!!.availableSlot)
        val other = RequestBody.create(MultipartBody.FORM, this.data!!.other)
        val otherArtist = RequestBody.create(MultipartBody.FORM, this.data!!.otherArtist)
        val deviceType = RequestBody.create(MultipartBody.FORM, Constant.DEVICE_TYPE)
        val deviceID = RequestBody.create(MultipartBody.FORM, Settings.Secure.getString(contentResolver, Settings.Secure.ANDROID_ID))
        val fcmToken = RequestBody.create(MultipartBody.FORM, SharedPreferenceUtils.getInstance(this).getStringValue(Constant.DEVICETOKEN,"").toString())

        /*callNewAPI(
           Constant.REGISTRATION,
           this,
           Constant.POST_API,
           map,
           true,
           false,
           Constant.REGISTRATION,
           body,
           this
       )*/


        var call: Call<String>? = null

        call = RetroClient2.getClient(Constant.BASE_URL)
            ?.callForSignUp(
                email = email,
                password = password,
                name = name,
                username = username,
                gender = gender,
                country = country,
                city = city,
                address = address,
                latlong = latlong,
                artistType = artistType,
                availableSlot = availableSlot,
                phone = phone,
                files = body!!,
                other = other,
                otherArtist = otherArtist,
                deviceType = deviceType,
                deviceID = deviceID,
                fcmToken = fcmToken
            )



        call!!.enqueue(object : Callback<String> {
            override fun onResponse(call: Call<String>, response: Response<String>) {
                Logger.print(">>", "response code --->> " + response.code())
                Logger.print(">>", "response bofu --->> " + response.body())
                loader!!.hide()
                try {
                    if (response.code() == 200) {


                        try {
                            val commonResponse = getCommonResponse<UserInfo>(response.body().toString())
                            val userInfo = commonResponse!!.result
                            val code = commonResponse.success
                            printToastAlert(this@VerificationActivity, commonResponse.message)
                            if (code == 1) {

                                SharedPreferenceUtils.getInstance(this@VerificationActivity)
                                    .setObject(Constant.LOGINUSERDATA, userInfo)
                                SharedPreferenceUtils.getInstance(this@VerificationActivity)
                                    .setValue(Constant.IS_USER_STATUS, Constant.STATUS_LOGIN_SUCCESS)
                                val userData : UserInfo? = SharedPreferenceUtils.getInstance(this@VerificationActivity)
                                    .getObject(Constant.LOGINUSERDATA, UserInfo::class.java)

                                SharedPreferenceUtils.getInstance(this@VerificationActivity)
                                    .setValue(Constant.AUTH_TOKEN, userData!!.accessToken)
                                SharedPreferenceUtils.getInstance(this@VerificationActivity).removeKey(Constant.SIGNUP_DATA)
                                if(userData!!.is_subscribed!=null && userData.is_subscribed ==0){
                                    if (userData.typeName.isEmpty()){
                                        SharedPreferenceUtils.getInstance(this@VerificationActivity)
                                            .setValue(Constant.IS_USER_SUBSCRIBE, true)
                                        val intent = Intent(this@VerificationActivity, HomeActivity::class.java)
                                        startActivity(intent)
                                    }else {
                                        SharedPreferenceUtils.getInstance(this@VerificationActivity)
                                            .setValue(Constant.IS_USER_SUBSCRIBE, false)
                                        startActivity(
                                            Intent(
                                                this@VerificationActivity,
                                                PurchasePlan::class.java
                                            )
                                        )
                                    }
                                }else {
                                    SharedPreferenceUtils.getInstance(this@VerificationActivity)
                                        .setValue(Constant.IS_USER_SUBSCRIBE, true)
                                    val intent = Intent(this@VerificationActivity, HomeActivity::class.java)
                                    startActivity(intent)
                                }
                                finish()
                            }

                        } catch (e: Exception) {
                            e.printStackTrace()
                        }

                    }
                } catch (e: Exception) {
                    e.printStackTrace()
                }

            }

            override fun onFailure(call: Call<String>, t: Throwable) {
                loader!!.hide()
            }
        })

    }

    private fun callForResendOtp() {
        val map = HashMap<String, String>()
        map["email"] = data!!.email
        val otpData = String.format("%04d", Random.nextInt(10000))
        data!!.otp = otpData
        map["otp"] = otpData
        map["deviceID"] = Settings.Secure.getString(contentResolver, Settings.Secure.ANDROID_ID)
        callAPI(
            Constant.SEND_OTP,
            this,
            Constant.POST_API,
            map,
            true,
            false,
            Constant.SEND_OTP,
            null,
            this
        )
    }


    override fun onResponse(
        MethodName: String,
        responseCode: Int,
        mRes: String,
        responseTag: String
    ) {
        super.onResponse(MethodName, responseCode, mRes, responseTag)

        when (responseTag) {
            Constant.SEND_OTP -> {
                try {
                    val commonResponse = getCommonResponse<UserInfo>(mRes)
                    val code = commonResponse!!.success
                    printToastAlert(this, commonResponse.message)
                    if (code == 1) {


                    }

                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }

        }

    }
}
