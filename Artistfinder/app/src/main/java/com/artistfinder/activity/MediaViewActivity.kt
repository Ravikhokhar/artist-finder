package com.artistfinder.activity


import android.annotation.SuppressLint
import android.net.Uri
import android.os.Bundle
import android.view.View
import android.widget.MediaController
import com.artistfinder.R
import com.artistfinder.baseclass.BaseActivity
import com.artistfinder.dialog.CustomLoaderDialog
import com.artistfinder.global.Constant
import kotlinx.android.synthetic.main.activity_media_view.*
import kotlinx.android.synthetic.main.generic_toolbar.view.*


class MediaViewActivity : BaseActivity() {

    private var mediaType: String = ""
    private var mediaURL: String = ""
    private var currentPos = 0
    private var isPlaying = ""
    private var mediaController: MediaController? = null
    private var loader: CustomLoaderDialog? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_media_view)
        initView()
    }

    @SuppressLint("SetTextI18n")
    private fun initView() {
        val intent = intent
        toolbar.ivNotification?.visibility = View.GONE

        toolbar.ivSearch?.visibility = View.GONE

        // set back icon
        toolbar.ivHome?.visibility = View.VISIBLE
        toolbar.ivHome?.setImageDrawable(getIconFromAttributes(R.attr.backIcon))
        toolbar.ivHome?.setOnClickListener { onBackPressed() }
        if (intent != null && intent.hasExtra(Constant.MEDIA_TYPE) && intent.hasExtra(Constant.MEDIA_URL)) {
            mediaType = intent.getStringExtra(Constant.MEDIA_TYPE)
            mediaURL = intent.getStringExtra(Constant.MEDIA_URL)

            if (mediaType == Constant.MEDIA_IMG) {
                toolbar.tvTitle.text = "Photo"
                rlVideoView.visibility = View.GONE
                videoView.visibility = View.GONE
                imageView.visibility = View.VISIBLE
                imageView.setImageURI(mediaURL)
            } else {
                loader = CustomLoaderDialog(this)
                toolbar.tvTitle.text = "Video"
                imageView.visibility = View.GONE
                rlVideoView.visibility = View.VISIBLE
                videoView.visibility = View.VISIBLE
                videoSetup()
            }

        }
    }

    private fun videoSetup() {
        mediaController = MediaController(this)
        mediaController!!.setAnchorView(videoView)

        videoView.setMediaController(mediaController)
        videoView.setVideoURI(Uri.parse(mediaURL))
        videoView.requestFocus()
        loader!!.show(true)
        videoView.setOnPreparedListener { mp ->

            loader!!.hide()
            videoView.start()
        }

        videoView.setOnCompletionListener { mp ->

            videoView.seekTo(0)
            videoView.requestFocus()
            videoView.pause()
        }

        videoView.setOnErrorListener { mp, what, extra ->

            false
        }

    }

    override fun onStop() {
        if (videoView != null && videoView.isPlaying) {
            isPlaying = "0"
            currentPos = videoView.currentPosition
            videoView.pause()
        }
        super.onStop()
    }

    override fun onResume() {
        if (videoView != null && !videoView.isPlaying && isPlaying == "0") {

            videoView.seekTo(currentPos)
            videoView.start()
        }
        super.onResume()
    }

    override fun onDestroy() {
        super.onDestroy()
        if (videoView != null && videoView.isPlaying) {
            videoView.stopPlayback()
        }
    }
}
