package com.artistfinder.activity

import android.content.Intent
import android.os.Bundle
import android.provider.Settings
import android.view.View
import android.widget.CompoundButton
import androidx.appcompat.app.AppCompatDelegate
import androidx.appcompat.widget.PopupMenu
import androidx.databinding.DataBindingUtil
import com.artistfinder.R
import com.artistfinder.baseclass.BaseActivity
import com.artistfinder.databinding.ActivityLoginBinding
import com.artistfinder.global.Constant
import com.artistfinder.model.UserInfo
import com.artistfinder.utils.SharedPreferenceUtils
import com.artistfinder.utils.Utils
import kotlinx.android.synthetic.main.activity_login.*


class LoginActivity : BaseActivity() {

    private var binding: ActivityLoginBinding? = null
//    var states = arrayOf(intArrayOf(R.attr.state_checked), intArrayOf(android.R.attr.state_checked))

    var thumbColors = intArrayOf(R.color.white, R.color.color_dark_black)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_login)
        overridePendingTransition(R.anim.enter, R.anim.exit)
        initViews()
    }

    private fun initViews() {

        // click listener
        binding?.btnLogin?.setOnClickListener(this)
        binding?.txtSignup?.setOnClickListener(this)
        themeView.setOnClickListener(this)
        switchTheme.isChecked =
            AppCompatDelegate.getDefaultNightMode() == AppCompatDelegate.MODE_NIGHT_YES
        switchTheme.setOnCheckedChangeListener(object : CompoundButton.OnCheckedChangeListener {
            override fun onCheckedChanged(buttonView: CompoundButton?, isChecked: Boolean) {
                if (isChecked) {
                    AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES);
                    SharedPreferenceUtils.getInstance(this@LoginActivity)
                        .setValue(Constant.IS_DARK_MODE, true)
                } else {
                    AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO)
                    SharedPreferenceUtils.getInstance(this@LoginActivity)
                        .setValue(Constant.IS_DARK_MODE, false)
                }
            }
        })
    }


    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.txtSignup -> {
                val intent = Intent(this@LoginActivity, SignupActivity::class.java)
                startActivity(intent)
                finish()
            }

            R.id.btnLogin -> {
                if (isValidate()) {
                    callForLogin()
                }

            }

            R.id.themeView -> {
                val popupMenu: PopupMenu = PopupMenu(this, themeView)
                popupMenu.menuInflater.inflate(R.menu.popup_menu, popupMenu.menu)
                popupMenu.setOnMenuItemClickListener(PopupMenu.OnMenuItemClickListener { item ->
                    when (item.itemId) {
                        R.id.dark -> {
                            if (AppCompatDelegate.getDefaultNightMode() != AppCompatDelegate.MODE_NIGHT_YES) {
                                AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES)
                                SharedPreferenceUtils.getInstance(this)
                                    .setValue(Constant.IS_DARK_MODE, true)
//                                finish()
//                                startActivity(Intent(this, LoginActivity::class.java))
                            }
                        }
                        R.id.light -> {
                            if (AppCompatDelegate.getDefaultNightMode() != AppCompatDelegate.MODE_NIGHT_NO) {
                                AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);
                                SharedPreferenceUtils.getInstance(this)
                                    .setValue(Constant.IS_DARK_MODE, false)
//                                finish()
//                                startActivity(Intent(this, LoginActivity::class.java))
                            }


                        }
                    }

                    true
                })
                popupMenu.show()
            }
        }
    }

    private fun callForLogin() {
        val map = HashMap<String, String>()
        map["email"] = txtEmail.text.toString()
        map["password"] = txtPassword.text.toString()
        map["fcmToken"] =SharedPreferenceUtils.getInstance(this).getStringValue(Constant.DEVICETOKEN,"").toString()
        map["deviceID"] = Settings.Secure.getString(contentResolver, Settings.Secure.ANDROID_ID)
        map["deviceType"] = Constant.DEVICE_TYPE
        callAPI(
            Constant.LOGIN,
            this,
            Constant.POST_API,
            map,
            true,
            false,
            Constant.LOGIN,
            null,
            this
        )
    }

    private fun isValidate(): Boolean {
        if (txtEmail.text.toString().trim().length <= 0) {
            printToastAlert(this, getString(R.string.valid_enter_email))
            return false
        } else if (!Utils.isEmailValid(txtEmail.text.toString().trim())) {
            printToastAlert(this, getString(R.string.validate_email))
            return false
        } else if (txtPassword.text.toString().trim().length <= 0) {
            printToastAlert(this, getString(R.string.validate_enter_password))
            return false
        }
        return true
    }


    override fun onResponse(
        MethodName: String,
        responseCode: Int,
        mRes: String,
        responseTag: String
    ) {
        super.onResponse(MethodName, responseCode, mRes, responseTag)

        try {
            val commonResponse = getCommonResponse<UserInfo>(mRes)
            val userInfo = commonResponse!!.result
            val code = commonResponse.success
            printToastAlert(this, commonResponse.message)
            if (code == 1) {

                SharedPreferenceUtils.getInstance(this)
                    .setObject(Constant.LOGINUSERDATA, userInfo)
                SharedPreferenceUtils.getInstance(this)
                    .setValue(Constant.IS_USER_STATUS, Constant.STATUS_LOGIN_SUCCESS)
                val userData : UserInfo? = SharedPreferenceUtils.getInstance(this)
                    .getObject(Constant.LOGINUSERDATA, UserInfo::class.java)

                SharedPreferenceUtils.getInstance(this)
                    .setValue(Constant.AUTH_TOKEN, userData!!.accessToken)

                if(userData!!.is_subscribed!=null && userData!!.is_subscribed ==0){
                    if (userData.typeName.isEmpty()){
                        SharedPreferenceUtils.getInstance(this)
                            .setValue(Constant.IS_USER_SUBSCRIBE, true)
                        val intent = Intent(this@LoginActivity, HomeActivity::class.java)
                        startActivity(intent)
                    }else {
                        SharedPreferenceUtils.getInstance(this)
                            .setValue(Constant.IS_USER_SUBSCRIBE, false)
                        startActivity(Intent(this, PurchasePlan::class.java))
                    }
                }else {
                    SharedPreferenceUtils.getInstance(this)
                        .setValue(Constant.IS_USER_SUBSCRIBE, true)
                    val intent = Intent(this@LoginActivity, HomeActivity::class.java)
                    startActivity(intent)
                }
                finish()
            }

        } catch (e: Exception) {
            e.printStackTrace()
        }
    }
}
