package com.artistfinder.activity

import android.graphics.drawable.Drawable
import android.os.Bundle
import android.util.TypedValue
import android.view.View
import android.webkit.WebView
import android.webkit.WebViewClient
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.app.AppCompatDelegate
import androidx.core.content.ContextCompat
import com.artistfinder.R
import com.artistfinder.global.Constant
import com.artistfinder.utils.SharedPreferenceUtils
import kotlinx.android.synthetic.main.activity_web_view.*

class WebViewActivity : AppCompatActivity(), View.OnClickListener {

    var urlType = ""
    var url = ""
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (AppCompatDelegate.getDefaultNightMode() == AppCompatDelegate.MODE_NIGHT_YES) {
            setTheme(R.style.DarkTheme);
        } else {
            setTheme(R.style.LightTheme);
        }
        setContentView(R.layout.activity_web_view)
        overridePendingTransition(R.anim.enter, R.anim.exit)
        initView()
    }

    private fun initView() {
        setUpToolbar()
        setOnClickListener()

        val intent  = intent
        if (intent.hasExtra(Constant.BUNDLE_URL)){
            urlType = intent.getStringExtra(Constant.BUNDLE_URL)
        }
        val isNightModeEnabled =
            SharedPreferenceUtils.getInstance(this@WebViewActivity).getBoolanValue(
                Constant.IS_DARK_MODE, false
            )
        if (urlType == "terms"){
            txtTitle.text = this.resources?.getString(R.string.terms_condition)
            url = if (isNightModeEnabled) {
                Constant.URL_TERMS_CONDITION_BLACK
            } else {
                Constant.URL_TERMS_CONDITION_WHITE
            }
        }else {
            txtTitle.text = this.resources?.getString(R.string.privacy_policy)
            url = if (isNightModeEnabled) {
                Constant.URL_PRIVACY_POLICY_BLACK
            } else {
                Constant.URL_PRIVACY_POLICY_WHITE
            }
        }

        val webView = findViewById<WebView>(R.id.webView)
        webView.settings.javaScriptEnabled = true
        webView.settings.loadWithOverviewMode = true
        webView.webViewClient = object : WebViewClient() {
            override fun shouldOverrideUrlLoading(view: WebView?, url: String?): Boolean {
                view?.loadUrl(url)
                return true
            }
        }
        webView.loadUrl(url)
    }

    private fun setOnClickListener() {
        imgBack?.setOnClickListener(this)
    }

    override fun onClick(v: View?) {
        when (v!!.id) {
            R.id.imgBack -> {
                onBackPressed()
            }
        }
    }

    private fun setUpToolbar() {

    }

    fun getIconFromAttributes(attrId: Int): Drawable {
        val typedValue = TypedValue()
        this.theme?.resolveAttribute(attrId, typedValue, true)
        val imageResId = typedValue.resourceId
        return this.let { ContextCompat.getDrawable(it, imageResId) }
            ?: throw IllegalArgumentException("Cannot load drawable $imageResId")
    }
}
