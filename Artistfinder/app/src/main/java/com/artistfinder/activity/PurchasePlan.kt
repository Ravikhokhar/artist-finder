package com.artistfinder.activity

import android.content.Intent
import android.os.Bundle
import android.text.SpannableStringBuilder
import android.text.method.LinkMovementMethod
import android.text.style.ClickableSpan
import android.text.style.ForegroundColorSpan
import android.view.View
import android.widget.TextView
import androidx.core.content.ContextCompat
import com.android.billingclient.api.BillingClient
import com.android.billingclient.api.Purchase
import com.android.billingclient.api.SkuDetails
import com.android.billingclient.api.SkuDetailsResponseListener
import com.artistfinder.R
import com.artistfinder.billing.BillingConstants
import com.artistfinder.billing.BillingManager
import com.artistfinder.global.Constant
import com.artistfinder.model.SubscriptionInfo
import com.artistfinder.model.UserInfo
import com.artistfinder.retrofitclient.RetroFitResponse
import com.artistfinder.utils.Logger
import com.artistfinder.utils.SharedPreferenceUtils
import kotlinx.android.synthetic.main.activity_purchase_plan.*


class PurchasePlan : com.artistfinder.baseclass.BaseActivity(),
    BillingManager.BillingUpdatesListener,
    SkuDetailsResponseListener, RetroFitResponse {

    private var billingManager: BillingManager? = null
    private var isSubscribe: Boolean = false
    private var userInfo: UserInfo? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_purchase_plan)
        userInfo = SharedPreferenceUtils.getInstance(this)
            .getObject(Constant.LOGINUSERDATA, UserInfo::class.java)

        if (userInfo!!.typeName == "Other" && userInfo!!.typeName.isEmpty()) {
            finish()
        }
        btnSubscribe.setOnClickListener {
            callSubcription()

        }
        customTextView(tvPolicy)
    }


    private fun callSubcription() {
        if (billingManager != null && billingManager!!.billingClientResponseCode > BillingManager.BILLING_MANAGER_NOT_INITIALIZED) {
            val skuItem = java.util.ArrayList<String>()
            skuItem.add(BillingConstants.SKU_MONTHLY)
            billingManager!!.querySkuDetailsAsync(BillingClient.SkuType.SUBS, skuItem, this)
            isSubscribe = true
        }
    }

    override fun onResume() {
        super.onResume()

        billingManager = BillingManager(this, this)

        if (billingManager != null && billingManager!!.billingClientResponseCode === com.android.billingclient.api.BillingClient.BillingResponse.OK) {
            billingManager!!.queryPurchases()
        }

    }

    override fun onSkuDetailsResponse(responseCode: Int, skuDetailsList: MutableList<SkuDetails>?) {
        if (responseCode == 0 && skuDetailsList!!.size > 0) {
//            val sku = skuDetailsList.get(0).sku
//            purchasingId = sku
//            val type = skuDetailsList.get(0).type
//            billingManager!!.initiatePurchaseFlow(sku, type)
//            billingManager!!.initiatePurchaseFlowNew(skuDetailsList)
            billingManager!!.initiatePurchaseFlow(
                BillingConstants.SKU_MONTHLY,
                BillingClient.SkuType.SUBS
            )
        }


    }

    override fun onBillingClientSetupFinished() {

    }

    override fun onConsumeFinished(token: String, result: Int) {
        if (result == BillingClient.BillingResponse.OK) {
            // Successfully consumed, so we apply the effects of the item in our
            // game world's logic, which in our case means filling the gas tank a bit
            Logger.print(">>", "Consumption successful..")
            /*customLoaderDialog.hide()
            if (purchasingId == BillingConstants.SKU_BTC_MINING) {
                SharedPreferenceUtils.getInstance(this).setValue(Constant.IS_BTC_MINING, true)
                purchasingId = ""
            } else if (purchasingId == BillingConstants.SKU_DASH_MINIG) {
                SharedPreferenceUtils.getInstance(this).setValue(Constant.IS_DASH_MINING, true)
                purchasingId = ""
            } else {
                for (i in planData.indices) {
                    if (purchasingId == planData.get(i).getSkuName()) {

                        updateUserWallet(planData.get(i).getPrice())

                    }
                }
            }*/

        }
    }

    override fun onPurchasesUpdated(resultCode: Int, purchases: List<Purchase>) {
        if (resultCode == BillingClient.BillingResponse.OK) {
            for (purchase in purchases) {
                /* if (purchase.getSku() == BillingConstants.SKU_BTC_MINING) {
                     SharedPreferenceUtils.getInstance(this).setValue(Constant.IS_BTC_MINING, true)
                 } else if (purchase.getSku() == BillingConstants.SKU_DASH_MINIG) {
                     SharedPreferenceUtils.getInstance(this).setValue(Constant.IS_DASH_MINING, true)
                 }
                 customLoaderDialog.show(false)*/
                billingManager!!.consumeAsync(purchase.purchaseToken)
                if (!purchase.sku.isEmpty() && isSubscribe) {
                    isSubscribe = false
                    callForSubscription(
                        purchase.orderId,
                        userInfo!!.userID,
                        purchase.purchaseToken,
                        purchase.isAutoRenewing
                    )
                }
            }
        }
    }


    private fun callForSubscription(
        orderID: String?,
        userId: Int?,
        purchaseToken: String?,
        auto_renew: Boolean
    ) {
        var autorenew = 0
        if (auto_renew) {
            autorenew = 0
        } else {
            autorenew = 1
        }
        val map = HashMap<String, String>()
        map["userID"] = "${userId}"
        map["orderID"] = "${orderID}"
        map["productType"] = "1"
        map["purchaseToken"] = "${purchaseToken}"
        map["auto_renew"] = "${autorenew}"
        callAPI(
            Constant.IN_APP_PURCHASE,
            this,
            Constant.POST_API,
            map,
            true,
            false,
            Constant.IN_APP_PURCHASE,
            null,
            this
        )
    }

    override fun onResponse(
        MethodName: String,
        responseCode: Int,
        mRes: String,
        responseTag: String
    ) {
        super.onResponse(MethodName, responseCode, mRes, responseTag)

        try {
            val commonResponse = getCommonResponse<SubscriptionInfo>(mRes)
            val subInfo = commonResponse!!.result
            val code = commonResponse.success
            if (code == 1) {

                SharedPreferenceUtils.getInstance(this)
                    .setObject(Constant.SUBSCRIPTION_DATA, subInfo)

                val userData: SubscriptionInfo? = SharedPreferenceUtils.getInstance(this)
                    .getObject(Constant.SUBSCRIPTION_DATA, SubscriptionInfo::class.java)
                SharedPreferenceUtils.getInstance(this)
                    .setValue(Constant.IS_USER_SUBSCRIBE, true)
                val intent = Intent(this@PurchasePlan, HomeActivity::class.java)
                startActivity(intent)
                finish()
            }

        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    override fun onRetrofitError(MethodName: String, mError: String, responseTag: Any) {
    }

    private fun customTextView(view: TextView) {
        val isNightModeEnabled =
            SharedPreferenceUtils.getInstance(this).getBoolanValue(Constant.IS_DARK_MODE, false)
        val termsURL = if (isNightModeEnabled) {
            Constant.URL_TERMS_CONDITION_BLACK
        } else {
            Constant.URL_TERMS_CONDITION_WHITE
        }

        val privacyURL = if (isNightModeEnabled) {
            Constant.URL_PRIVACY_POLICY_BLACK
        } else {
            Constant.URL_PRIVACY_POLICY_WHITE
        }

        val spanTxt = SpannableStringBuilder(
            ""
        )
        spanTxt.append("Policy Privacy")
        spanTxt.setSpan(object : ClickableSpan() {
            override fun onClick(widget: View?) {
                val intent = Intent(this@PurchasePlan, WebViewActivity::class.java)
                intent.putExtra(Constant.BUNDLE_URL, "Policy")
                startActivity(intent)
            }
        }, spanTxt.length - "Policy Privacy".length, spanTxt.length, 0)
        spanTxt.append(" And ")
//        spanTxt.setSpan(ForegroundColorSpan(Color.BLACK), 15, spanTxt.length, 0)
        spanTxt.append("Terms Of Service")
        spanTxt.setSpan(object : ClickableSpan() {
            override fun onClick(widget: View?) {
                val intent = Intent(this@PurchasePlan, WebViewActivity::class.java)
                intent.putExtra(Constant.BUNDLE_URL, "terms")
                startActivity(intent)
            }
        }, spanTxt.length - "Terms Of Service".length, spanTxt.length, 0)
        spanTxt.setSpan(
            ForegroundColorSpan(ContextCompat.getColor(this, R.color.blue)),
            0,
            spanTxt.length,
            0
        )
        view.movementMethod = LinkMovementMethod.getInstance()
        view.setText(spanTxt, TextView.BufferType.SPANNABLE)
    }
}
