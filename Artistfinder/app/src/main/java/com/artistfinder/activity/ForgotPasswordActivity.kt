package com.artistfinder.activity

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.artistfinder.R
import com.artistfinder.baseclass.BaseActivity

class ForgotPasswordActivity : BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_forgot_password)
        overridePendingTransition(R.anim.enter, R.anim.exit)
    }
}
