package com.artistfinder.activity

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import androidx.appcompat.app.AppCompatActivity
import com.artistfinder.R
import com.artistfinder.baseclass.BaseActivity
import com.artistfinder.global.Constant
import com.artistfinder.utils.SharedPreferenceUtils


class SplashActivity : BaseActivity() {
    private var isLoginStatus = ""
    private var isSubscribeStatus  =  false
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)
        overridePendingTransition(R.anim.enter, R.anim.exit)
        nextActivity()
    }

    private fun nextActivity() {
        isLoginStatus = SharedPreferenceUtils.getInstance(this).getStringValue(Constant.IS_USER_STATUS,"")!!

        val handler = Handler()
        handler.postDelayed({
            val intent : Intent = when (isLoginStatus) {
                Constant.STATUS_LOGIN_SUCCESS ->{
                    isSubscribeStatus = SharedPreferenceUtils.getInstance(this).getBoolanValue(Constant.IS_USER_SUBSCRIBE,false)!!
                    if (isSubscribeStatus) {
                        Intent(this@SplashActivity, HomeActivity::class.java)
                    }else{
                        Intent(this@SplashActivity, PurchasePlan::class.java)
                    }
                }
                Constant.STATUS_OTP -> Intent(this@SplashActivity, VerificationActivity::class.java)
                else -> Intent(this@SplashActivity, LoginActivity::class.java)
            }
            startActivity(intent)
            finish()

        }, 2000)

    }
}
