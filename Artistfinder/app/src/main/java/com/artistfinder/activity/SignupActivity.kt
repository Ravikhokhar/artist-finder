package com.artistfinder.activity

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.os.Message
import android.provider.Settings
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import androidx.annotation.RequiresApi
import com.artistfinder.R
import com.artistfinder.baseclass.BaseActivity
import com.artistfinder.dialog.CustomLoaderDialog
import com.artistfinder.fragment.WebViewFragment
import com.artistfinder.global.Constant
import com.artistfinder.listners.OnMultiMediaListener
import com.artistfinder.model.UserInfo
import com.artistfinder.model.UserProfile
import com.artistfinder.retrofitclient.RetroClient2
import com.artistfinder.utils.*
import com.kbeanie.multipicker.api.entity.ChosenImage
import com.kbeanie.multipicker.api.entity.ChosenVideo
import com.theartofdev.edmodo.cropper.CropImage
import com.vincent.filepicker.filter.entity.ImageFile
import com.vincent.filepicker.filter.entity.VideoFile
import kotlinx.android.synthetic.main.activity_signup.*
import kotlinx.android.synthetic.main.layout_slot_time.*
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.File
import kotlin.random.Random


class SignupActivity : BaseActivity(), CameraUtils.OnCameraResult,
    AdapterView.OnItemSelectedListener, OnMultiMediaListener {
    override fun onMultiImageListener(imageList: ArrayList<ImageFile>) {
    }

    override fun onMultiVideoListener(videoList: ArrayList<VideoFile>) {
    }

    override fun onVideoSuccess(images: List<ChosenVideo>) {

    }


    private var cameraUtils: CameraUtils? = null
    private var selectedImageFile: File? = null
    private var loader: CustomLoaderDialog? = null
    private var artistId: Int = -1
    private var selectedArtistType: String = "Artist Type"
    private var selectedTimeSlotList: ArrayList<String> = ArrayList()
    private var isButton1 = false
    private var isButton2 = false
    private var isButton3 = false
    private var isButton4 = false
    private var isButton5 = false
    private var isButton6 = false
    private var isButton7 = false
    private var isButton8 = false
    private var isButton9 = false

    var lat: Double = 0.0
    var long: Double = 0.0
    var selectedTimeSlot: String = ""
    var artistType: String = ""
    var artistTypeName: String = ""
    var other: String = ""
    var otherArtist: String = ""
    var otp: String = ""
    var isFirstTime: Boolean = true
    var size: Int = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_signup)
        overridePendingTransition(R.anim.enter, R.anim.exit)
        initViews()
    }

    private fun initViews() {
        imgprofile.setOnClickListener(this)
        txtLoginNow.setOnClickListener(this)
        btnSignup.setOnClickListener(this)
        imgBack.setOnClickListener(this)
        tvArtistName.setOnClickListener(this)
        txtTermsAndCondition.setOnClickListener(this)
        btn1.setOnClickListener(this)
        btn2.setOnClickListener(this)
        btn3.setOnClickListener(this)
        btn4.setOnClickListener(this)
        btn5.setOnClickListener(this)
        btn6.setOnClickListener(this)
        btn7.setOnClickListener(this)
        btn8.setOnClickListener(this)
        btn9.setOnClickListener(this)

        cameraUtils = CameraUtils(this, this, this)
        loader = CustomLoaderDialog(this)
        spinnerSetUp()
    }

    override fun onClick(v: View?) {
        when (v!!.id) {
            R.id.imgprofile -> {
                cameraUtils!!.openCameraGallery(true, false)
            }

            R.id.txtLoginNow -> {
                loginActivity()
            }

            R.id.btnSignup -> {

                callForLatLong()

                /*  if (isValidate()) {
                      //callForSignUp()
                      callForSentOtp()
                  }*/
            }


            R.id.imgBack -> {
                onBackPressed()
            }

            R.id.tvArtistName -> {
                spArtistType.performClick()
            }


            R.id.btn1 -> {
                // btn1.isActivated = !btn1.isActivated
                setButton1()
            }

            R.id.btn2 -> {
                //btn2.isActivated = !btn2.isActivated
                setButton2()
            }

            R.id.btn3 -> {
                // btn3.isActivated = !btn3.isActivated
                setButton3()
            }

            R.id.btn4 -> {
                //btn4.isActivated = !btn4.isActivated
                setButton4()
            }

            R.id.btn5 -> {
                //btn5.isActivated = !btn5.isActivated
                setButton5()
            }

            R.id.btn6 -> {
                // btn6.isActivated = !btn6.isActivated
                setButton6()
            }
            R.id.btn7 -> {
                //btn6.isActivated = !btn6.isActivated
                setButton7()
            }
            R.id.btn8 -> {
                // btn6.isActivated = !btn6.isActivated
                setButton8()
            }
            R.id.btn9 -> {
                //btn6.isActivated = !btn6.isActivated
                setButton9()
            }
            R.id.txtTermsAndCondition -> {

                val intent = Intent(this@SignupActivity, WebViewActivity::class.java)
                intent.putExtra(Constant.BUNDLE_URL,"terms")
                startActivity(intent)
            }
        }
    }

    private fun callForLatLong() {
        val locationGeo: GeocodingLocation = GeocodingLocation()
        locationGeo.getAddressFromLocation(
            "${txtAddress.text.toString()},${txtCity.text},${txtCountry.text}",
            this,
            Handler() {

                val bundle = it!!.data
                lat = bundle.getDouble("latittude")
                long = bundle.getDouble("longitude")
                Logger.print(">>", lat.toString() + " " + long)

                if (isValidate()) {
                    //callForSignUp()
                    callForSentOtp()
                }

                return@Handler true

            })

    }

    private fun loginActivity() {
        val intent = Intent(this@SignupActivity, LoginActivity::class.java)
        startActivity(intent)
        finish()
    }

    override fun onBackPressed() {
        loginActivity()
    }

    private fun callForSentOtp() {
        val map = HashMap<String, String>()
        map["email"] = txtEmail.text.toString()
        otp = String.format("%04d", Random.nextInt(10000))
        Logger.print("OTP>>", otp)
        map["otp"] = otp
        map["deviceID"] = Settings.Secure.getString(contentResolver, Settings.Secure.ANDROID_ID)
        callAPI(
            Constant.SEND_OTP,
            this,
            Constant.POST_API,
            map,
            true,
            false,
            Constant.SEND_OTP,
            null,
            this
        )
    }

    private fun callForSignUp() {

        loader!!.show(true)
        val map = HashMap<String, RequestBody>()

        val email = RequestBody.create(MultipartBody.FORM, txtEmail.text.toString())
        val password = RequestBody.create(MultipartBody.FORM, txtCreatePassword.text.toString())
        val name = RequestBody.create(MultipartBody.FORM, txtname.text.toString())
        val username = RequestBody.create(MultipartBody.FORM, txtUsername.text.toString())
        val gender = RequestBody.create(MultipartBody.FORM, "")
        val country = RequestBody.create(MultipartBody.FORM, txtCountry.text.toString())
        val city = RequestBody.create(MultipartBody.FORM, txtCity.text.toString())
        val address = RequestBody.create(MultipartBody.FORM, txtAddress.text.toString())
        val latlong = RequestBody.create(MultipartBody.FORM, "")
        val phone = RequestBody.create(MultipartBody.FORM, txtNumber.text.toString())


        /*map["email"] = txtEmail.text.toString()
        map["password"] = txtCreatePassword.text.toString()
        map["name"] = txtname.text.toString()
        map["username"] = txtUsername.text.toString()
        map["gender"] = ""
        map["country"] = txtCountry.text.toString()
        map["city"] = txtCity.text.toString()
        map["address"] = txtAddress.text.toString()
        map["latlong"] = ""
        map["artistType"] = "2"
        map["phone"] = txtNumber.text.toString()*/
        var body: MultipartBody.Part? = null
        if (selectedImageFile != null) {

            val ext = Utils!!.getExtensionFromFilePath(selectedImageFile!!.path);

            System.out.println("file name" + ext);

//            map["profileImg"] =  "${System.currentTimeMillis()}$ext"


            val reqFile = RequestBody.create(MediaType.parse("image/*"), selectedImageFile);
            body = MultipartBody.Part.createFormData(
                "profileImg",
                selectedImageFile!!.getName(),
                reqFile
            );

        }

        checkArtistType()
        val artistType = RequestBody.create(MultipartBody.FORM, this.artistType)
        val availableSlot = RequestBody.create(MultipartBody.FORM, this.selectedTimeSlot)
        val other = RequestBody.create(MultipartBody.FORM, this.other)
        val otherArtist = RequestBody.create(MultipartBody.FORM, this.otherArtist)
        val deviceType = RequestBody.create(MultipartBody.FORM, Constant.DEVICE_TYPE)
        val deviceID = RequestBody.create(MultipartBody.FORM, Settings.Secure.getString(contentResolver, Settings.Secure.ANDROID_ID))
        val fcmToken = RequestBody.create(MultipartBody.FORM, SharedPreferenceUtils.getInstance(this).getStringValue(Constant.DEVICETOKEN,"").toString())

        /*callNewAPI(
           Constant.REGISTRATION,
           this,
           Constant.POST_API,
           map,
           true,
           false,
           Constant.REGISTRATION,
           body,
           this
       )*/


        var call: Call<String>? = null

        call = RetroClient2.getClient(Constant.BASE_URL)
            ?.callForSignUp(
                email = email,
                password = password,
                name = name,
                username = username,
                gender = gender,
                country = country,
                city = city,
                address = address,
                latlong = latlong,
                artistType = artistType,
                availableSlot = availableSlot,
                phone = phone,
                files = body!!,
                other = other,
                otherArtist = otherArtist,
                deviceType = deviceType,
                deviceID = deviceID,
                fcmToken = fcmToken
            )



        call!!.enqueue(object : Callback<String> {
            override fun onResponse(call: Call<String>, response: Response<String>) {
                Logger.print(">>", "response code --->> " + response.code())
                Logger.print(">>", "response bofu --->> " + response.body())
                loader!!.hide()
                try {
                    if (response.code() == 200) {

                        try {
                            val resp = JSONObject(response.body())
                            val code = resp.getInt("success");
                            printToastAlert(this@SignupActivity, resp.getString("message"))
                            if (code == 1) {
                                val intent = Intent(this@SignupActivity, LoginActivity::class.java)
                                startActivity(intent)
                                finish()
                            }

                        } catch (e: Exception) {
                            e.printStackTrace()
                        }

                    }
                } catch (e: Exception) {
                    e.printStackTrace()
                }

            }

            override fun onFailure(call: Call<String>, t: Throwable) {
                loader!!.hide()
            }
        })

    }

    private fun isValidate(): Boolean {
        if (txtname.text.toString().trim().isEmpty()) {
            printToastAlert(this, getString(R.string.valid_enter_name))
            return false
        } else if (txtUsername.text.toString().trim().isEmpty()) {
            printToastAlert(this, getString(R.string.valid_enter_username))
            return false
        } else if (txtEmail.text.toString().trim().isEmpty()) {
            printToastAlert(this, getString(R.string.valid_enter_email))
            return false
        } else if (!Utils.isEmailValid(txtEmail.text.toString().trim())) {
            printToastAlert(this, getString(R.string.validate_email))
            return false
        } else if (txtCountry.text.toString().trim().isEmpty()) {
            printToastAlert(this, getString(R.string.valid_country))
            return false
        } else if (txtCity.text.toString().trim().isEmpty()) {
            printToastAlert(this, getString(R.string.valid_city))
            return false
        } else if (txtAddress.text.toString().trim().isEmpty()) {
            printToastAlert(this, getString(R.string.validate_address))
            return false
        } else if (lat == 0.0 && long == 0.0) {
            printToastAlert(this, getString(R.string.validate_proper_address))
            return false
        } else if (selectedArtistType.equals("Artist Type")) {
            printToastAlert(this, getString(R.string.validate_spinner))
            return false
        } else if (selectedArtistType.equals("Other") && edtOtherArtistType.text.toString().trim().isEmpty()) {

            printToastAlert(this, getString(R.string.validate_other_artist))
            return false
        } else if (selectedArtistType == "Other" && !checkTimeSlotIsSelected()) {
            printToastAlert(this, getString(R.string.validate_time_slot))
            return false

        } else if (selectedArtistType == "Artist" && artistId == -1) {
            printToastAlert(this, getString(R.string.validate_artist))
            return false

        } else if (selectedArtistType == "Artist" && !checkTimeSlotIsSelected()) {
            printToastAlert(this, getString(R.string.validate_time_slot))
            return false

        } else if (txtNumber.text.toString().trim().isEmpty()) {
            printToastAlert(this, getString(R.string.validate_phone))
            return false
        } else if (txtNumber.text.toString().trim().length < 8 || txtNumber.text.toString().trim().length > 11) {
            printToastAlert(this, getString(R.string.validate_valid_phone))
            return false
        } else if (txtCreatePassword.text.toString().trim().isEmpty()) {
            printToastAlert(this, getString(R.string.validate_enter_password))
            return false
        } else if (txtCreatePassword.text.toString().trim().length < 8) {
            printToastAlert(this, getString(R.string.validate_enter_valid_password))
            return false
        } else if (txtConfirmPassword.text.toString().trim().isEmpty()) {
            printToastAlert(this, getString(R.string.validate_enter_confirm_password))
            return false
        } else if (txtCreatePassword.text.toString().trim() != txtConfirmPassword.text.toString().trim()) {
            printToastAlert(this, getString(R.string.validate_confirm_password))
            return false
        } else if (selectedImageFile == null) {
            printToastAlert(this, getString(R.string.validate_profile))
            return false
        } else if (!checkTerms.isChecked) {
            printToastAlert(this, getString(R.string.validate_terms_condition))
            return false
        }
        return true
    }

    private fun spinnerSetUp() {
        spArtistType!!.onItemSelectedListener = this
        // remove space
        spArtistType.post { spArtistType.setPadding(0, 0, 0, 0) }

        val stringList: ArrayList<String> = ArrayList()
        stringList.add("Artist")
        stringList.add("Non Artist")
//        stringList.add("Other")
        stringList.add("Artist Type")
        size = stringList.size - 1


        // Create an ArrayAdapter using a simple spinner layout and languages array
        val spAdapter = SpinnerArrayAdapter(this, R.layout.item_spinner, stringList)
        spAdapter.setDropDownViewResource(R.layout.spinner_dropdown_item)
        // Set Adapter to Spinner
        spArtistType!!.adapter = spAdapter

        spArtistType!!.setSelection(size)


    }


    class SpinnerArrayAdapter(context: Context?, val resource: Int, val list: ArrayList<String>) :
        ArrayAdapter<String>(context, resource, list) {
        override fun getCount(): Int {
            return list.size - 1
        }
    }

    override fun onError(error: String) {
        printToastAlert(this, error)
    }

    override fun onSuccess(images: List<ChosenImage>) {
        if (images != null && images.size > 0) {
            selectedImageFile = File(images[0].thumbnailPath)
            //binding.ivProfilePic.setImageURI(Uri.fromFile(selectedImageFile));
            CropImage.activity(Uri.fromFile(selectedImageFile))
                .setAllowFlipping(false)
                .setFlipHorizontally(false)
                .setFlipVertically(false)
                .setFixAspectRatio(true)
                .start(this)
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        cameraUtils!!.onActivityResult(requestCode, resultCode, data)
        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            val result = CropImage.getActivityResult(data)
            if (resultCode == Activity.RESULT_OK) {
                val resultUri = result.uri
                imgprofile.setImageURI(resultUri, this)
                txtChoosePhoto.visibility = View.GONE
                selectedImageFile = File(resultUri.path!!)
                /*filePath = result.getUri().getPath();
                binding.svImage.setImageURI(resultUri);*/
            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                val error = result.error
            }
        } else if (requestCode == 105) {
            Logger.print(">>GOTa")
            if (data != null && data.hasExtra("id")) {
                artistId = data.getIntExtra("id", -1)
                artistTypeName = data.getStringExtra("name")
                if (artistId == -1 && artistTypeName == "") {
                    spArtistType!!.setSelection(size)
                    tvArtistName.text = ""
                    timeSlot.visibility = View.GONE
                } else {
                    if (data.getStringExtra("name").equals("Other",true)){
                        edtOtherArtistType.visibility = View.VISIBLE
                        selectedArtistType = data.getStringExtra("name")
                    }else {
                        edtOtherArtistType.visibility = View.GONE
                    }
                    tvArtistName.text = data.getStringExtra("name")
                    timeSlot.visibility = View.VISIBLE

                }
            }


        }

    }


    @RequiresApi(api = Build.VERSION_CODES.M)
    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String>,
        grantResults: IntArray
    ) {
        cameraUtils!!.onRequestPermissionsResult(requestCode, permissions, grantResults)

    }

    override fun onNothingSelected(parent: AdapterView<*>?) {

    }


    override fun onResponse(
        MethodName: String,
        responseCode: Int,
        mRes: String,
        responseTag: String
    ) {
        super.onResponse(MethodName, responseCode, mRes, responseTag)

        try {
            val commonResponse = getCommonResponse<UserInfo>(mRes)
            val code = commonResponse!!.success
            printToastAlert(this, commonResponse.message)
            if (code == 1) {

                var body: MultipartBody.Part? = null

                checkArtistType()


                val data = UserProfile(
                    selectedImageFile!!.path,
                    txtname.text.toString(),
                    txtUsername.text.toString(),
                    txtEmail.text.toString(),
                    txtCountry.text.toString(),
                    txtCity.text.toString(),
                    otp,
                    txtAddress.text.toString(),
                    artistType,
                    otherArtist,
                    txtNumber.text.toString(),
                    txtCreatePassword.text.toString(),
                    lat.toString(),
                    long.toString(),
                    other,
                    selectedTimeSlot
                )
                SharedPreferenceUtils.getInstance(this@SignupActivity)
                    .setValue(Constant.IS_USER_STATUS, Constant.STATUS_OTP)
                SharedPreferenceUtils.getInstance(this@SignupActivity)
                    .setObject(Constant.SIGNUP_DATA, data)
                val i = Intent(this@SignupActivity, VerificationActivity::class.java)
                //i.putExtra(Constant.DATA, data)
                startActivity(i)
                finish()

            }

        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
        selectedArtistType = spArtistType.getItemAtPosition(position).toString()
        tvArtistName.text = selectedArtistType
        clearSelection()
        when (position) {
            0 -> {

                edtOtherArtistType.visibility = View.GONE
                val i = Intent(this, ArtistTypeActivity::class.java)
                startActivityForResult(i, 105)

            }
//            2 -> {
//                edtOtherArtistType.visibility = View.VISIBLE
//                timeSlot.visibility = View.VISIBLE
//            }
            else -> {
                timeSlot.visibility = View.GONE
                edtOtherArtistType.visibility = View.GONE
            }
        }
    }


    private fun setButton1() {
        isButton1 = if (isButton1) {
            btn1.setBackgroundResource(R.drawable.border_back)
            false
        } else {
            btn1.setBackgroundResource(R.drawable.bg_button_rectangle)
            true
        }
    }

    private fun setButton2() {
        isButton2 = if (isButton2) {
            btn2.setBackgroundResource(R.drawable.border_back)
            false
        } else {
            btn2.setBackgroundResource(R.drawable.bg_button_rectangle)
            true
        }
    }

    private fun setButton3() {
        isButton3 = if (isButton3) {
            btn3.setBackgroundResource(R.drawable.border_back)
            false
        } else {
            btn3.setBackgroundResource(R.drawable.bg_button_rectangle)
            true
        }
    }

    private fun setButton4() {
        isButton4 = if (isButton4) {
            btn4.setBackgroundResource(R.drawable.border_back)
            false
        } else {
            btn4.setBackgroundResource(R.drawable.bg_button_rectangle)
            true
        }
    }

    private fun setButton5() {
        isButton5 = if (isButton5) {
            btn5.setBackgroundResource(R.drawable.border_back)
            false
        } else {
            btn5.setBackgroundResource(R.drawable.bg_button_rectangle)
            true
        }
    }

    private fun setButton6() {
        isButton6 = if (isButton6) {
            btn6.setBackgroundResource(R.drawable.border_back)
            false
        } else {
            btn6.setBackgroundResource(R.drawable.bg_button_rectangle)
            true
        }
    }

    private fun setButton7() {
        isButton7 = if (isButton7) {
            btn7.setBackgroundResource(R.drawable.border_back)
            false
        } else {
            btn7.setBackgroundResource(R.drawable.bg_button_rectangle)
            true
        }
    }

    private fun setButton8() {
        isButton8 = if (isButton8) {
            btn8.setBackgroundResource(R.drawable.border_back)
            false
        } else {
            btn8.setBackgroundResource(R.drawable.bg_button_rectangle)
            true
        }
    }

    private fun setButton9() {
        isButton9 = if (isButton9) {
            btn9.setBackgroundResource(R.drawable.border_back)
            false
        } else {
            btn9.setBackgroundResource(R.drawable.bg_button_rectangle)
            true
        }
    }


    private fun clearSelection() {
        isButton1 = false
        isButton2 = false
        isButton3 = false
        isButton4 = false
        isButton5 = false
        isButton6 = false
        isButton7 = false
        isButton8 = false
        isButton9 = false
        btn1.setBackgroundResource(R.drawable.border_back)
        btn2.setBackgroundResource(R.drawable.border_back)
        btn3.setBackgroundResource(R.drawable.border_back)
        btn4.setBackgroundResource(R.drawable.border_back)
        btn5.setBackgroundResource(R.drawable.border_back)
        btn6.setBackgroundResource(R.drawable.border_back)
        btn7.setBackgroundResource(R.drawable.border_back)
        btn8.setBackgroundResource(R.drawable.border_back)
        btn9.setBackgroundResource(R.drawable.border_back)

        selectedTimeSlotList.clear()
        selectedTimeSlot = ""
    }

    private fun checkTimeSlotIsSelected(): Boolean {
        var check = 0
        if (isButton1) {
            selectedTimeSlotList.add(btn1.text.toString())
            check++
        }

        if (isButton2) {
            selectedTimeSlotList.add(btn2.text.toString())
            check++
        }
        if (isButton3) {
            selectedTimeSlotList.add(btn3.text.toString())
            check++
        }
        if (isButton4) {
            selectedTimeSlotList.add(btn4.text.toString())
            check++
        }
        if (isButton5) {
            selectedTimeSlotList.add(btn5.text.toString())
            check++
        }
        if (isButton6) {
            selectedTimeSlotList.add(btn6.text.toString())
            check++
        }
        if (isButton7) {
            selectedTimeSlotList.add(btn7.text.toString())
            check++
        }
        if (isButton8) {
            selectedTimeSlotList.add(btn8.text.toString())
            check++
        }
        if (isButton9) {
            selectedTimeSlotList.add(btn9.text.toString())
            check++
        }


        return check != 0
    }

    /**
     * This method set all the value of artist type weather user selected artist or no artist or other
     */
    private fun checkArtistType() {
        when (selectedArtistType) {
            "Artist" -> {
                artistType = artistId.toString()
                otherArtist = ""
                setTimeSlot()
            }
            "Non Artist" -> {
                artistType = ""
                otherArtist = ""
                selectedTimeSlot = ""
            }
            "Other" -> {
                artistType = "10"
                otherArtist = edtOtherArtistType.text.toString().trim()
                setTimeSlot()
            }
        }
    }

    private fun setTimeSlot(){
        selectedTimeSlot = ""
        if (selectedTimeSlotList!= null && selectedTimeSlotList.size > 0) {
            val noRepeatedList: ArrayList<String> = ArrayList(LinkedHashSet<String>(selectedTimeSlotList))
            for (i in noRepeatedList.indices) {
                if (i == (noRepeatedList.size - 1)) {
                    selectedTimeSlot += noRepeatedList[i]
                } else {
                    selectedTimeSlot = selectedTimeSlot + noRepeatedList[i] + ", "
                }
            }
        }
    }

    class GeocoderHandler : Handler() {
        override fun handleMessage(msg: Message?) {
            super.handleMessage(msg)
            var address: String = ""
            Logger.print(">>", msg!!.what.toString())


            /* when(msg!!.what){
                 1->{
                     val bundle = msg!!.data
                     address = bundle.getString("address")
                     Logger.print(">>",address)

                 }
             }*/
        }
    }

}
