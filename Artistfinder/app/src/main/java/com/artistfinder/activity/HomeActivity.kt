package com.artistfinder.activity

import android.content.DialogInterface
import android.content.Intent
import android.os.Bundle
import android.view.Gravity
import android.view.View
import android.widget.AdapterView
import androidx.appcompat.app.AlertDialog
import androidx.core.content.ContextCompat
import androidx.drawerlayout.widget.DrawerLayout
import com.android.billingclient.api.BillingClient
import com.android.billingclient.api.Purchase
import com.android.billingclient.api.SkuDetails
import com.android.billingclient.api.SkuDetailsResponseListener
import com.artistfinder.R
import com.artistfinder.adapter.DrawerMenuAdapter
import com.artistfinder.baseclass.BaseActivity
import com.artistfinder.billing.BillingConstants
import com.artistfinder.billing.BillingManager
import com.artistfinder.billing.BillingManager.Companion.BILLING_MANAGER_NOT_INITIALIZED
import com.artistfinder.fragment.*
import com.artistfinder.global.Constant
import com.artistfinder.model.DrawerMenu
import com.artistfinder.model.UserInfo
import com.artistfinder.utils.Logger
import com.artistfinder.utils.SharedPreferenceUtils
import kotlinx.android.synthetic.main.activity_home.*
import kotlinx.android.synthetic.main.layout_menu_header.view.*
import java.util.*
import kotlin.collections.ArrayList


class HomeActivity : BaseActivity(), AdapterView.OnItemClickListener{



    var adapter: DrawerMenuAdapter? = null
    private var drawerMenuArrayList: ArrayList<DrawerMenu>? = null
    private var userInfo: UserInfo? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)
        overridePendingTransition(R.anim.enter, R.anim.exit)
        listMenu.setOnItemClickListener(this)
        userInfo = SharedPreferenceUtils.getInstance(this)
            .getObject(Constant.LOGINUSERDATA, UserInfo::class.java)
        setDrawerAdapter()

        setDrawerListiner()

        setDrawerDetails()
        setFragment(
            ArtistCategoryFragment.newInstance("", ""),
            com.artistfinder.R.id.frContainer,
            false,
            false, false
        )
        if(intent!=null && intent.hasExtra(Constant.DATA)){
            setFragment(
                NotificationFragment.newInstance(true),
                R.id.frContainer,
                true,
                true,true
            )
        }


    }

    override fun onResume() {
        super.onResume()


    }



    private fun setDrawerDetails() {

        if (userInfo != null && userInfo!!.profileImg.isNotEmpty()) {
            includeMenuHeader.svDrawerImage.setImageURI(userInfo!!.profileImg)
        } else {
            includeMenuHeader.svDrawerImage.setActualImageResource(com.artistfinder.R.drawable.img_user)
        }
        if (userInfo != null && userInfo!!.username.isNotEmpty()) {
            includeMenuHeader.svDrawerUserName.text = userInfo!!.username
        }



        includeMenuHeader.svDrawerImage.setOnClickListener {
            setFragment(
                ProfileFragment(),
                R.id.frContainer,
                true,
                true, true
            )
        }
    }

    private fun setDrawerListiner() {
        drawer_layout.setScrimColor(ContextCompat.getColor(this, R.color.transparent))

        drawer_layout.addDrawerListener(object : DrawerLayout.DrawerListener {
            override fun onDrawerStateChanged(newState: Int) {

            }

            override fun onDrawerSlide(drawerView: View, slideOffset: Float) {
                val width = linDrawer.width
                frContainer.translationX = (width * slideOffset)
            }

            override fun onDrawerClosed(drawerView: View) {
            }

            override fun onDrawerOpened(drawerView: View) {

            }
        })

    }



    fun openDrawer() {
        drawer_layout.openDrawer(Gravity.LEFT)
    }

    fun closeDrawer() {
        drawer_layout.closeDrawers()
    }

    fun lockUnlockDrawer(isLocked: Boolean) {
        if (isLocked) {
            drawer_layout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED)
        } else {
            drawer_layout.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED)
        }
    }

    private fun setDrawerAdapter() {
        if (adapter == null) {

            drawerMenuArrayList = ArrayList<DrawerMenu>()
            drawerMenuArrayList = DrawerMenu.userMenu

            adapter = DrawerMenuAdapter(this@HomeActivity, drawerMenuArrayList!!)
            listMenu.setAdapter(adapter)
        } else {
            adapter!!.notifyDataSetChanged()
        }
    }

    override fun onClick(v: View?) {
        super.onClick(v)

    }

    override fun onNewIntent(intent: Intent?) {
        super.onNewIntent(intent)
        if(intent!=null && intent.hasExtra(Constant.DATA)){
            setFragment(
                NotificationFragment.newInstance(true),
                R.id.frContainer,
                true,
                true,true
            )
        }
    }

    override fun onItemClick(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
        closeDrawer()
        when (drawerMenuArrayList!![position].id) {
            0 -> {
                setFragment(
                    ArtistCategoryFragment.newInstance("", ""),
                    com.artistfinder.R.id.frContainer,
                    false,
                    false, false
                )
            }

            1 -> {
                setFragment(
                    BookedAppointmentFragment(),
                    R.id.frContainer,
                    false,
                    false, false
                )
            }

            2 -> {
                setFragment(
                    ApointmnetReqFragment.newInstance(false, ""),
                    R.id.frContainer,
                    false,
                    false, false
                )
            }

            3 -> {
                setFragment(
                    NotificationFragment.newInstance(false),
                    R.id.frContainer,
                    false,
                    false, false
                )
            }

            4 -> {
                setFragment(
                    SettingFragment(),
                    R.id.frContainer,
                    false,
                    false, false
                )
            }

            5 -> {
                showLogoutAlert()
            }

        }
    }


    public fun showLogoutAlert() {

        val builder = AlertDialog.Builder(this)
        builder.setMessage(R.string.dialog_msg_logout)
        builder.setNegativeButton(R.string.alert_btn_cancel,
            DialogInterface.OnClickListener { dialog, which -> })
        builder.setPositiveButton(R.string.alert_btn_logout,
            DialogInterface.OnClickListener { dialog, which ->
                //                Pref.clearPreferance(this@MainActivity)
                SharedPreferenceUtils.getInstance(this).clear()
                val intent = Intent(this@HomeActivity, SplashActivity::class.java)
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK)
                startActivity(intent)
            })
        builder.show()
    }

    override fun onBackPressed() {
        // Temporary add below
        val fm = supportFragmentManager
        if (fm.backStackEntryCount > 0) {
            fm.popBackStack()
        } else {
            super.onBackPressed()
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (getCurrentFrg(R.id.frContainer) is EditProfileFragment) {
            val frag: EditProfileFragment = getCurrentFrg(R.id.frContainer) as EditProfileFragment
            frag.onActivityResult(requestCode, resultCode, data)
        }
        Logger.print(">>", "onActivityResult")
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (getCurrentFrg(R.id.frContainer) is EditProfileFragment) {
            val frag: EditProfileFragment = getCurrentFrg(R.id.frContainer) as EditProfileFragment
            frag.onRequestPermissionsResult(requestCode, permissions, grantResults)
        }
    }

    public override fun onDestroy() {

        super.onDestroy()
    }
}
