package com.artistfinder.activity

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import androidx.databinding.DataBindingUtil
import com.artistfinder.R
import com.artistfinder.baseclass.BaseActivity
import com.artistfinder.databinding.ActivitySettingBinding

class SettingActivity : BaseActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val binding: ActivitySettingBinding = DataBindingUtil.setContentView(this,R.layout.activity_setting);

        binding.txtAboutUs.setOnClickListener(View.OnClickListener {  })
        binding.txtLogout.setOnClickListener(View.OnClickListener {  })
        binding.txtTerms.setOnClickListener(View.OnClickListener {  })
        binding.txtProfile.setOnClickListener(View.OnClickListener {  })
        binding.txtResetPass.setOnClickListener(View.OnClickListener {  })
    }
}
