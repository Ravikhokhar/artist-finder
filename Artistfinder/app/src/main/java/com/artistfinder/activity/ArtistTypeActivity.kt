package com.artistfinder.activity

import android.content.Intent
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import com.artistfinder.R
import com.artistfinder.adapter.ArtistTypeAdapter
import com.artistfinder.baseclass.BaseActivity
import com.artistfinder.global.Constant
import com.artistfinder.listners.OnItemClickListener
import com.artistfinder.model.ArtistCategory
import kotlinx.android.synthetic.main.activity_artist_type.*

class ArtistTypeActivity : BaseActivity(), OnItemClickListener {


    private var dataCategory: ArrayList<ArtistCategory> = ArrayList()
    private var adapter: ArtistTypeAdapter? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_artist_type)
        callForArtistType()
        imgBack.setOnClickListener {

           onBackPressed()
        }

        edSearch.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
                val dataSearch = edSearch.getText().toString() + ""
                if (dataSearch.trim({ it <= ' ' }).length > 0) {
                    setAdapter(searchText(dataSearch.trim({ it <= ' ' })))
                } else {
                    setAdapter(dataCategory!!)
                }
            }

            override fun afterTextChanged(s: Editable) {

            }
        })
    }

    private fun callForArtistType() {
        val map = HashMap<String, String>()
//        map["email"] = txtEmail.text.toString()
//        map["password"] = txtPassword.text.toString()
//        map["fcmToken"] = "ft9Plz2NxpU:APA91bHQhWm-iTk7H3uNWEWCdenCznNpaNkqmSWdTmH9UsYfdND6exwmLHFH8JxsjdCTFmkF-upfH_zSSCK5TRcBm6cFrCgdAcxHhmGzBLwL4S3B3i_kXopStkDGL-SBhRpDAd42tgfA"
//        map["deviceID"] = Settings.Secure.getString(contentResolver, Settings.Secure.ANDROID_ID)
        callAPI(
            Constant.ARTIST_TYPE,
            this,
            Constant.POST_API,
            map,
            true,
            false,
            Constant.ARTIST_TYPE,
            null,
            this
        )
    }

    override fun onResponse(
        MethodName: String,
        responseCode: Int,
        mRes: String,
        responseTag: String
    ) {
        try {
            val commonResponse = getCommonResponse<ArrayList<ArtistCategory>>(mRes)
            setArrayList(this@ArtistTypeActivity, commonResponse!!.result as ArrayList<*>)
            val code = commonResponse.success

            when (responseTag) {
                Constant.ARTIST_TYPE -> {

                    if (code == 1) {

                        dataCategory.addAll(
                            getArrayList(
                                this@ArtistTypeActivity,
                                genericType<ArrayList<ArtistCategory>>()
                            )
                        )


                        runOnUiThread {
                            setAdapter(dataCategory)
                        }
                    }

                }
            }

        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    override fun onViewClicked(v: View, obj: Any, position: Int) {
        when (v.id) {
            R.id.rootMain -> {
                val data = obj as ArtistCategory
                val dataIntent = Intent()
                dataIntent.putExtra("name", data.typeName)
                dataIntent.putExtra("id", data.typeID)
                setResult(105, dataIntent)
                finish()
            }
        }
    }

    private fun setAdapter(dataCategory : ArrayList<ArtistCategory>) {

        if(dataCategory!=null){
            adapter = ArtistTypeAdapter(dataCategory, this, this)
            reyData.adapter = adapter
        }

        /*if (adapter == null) {
            adapter = ArtistTypeAdapter(dataCategory, this, this)
            reyData.adapter = adapter
        } else {
            adapter!!.notifyDataSetChanged()
        }*/
    }

    override fun onRetrofitError(MethodName: String, mError: String, responseTag: Any) {

    }

    private fun searchText(dataSearch: String): java.util.ArrayList<ArtistCategory> {
        val searchList = java.util.ArrayList<ArtistCategory>()
        for (artist in dataCategory!!) {
            if (artist.typeName!!.toLowerCase().contains(dataSearch.toLowerCase())) {
                searchList.add(artist)
            }
        }
        return searchList
    }

    override fun onBackPressed() {
        val dataIntent = Intent()
        dataIntent.putExtra("name", "")
        dataIntent.putExtra("id", -1)
        setResult(105, dataIntent)
        finish()
    }
}
