package com.artistfinder.retrofitclient



import com.artistfinder.global.Constant
import com.artistfinder.global.Constant.Companion.BASE_URL
import okhttp3.*
import okhttp3.logging.HttpLoggingInterceptor

import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.http.*

import java.util.concurrent.TimeUnit


object RetroClient2 {


    private var emergencyContactInterFace: EmergencyContactInterFace? = null
    private var lineBullyPlaceInterface: LineBullyPlaceInterface? = null
    //    private static LineBullyGetAPIInterface lineBullyGETAPIInterface = null;
    private var uploadingFileInterface: UploadingFile? = null
    private val SINGLE_UPLOAD_TIME_OUT = 1000
    private var fileUploadingService: MultipleFileUploadingService? = null


    fun multiFileUploadService(): MultipleFileUploadingService? {
            if (fileUploadingService == null) {

                val interceptor = HttpLoggingInterceptor()

                val okHttpClient = OkHttpClient()
                val clientWith30sTimeout = okHttpClient.newBuilder()
                    .readTimeout(10000, TimeUnit.SECONDS)
                    .writeTimeout(10000, TimeUnit.SECONDS)
                    .connectTimeout(10000, TimeUnit.SECONDS)
                    .retryOnConnectionFailure(true)
                    .addInterceptor(interceptor)
                    .build()

                val client = Retrofit.Builder()
                    .baseUrl(Constant.BASE_URL)
                    .client(clientWith30sTimeout)
                    .addConverterFactory(MultiPartConverter())
                    .build()
                fileUploadingService = client.create(MultipleFileUploadingService::class.java)

            }
            return fileUploadingService
        }


    //https://maps.googleapis.com/maps/api/place/queryautocomplete/json
    fun getAPIClient(): RetroClient2.LineBullyPlaceInterface? {


            if (lineBullyPlaceInterface == null) {

                val interceptor = HttpLoggingInterceptor()
                interceptor.setLevel(HttpLoggingInterceptor.Level.NONE)

                val okHttpClient = OkHttpClient()
                val clientWith30sTimeout = okHttpClient.newBuilder()
                    .readTimeout(10000, TimeUnit.SECONDS)
                    .addInterceptor(interceptor)
                    .build()

                val client = Retrofit.Builder()
                    .baseUrl("https://maps.googleapis.com")
                    .addConverterFactory(ToStringConverter())
                    .client(clientWith30sTimeout)
                    .build()
                lineBullyPlaceInterface = client.create(RetroClient2.LineBullyPlaceInterface::class.java)
            }
            return lineBullyPlaceInterface
        }

    /*public static LineBullyGetAPIInterface getClientForGET(String BASE_URL) {
        try {
            if (lineBullyGETAPIInterface == null) {

                HttpLoggingInterceptor loggingInterceptor = new HttpLoggingInterceptor();
                loggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY);


                OkHttpClient okHttpClient = new OkHttpClient.Builder()
                        .readTimeout(5000, TimeUnit.SECONDS)
                        .connectTimeout(100, TimeUnit.SECONDS)
                        .writeTimeout(5000, TimeUnit.SECONDS)
                        .authenticator(new TokenAuthenticator())
                        .addInterceptor(loggingInterceptor)
                        .build();


                Retrofit retrofit = new Retrofit.Builder()
                        .baseUrl(BASE_URL)
                        .addConverterFactory(GsonConverterFactory.create())
                        .client(okHttpClient)
                        .build();
                lineBullyGETAPIInterface = retrofit.create(LineBullyGetAPIInterface.class);


            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return lineBullyGETAPIInterface;
    }*/

    fun fileUploadService(): UploadingFile?{
            if (uploadingFileInterface == null) {

                val loggingInterceptor = HttpLoggingInterceptor()
                loggingInterceptor.setLevel(HttpLoggingInterceptor.Level.NONE)


                val okHttpClient = OkHttpClient.Builder()
                    .readTimeout(SINGLE_UPLOAD_TIME_OUT.toLong(), TimeUnit.SECONDS)
//                    .authenticator(TokenAuthenticator())
                    .addInterceptor(loggingInterceptor)

                    .build()


                val retrofit = Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .addConverterFactory(ToStringConverter())
                    .client(okHttpClient)
                    .build()
                uploadingFileInterface = retrofit.create(RetroClient2.UploadingFile::class.java)


            }
            return uploadingFileInterface
        }


    fun getClient(BASE_URL: String): EmergencyContactInterFace? {
        try {
            if (emergencyContactInterFace == null) {

                val loggingInterceptor = HttpLoggingInterceptor()
                loggingInterceptor.setLevel(HttpLoggingInterceptor.Level.NONE)


                val okHttpClient = OkHttpClient.Builder()
                    .readTimeout(30000, TimeUnit.SECONDS)
                    .connectTimeout(100, TimeUnit.SECONDS)
                    .writeTimeout(30000, TimeUnit.SECONDS)
//                    .authenticator(TokenAuthenticator())
                    .addInterceptor(loggingInterceptor)
                    .build()


                val retrofit = Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .addConverterFactory(ToStringConverter())
                    .client(okHttpClient)
                    .build()
                emergencyContactInterFace = retrofit.create(EmergencyContactInterFace::class.java)

            }
        } catch (e: Exception) {
            e.printStackTrace()
        }

        return emergencyContactInterFace
    }

    interface UploadingFile// upload image
    /*@Headers({
                "Content-Type:multipart/form-data",
        })*//*@Multipart
        @POST
        Call<ResponseBody> upload(@Url String url, @Part MultipartBody.Part fileBody, @Header(HEADER_KEY) String authorisation,
                                  @PartMap HashMap<String, String> partMap);


        @Multipart
        @POST
        Call<ResponseBody> uploadWithoutImg(@Url String url, @Header(HEADER_KEY) String authorisation,
                                            @PartMap HashMap<String, String> partMap);*/


    interface LineBullyPlaceInterface {
        //        @GET
        //        Call<String> callWebserviceGet(@Url String apiName, @HeaderMap Map<String, String> headers, @QueryMap Map<String, String> params);


        @GET
        fun callWebserviceGet(@Url apiName: String, @QueryMap params: Map<String, String>): Call<String>


    }

    interface EmergencyContactInterFace {


        @FormUrlEncoded
        @POST
        fun getTokenFor(@Url url: String, @HeaderMap headers: Map<String, String>, @FieldMap params: Map<String, String>): Call<String>


        @FormUrlEncoded
        @POST
        fun callWebservice(@Url apiName: String, @HeaderMap headers: Map<String, String>, @FieldMap fields: Map<String, String>): Call<String>

        @GET
        fun callWebserviceGet(@Url apiName: String, @HeaderMap headers: Map<String, String>, @QueryMap params: Map<String, String>): Call<String>

        @FormUrlEncoded
        @PUT
        fun callWebservicePut(@Url apiName: String, @HeaderMap headers: Map<String, String>, @FieldMap params: Map<String, String>): Call<String>

        @DELETE
        fun callWebserviceDelete(@Url apiName: String, @HeaderMap headers: Map<String, String>, @QueryMap params: Map<String, String>): Call<String>


        @Multipart
        @POST
        fun callWebserviceMultipart(
            @Url apiName: String, @HeaderMap headers: Map<String, String>,
            @Part files: List<MultipartBody.Part>, @PartMap params: Map<String, String>
        ): Call<String>


        /*  @Multipart
        @POST
        Call<String> callWebserviceSingleMultipart(@Url String apiName, @HeaderMap Map<String, String> headers,
                                                   @Part MultipartBody.Part fileBody, @PartMap HashMap<String, RequestBody> response);*/

        @Multipart
        @POST
        fun callWebserviceSingleMultipart(
            @Url apiName: String, @HeaderMap headers: Map<String, String>,
            @Part files: MultipartBody.Part, @PartMap params: Map<String, String>
        ): Call<String>


        @Multipart
        @POST("register")
        fun callForSignUp(
//            @Url apiName: String,
            @Part("email")  email : RequestBody,
            @Part("password")  password : RequestBody,
            @Part("name")  name :RequestBody,
            @Part("username")  username : RequestBody,
            @Part("gender")  gender : RequestBody,
            @Part("country")  country : RequestBody,
            @Part("city")  city : RequestBody,
            @Part("address")  address : RequestBody,
            @Part("latlong")  latlong : RequestBody,
            @Part("artistType")  artistType : RequestBody,
            @Part("availableSlot")  availableSlot : RequestBody,
            @Part("phone")  phone : RequestBody,
            @Part files: MultipartBody.Part,
            @Part("other")  other : RequestBody,
            @Part("otherArtist")  otherArtist : RequestBody,
            @Part("deviceType")  deviceType : RequestBody,
            @Part("deviceID")  deviceID : RequestBody,
            @Part("fcmToken")  fcmToken : RequestBody

        ): Call<String>

        @Multipart
        @POST("editProfile")
        fun callForEditProfile(
//            @Url apiName: String,
            @Part("userID")  userID : RequestBody,
            @Part("name")  name :RequestBody,
            @Part("username")  username : RequestBody,
            @Part("gender")  gender : RequestBody,
            @Part("country")  country : RequestBody,
            @Part("city")  city : RequestBody,
            @Part("address")  address : RequestBody,
            @Part("latlong")  latlong : RequestBody,
            @Part("artistType")  artistType : RequestBody,
            @Part("availableSlot")  availableSlot : RequestBody,
            @Part("phone")  phone : RequestBody,
            @Part files: MultipartBody.Part,
            @Part("other")  other : RequestBody,
            @HeaderMap headers: Map<String, String>

        ): Call<String>

        @Multipart
        @POST("addPotfolio")
        fun callForUploadImagesAndVideo(

            @Part("userID")  userID : RequestBody,
            @Part("potfolioType")  potfolioType :RequestBody,
            @Part files:  List<MultipartBody.Part>,
            @HeaderMap headers: Map<String, String>

        ): Call<String>

    }

    interface MultipleFileUploadingService {

        @Multipart
        @POST
        fun postFile(@Url url: String, @Part image: List<MultipartBody.Part>, @HeaderMap headers: Map<String, String>, @PartMap Files: Map<String,@JvmSuppressWildcards RequestBody>): Call<String>

        @Multipart
        @PUT
        fun putFile(@Url url: String, @Part image: List<MultipartBody.Part>, @HeaderMap headers: Map<String, String>, @PartMap Files: Map<String,@JvmSuppressWildcards RequestBody>): Call<String>


        @Multipart
        @POST
        fun postFile(@Url url: String, @HeaderMap headers: Map<String, String>, @PartMap Files: Map<String,@JvmSuppressWildcards RequestBody>): Call<String>


        @Multipart
        @PUT
        fun putFile(@Url url: String, @HeaderMap headers: Map<String, String>, @PartMap Files: Map<String, @JvmSuppressWildcards RequestBody>): Call<String>

    }


    /* public interface  LineBullyGetAPIInterface{

        @Headers({
                "Content-Type:application/json",
        })
        @GET
        Call<ResponseBody> callWebserviceGet(@Url String url, @Header(HEADER_KEY) String authorisation);

        @GET
        Call<String> callWebserviceGet(@Url String url, @QueryMap Map<String, String> params);
    }*/

    /**
     * This class authenticate the request and if token expired then need to refresh that access token and refresh token.
     */
    //from demo test


    /*class TokenAuthenticator : Authenticator {

        @Throws(IOException::class)
        override fun authenticate(route: Route, response: Response): Request? {
            val hashMap =
                HashMap<String, String>()//        mHash.put("refresh_token", "a9f16da0-d2b7-11e8-be05-19659e21faa9");

            hashMap["refresh_token"] = SharedPreferenceUtils.getInstance(MyApplication.context)
                .getStringValue(Constant.REFRESH_TOKEN, APIConst.DEFAULT_REFRESH)
            //            hashMap.put("refresh_token", "a9f16da0-d2b7-11e8-be05-19659e21faa9");
            try {
                Log.d(">>****", "" + response.code())
                Log.d(">>Req", "" + hashMap)

                val jsonRequest = JSONObject()
                val keyIterator = hashMap.keys.iterator()
                while (keyIterator.hasNext()) {
                    val key = keyIterator.next()
                    if (key != "auth_token") {
                        try {
                            jsonRequest.put(key, hashMap[key])
                        } catch (e: JSONException) {
                            e.printStackTrace()
                        }

                    }
                }

                val headers = HashMap<String, String>()
                headers["Content-Type"] = "application/x-www-form-urlencoded"
                headers["android_app_version"] = APP_VERSION
                headers["language"] = "en"
                headers["auth_token"] = SharedPreferenceUtils.getInstance(MyApplication.context)
                    .getStringValue(Constant.AUTH_TOKEN, APIConst.DEFAULT_AUTH)
                //                headers.put("auth_token", "sdmf$@df44sjkdnf#32wer23");


                if (lineBullyAPIInterface == null) {
                    lineBullyAPIInterface = getClient(BASE_URL)
                    Logger.print("interface>>", "null")

                    //same as below  30march test

                    val body = RequestBody.create(
                        okhttp3.MediaType.parse("application/json; charset=utf-8"),
                        jsonRequest.toString()
                    )

                    val callToken = lineBullyAPIInterface!!.getTokenFor(APIConst.REFRESH_TOKEN, headers, hashMap)
                    val tokenResponse = callToken.execute().body()!!.toString()
                    Logger.print("tokenRes>>", tokenResponse)
                    val jsonObject = JSONObject(tokenResponse)
                    val auth_token = jsonObject.getJSONObject("data").getString("auth_token")
                    val refresh_token = jsonObject.getJSONObject("data").getString("refresh_token")
                    SharedPreferenceUtils.getInstance(MyApplication.context).setValue(REFRESH_TOKEN, refresh_token)
                    SharedPreferenceUtils.getInstance(MyApplication.context).setValue(AUTH_TOKEN, auth_token)

                    *//*callToken.enqueue(new Callback<String>() {
                        @Override
                        public void onResponse(Call<String> call, retrofit2.Response<String> response) {

                            try {
                                Logger.print("tokenRes>>", response.body());
                                JSONObject jsonObject = new JSONObject(response.body());
                                String auth_token = jsonObject.getJSONObject("data").getString("auth_token");
                                String refresh_token = jsonObject.getJSONObject("data").getString("refresh_token");
                                SharedPreferenceUtils.getInstance(MyApplication.context).setValue(REFRESH_TOKEN, refresh_token);
                                SharedPreferenceUtils.getInstance(MyApplication.context).setValue(AUTH_TOKEN, auth_token);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }

                        }

                        @Override
                        public void onFailure(Call<String> call, Throwable t) {
                            Logger.print("tokenFail>>");

                        }
                    });*//*
                    //                    String tokenResponse = callToken.execute().body().string();
                    //                    Logger.print("responseToken", ">>" + tokenResponse);
                    *//*try {
                        JSONObject jsonObject = new JSONObject(tokenResponse);
                        String auth_token = jsonObject.getJSONObject("data").getString("auth_token");
                        String refresh_token = jsonObject.getJSONObject("data").getString("refresh_token");
                        SharedPreferenceUtils.getInstance(MyApplication.context).setValue(REFRESH_TOKEN, refresh_token);
                        SharedPreferenceUtils.getInstance(MyApplication.context).setValue(AUTH_TOKEN, auth_token);
//                        Pref.setRefToken(MyApplication.context, refresh_token);
//                        Pref.setAuthToken(MyApplication.context, BEARER + auth_token);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }*//*

                    Log.d("EXPIRE>>", response.request().method() + "   rew:" + response.request().body())
                    return response.request().newBuilder()
                        .method(response.request().method(), response.request().body())
                        .header(
                            "auth_token",
                            SharedPreferenceUtils.getInstance(MyApplication.context).getStringValue(
                                Constant.AUTH_TOKEN,
                                APIConst.DEFAULT_AUTH
                            )
                        )
                        .header("android_app_version", APP_VERSION)
                        .header("language", "en")
                        .build()
                } else {
                    Logger.print("interface>>", "notnull")

                    val body = RequestBody.create(
                        okhttp3.MediaType.parse("application/json; charset=utf-8"),
                        jsonRequest.toString()
                    )

                    val callToken = lineBullyAPIInterface!!.getTokenFor(APIConst.REFRESH_TOKEN, headers, hashMap)
                    val tokenResponse = callToken.execute().body()!!.toString()
                    Logger.print("tokenRes>>", tokenResponse)
                    val jsonObject = JSONObject(tokenResponse)
                    val auth_token = jsonObject.getJSONObject("data").getString("auth_token")
                    val refresh_token = jsonObject.getJSONObject("data").getString("refresh_token")
                    SharedPreferenceUtils.getInstance(MyApplication.context).setValue(REFRESH_TOKEN, refresh_token)
                    SharedPreferenceUtils.getInstance(MyApplication.context).setValue(AUTH_TOKEN, auth_token)
                    *//*callToken.enqueue(new Callback<String>() {
                        @Override
                        public void onResponse(Call<String> call, retrofit2.Response<String> response) {
                            try {
                                Logger.print("tokenRes>>", response.body());
                                JSONObject jsonObject = new JSONObject(response.body());
                                String auth_token = jsonObject.getJSONObject("data").getString("auth_token");
                                String refresh_token = jsonObject.getJSONObject("data").getString("refresh_token");
                                SharedPreferenceUtils.getInstance(MyApplication.context).setValue(REFRESH_TOKEN, refresh_token);
                                SharedPreferenceUtils.getInstance(MyApplication.context).setValue(AUTH_TOKEN, auth_token);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }

                        }

                        @Override
                        public void onFailure(Call<String> call, Throwable t) {
                            Logger.print("tokenFail>>");

                        }
                    });*//*

                    *//*RequestBody body = RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8"), jsonRequest.toString());

                    Call<ResponseBody> callToken = lineBullyAPIInterface.getTokenFor(APIConst.REFRESH_TOKEN, body);
                    String tokenResponse = callToken.execute().body().string();

                    Logger.print("responseToken", ">>" + tokenResponse);
                    try {
                        JSONObject jsonObject = new JSONObject(tokenResponse);
                        String auth_token = jsonObject.getJSONObject("data").getString("auth_token");
                        String refresh_token = jsonObject.getJSONObject("data").getString("refresh_token");
//                        Pref.setRefToken(MyApplication.context, refresh_token);
//                        Pref.setAuthToken(MyApplication.context, BEARER + auth_token);
                        SharedPreferenceUtils.getInstance(MyApplication.context).setValue(REFRESH_TOKEN, refresh_token);
                        SharedPreferenceUtils.getInstance(MyApplication.context).setValue(AUTH_TOKEN, auth_token);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
*//*
                    Log.d("EXPIRE>>", response.request().method() + "   rew:" + response.request().body())
                    return response.request().newBuilder()
                        .method(response.request().method(), response.request().body())
                        .header(
                            "auth_token",
                            SharedPreferenceUtils.getInstance(MyApplication.context).getStringValue(
                                Constant.AUTH_TOKEN,
                                APIConst.DEFAULT_AUTH
                            )
                        )
                        .header("android_app_version", APP_VERSION)
                        .header("language", "en")
                        .build()
                }
            } catch (e: Exception) {
                e.printStackTrace()
            }

            return null
        }
    }*/
}
