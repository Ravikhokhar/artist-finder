package com.artistfinder.retrofitclient

/**
 * Created by tristateandroid2 on 26/2/18.
 */
interface RetroFitResponse {

    /**
     * @param mRes         means the response of webService
     * @param responseTag  means It display response of which Web Method .
     * @param responseCode send response code if 200 then response ok , if 404 then Error.
     */
    fun onResponse(MethodName: String, responseCode: Int, mRes: String, responseTag: String)

    /**
     * @param Code        means the Error code 404  "there is something going wrong "
     * @param responseTag means It display Error of which Web Method .
     * @param mError      send response "Error" when Error.
     */
    fun onRetrofitError(MethodName: String, mError: String, responseTag: Any)


}