package com.artistfinder.retrofitclient;

/**
 * Created by TristateAndroi on 9/9/2017.
 */

public interface onCallBackResponseListener {
    /**
     * On success response of server
     * @param response response string
     * @param methodName method name of web service.
     */
    void onSuccessCall(String response, String methodName);

    /**
     * On error response of server
     * @param error error message
     * @param eventName method name
     */
    void onErrorCall(String error, String eventName);
}
