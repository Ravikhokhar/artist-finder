package com.artistfinder.listners

import com.vincent.filepicker.filter.entity.ImageFile
import com.vincent.filepicker.filter.entity.VideoFile

interface OnMultiMediaListener {
    fun onMultiImageListener(imageList: ArrayList<ImageFile>)
    fun onMultiVideoListener(videoList: ArrayList<VideoFile>)
}