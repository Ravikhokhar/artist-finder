package com.artistfinder.listners

import android.view.View
import java.util.*

interface OnItemClickListener{
    fun onViewClicked(v : View,obj : Any ,position : Int)
}