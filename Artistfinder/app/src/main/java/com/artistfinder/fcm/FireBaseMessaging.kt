package com.artistfinder.fcm

import android.app.Notification
import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.os.Build
import android.os.Bundle
import androidx.core.app.NotificationCompat
import androidx.core.content.ContextCompat
import com.artistfinder.R
import com.artistfinder.activity.HomeActivity
import com.artistfinder.global.Constant
import com.artistfinder.utils.Logger
import com.artistfinder.utils.SharedPreferenceUtils
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage
import org.json.JSONObject


class FireBaseMessaging : FirebaseMessagingService(){

    override fun onMessageReceived(remoteMessage: RemoteMessage?) {
        super.onMessageReceived(remoteMessage)
        try {
            //        Log.d(TAG, "From: " + remoteMessage.getFrom());
            //        Check if message contains a data payload.
            if (remoteMessage!!.getData().size > 0) {
                Logger.print("Notification>>",remoteMessage!!.data.toString())

                if (!remoteMessage.getData().containsKey("desc")) {
                    return
                }
                val message = remoteMessage.data["desc"]
                val title = remoteMessage.data["title"]

//                val title = remoteMessage.data["text"]
                var type = 0


                val bundle = Bundle()
                /*if (remoteMessage.getData().containsKey("entity_id")) {
                    bundle.putString("entityId", remoteMessage.getData()["entity_id"])
                }
                if (remoteMessage.getData().containsKey("notification_id")) {
                    bundle.putString("notificationId", remoteMessage.getData()["notification_id"])
                }*/
                if (remoteMessage.getData().containsKey("notify_type")) {
                    type = (remoteMessage.getData()["notify_type"]!!).toInt()
                }
//                bundle.putBoolean("isfrompush", true)

                makeNotificationForNewMsg(applicationContext, title, message,type, HomeActivity::class.java, bundle)
            }

            //             Check if message contains a notification payload.
            if (remoteMessage.getNotification() != null) {
                Logger.print("Message Notification Body: " , remoteMessage.getNotification()!!.body!!)
                //            makeNotificationForNewMsg(getApplicationContext(), remoteMessage.getNotification().getBody(), HomeActivity.class, null);
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    override fun onNewToken(deviceToken: String?) {
        super.onNewToken(deviceToken)
       Logger.print("DeviceToken>>",deviceToken.toString())
        SharedPreferenceUtils.getInstance(this).setValue(Constant.DEVICETOKEN,deviceToken!!)
    }

    fun makeNotificationForNewMsg(context: Context, title: String?, msg: String?,type : Int?, cls: Class<*>, bundle: Bundle?) {
        var title = title
        var msg = msg
        if (SharedPreferenceUtils.getInstance(context).getStringValue(Constant.IS_USER_STATUS, "").equals(Constant.STATUS_LOGIN_SUCCESS)) {
            //Logger.print("LocalNotification", jsonObject.toString());
            val intent = Intent(context, cls)
            intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TOP
            intent.putExtra(Constant.DATA,type)
            if (bundle != null) {
                intent.putExtras(bundle)
            }

            Logger.print("generating>>","title:$title and  msd:$msg")

            val chatIntent = PendingIntent.getActivity(
                context, 0,
                intent, PendingIntent.FLAG_UPDATE_CURRENT
            )

            val notificationManager = context.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager

            if (title == null || title == "") {
                title = "Artist Finder"
            }

            if (msg == null || msg == "") {
                msg = "You have received a new message"
            }

            var bigTextStyle = NotificationCompat.BigTextStyle()
                .bigText(msg)


            var builder: NotificationCompat.Builder? = null
            builder = NotificationCompat.Builder(context,"ArtistFinder")
                .setStyle(bigTextStyle)
                .setContentTitle(title)
                .setWhen(System.currentTimeMillis())
                .setDefaults(Notification.DEFAULT_ALL)
                .setContentIntent(chatIntent)
                .setAutoCancel(true)

            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.JELLY_BEAN_MR1) {
                builder!!.setShowWhen(true)
            }

            if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                builder!!.setColor(ContextCompat.getColor(context, R.color.color_yellow))
                builder.setSmallIcon(R.drawable.ic_logo)
            } else {
                builder!!.setSmallIcon(R.drawable.ic_logo)
            }

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                // Create the NotificationChannel, but only on API 26+ because
                // the NotificationChannel class is new and not in the support library
                val name = context.getString(R.string.app_name)
                val importance = NotificationManager.IMPORTANCE_DEFAULT
                val channel = NotificationChannel("ArtistFinder", name, importance)
                //                    channel.setDescription(description);
                // Register the channel with the system
                notificationManager?.createNotificationChannel(channel)
                builder.setChannelId("ArtistFinder")
            }
            val notification = builder.build()
            notificationManager?.notify(System.currentTimeMillis().toInt(), notification)
        }
    }
}