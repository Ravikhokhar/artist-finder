package com.artistfinder.global


interface Constant {
    companion object {

        val DATA: String = "data"
        val APP_NAME = "Artist Finder"
        //                val BASE_URL = "http://192.168.1.13:3000"  //local
//        val BASE_URL = "http://54.219.172.96:3007"  //live
//        val BASE_URL = "http://18.136.220.134:3007"  // client live
        val BASE_URL = "http://3.17.208.36/appointment/index.php/api/"  // app live
        val URL_PRIVACY_POLICY_BLACK = "http://3.17.208.36/appointment/index.php/privacy?theme=black"
        val URL_PRIVACY_POLICY_WHITE = "http://3.17.208.36/appointment/index.php/privacy?theme=white"
        val URL_TERMS_CONDITION_BLACK = "http://3.17.208.36/appointment/index.php/terms?theme=black"
        val URL_TERMS_CONDITION_WHITE = "http://3.17.208.36/appointment/index.php/terms?theme=white"
        const val RELATIVE_URL = ""

        val MESSAGE_CHECK_INTERNET_CONNECTION = "Please check your internet connection"

        val DEFAULT_REFRESH_TOKEN = "E7CF5A3520C022PLI4FDC800E5FEAB8F347CE"
        val GOOGLE_MAP_APIKEY = "AIzaSyD6sdL135cWA7MR2cP1MeaN5ui7KzAysaI"

        val GET_API = 1
        val POST_API = 2
        val PUT_API = 3
        val DELETE_API = 4

        val TYPE_ROW_LOADING = 1
        val DEVICETOKEN = "device_token"


        val SKU_ID = "com.artistfinder.testplan"


        //Pref
        val IS_DARK_MODE = "isDarkModeEnable"


        val FORMAT1 = "HH:mm"
        val FORMAT2 = "hh:mm a"
        val FORMAT3 = "dd/MM/yyyy"
        val FORMAT4 = "yyyy-MM-dd"
        val FORMAT5 = "dd MMM, yyyy"
        val FORMAT6 = "hh:mm:ss a"
        val FORMAT7 = "dd MMM, yyyy hh:mm a"

        val DEVICE_TYPE = "2"

        //Prefrences key
        val AUTH_TOKEN = "auth_token"
        val REFRESH_TOKEN = "refersh_token"
        val IS_USER_STATUS = "is_login"
        val IS_USER_SUBSCRIBE = "IS_USER_SUBSCRIBE"
        val LOGINUSERDATA = "userData"
        val SIGNUP_DATA = "SIGNUP_DATA"
        //        val IS_REMEMBER = "save_email"
        val DELAY: Long = 1000
        val STATUS_OTP = "1"
        val STATUS_LOGIN_SUCCESS = "2"

        //Brodcast Action
        val CLICK_TIME_INTERVAL: Long = 300
        val APPOINTMENT_BOOKED: String = "appointment_booked"
        val SUBSCRIPTION_DATA = "subscription"

        //Api
        val REFRESH_TOKEN_API = "refreshToken"
        val GET_PROMOTION_FEED = "getPromotionList"
        val GET_EMERG_SERVICE = "getEmegencyServiceList"
        val GET_SERVICE = "getServiceList"
        val GET_SERVICE_PROVIDER = "getServiceProviderList"
        val LOGIN = "login"
        val GET_PROFILE = "getUserProfile"
        val LOGOUT = "logout"
        val REGISTRATION = "register"
        val CHANGE_PASSWORD = "changePassword"
        val EDIT_USER_PROFILE = "editUserProfile"
        val FORGOT_PASS = "forgotPassword"
        val ARTIST_TYPE = "artistTypes"
        val ARTIST_LISTING = "artistListing"
        var ARTIST_DETAIL = "artistData"
        val SEND_OTP = "sendOtp"
        val ADD_PORTFOLIO = "addPotfolio"
        val REMOVE_PORTFOLIO = "removePotfolio"
        val GETAPPOINTMENT = "getAppointment"
        val APPOINTMENT_REQUEST = "appointmentRequest"
        val BOOK_APOINTMENT = "bookAppointment"
        val GETSENT_APPOINTMENT = "getSentAppointmentList"
        val GET_NOTIFICATION = "getNotification"
        val GET_FREE_SHOT = "getFreeSlot"
        val IN_APP_PURCHASE = "InAppStatusAndroid"

        val MAP_SHARE = "https://www.google.com/maps/search/?api=1&query="
        val WHATSUP_MSG = "https://api.whatsapp.com/send?phone="
        val PLAYSTORE_APP = "https://play.google.com/store/apps/details?id=com.onglai"
        val MEDIA_IMG = "IMG"
        val MEDIA_VID = "VID"
        val MEDIA_TYPE = "media_type"
        val MEDIA_URL = "media_url"

        val BUNDLE_URL = "bundle_url"
    }
}