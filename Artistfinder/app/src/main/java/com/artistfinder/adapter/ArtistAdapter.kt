package com.artistfinder.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.artistfinder.R
import com.artistfinder.listners.OnItemClickListener
import com.artistfinder.model.Artist
import kotlinx.android.synthetic.main.item_artist.view.*

class ArtistAdapter(
    val context: Context,
    var artistList: ArrayList<Artist>,
    val listner: OnItemClickListener
) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    public fun updateData(list: ArrayList<Artist>)
    {
        artistList = list
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return ArtistCategoryAdapter.ArtistViewHolder(
            LayoutInflater.from(parent.context).inflate(
                R.layout.item_artist,
                parent,
                false
            )
        )
    }


    override fun getItemCount(): Int {
        return artistList.size
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val viewHolder = holder as ArtistViewHolder

        holder.itemView.imgProfile.setImageURI(artistList[position].profileImg)
        holder.itemView.txtName.setText(artistList[position].name)
        holder.itemView.imgProfile.setOnClickListener {
            listner.onViewClicked(it, artistList[position], position)
        }
    }

    class ArtistViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

    }
}