package com.artistfinder.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.artistfinder.R
import com.artistfinder.activity.HomeActivity
import com.artistfinder.listners.OnItemClickListener
import com.artistfinder.model.Artist
import com.artistfinder.model.ArtistCategory
import kotlinx.android.synthetic.main.item_artist_category.view.*

class ArtistCategoryAdapter(val arrayList: ArrayList<ArtistCategory>,val context : Context,val listner : OnItemClickListener) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return ArtistViewHolder(
            LayoutInflater.from(parent.context).inflate(
                R.layout.item_artist_category,
                parent,
                false
            )
        )
    }

    override fun getItemCount(): Int {
        return arrayList.size
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        holder.itemView.ivCategory.setImageURI(arrayList[position].typeImg)
        holder.itemView.tvCategory.text = arrayList[position].typeName
        holder.itemView.ivCategory.setOnClickListener {
            listner.onViewClicked(it,arrayList[position],position)
        }

        holder.itemView.rootView.layoutParams.width = (context as HomeActivity).getDisplayWidth()/2
//        holder.itemView.ivCategory.layoutParams.height = (context as HomeActivity).getDisplayWidth()/2

    }

    class ArtistViewHolder(view: View) : RecyclerView.ViewHolder(view) {

    }
}