package com.artistfinder.adapter

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.artistfinder.R
import com.artistfinder.activity.MediaViewActivity
import com.artistfinder.global.Constant
import com.artistfinder.listners.OnItemClickListener
import com.artistfinder.model.Photos
import com.artistfinder.utils.Utils
import com.vincent.filepicker.Util
import kotlinx.android.synthetic.main.item_photo_video.view.*
import java.io.File

class PhotosAdapter(
    val context: Context,
    private val photosList: ArrayList<Photos>,
    private val listner: OnItemClickListener,
    private val fromProfile: Boolean,
    private val isFromEditScreen: Boolean
) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return ArtistViewHolder(
            LayoutInflater.from(parent.context).inflate(
                R.layout.item_photo_video,
                parent,
                false
            )
        )
    }

    override fun getItemCount(): Int {
        return photosList.size
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val viewHolder = holder as ArtistViewHolder
        holder.itemView.imageViewAdd.tag = Constant.MEDIA_IMG
        holder.itemView.imageViewAdd.visibility = View.GONE
        holder.itemView.imgProfile.visibility = View.VISIBLE

        if (isFromEditScreen) {
            holder.itemView.imageViewClose.visibility = View.VISIBLE
        } else {
            holder.itemView.imageViewClose.visibility = View.GONE
        }

        if (!fromProfile && itemCount - 1 == holder.adapterPosition && photosList[holder.adapterPosition].isAddImageView) {
            holder.itemView.imageViewClose.visibility = View.GONE
            holder.itemView.imgProfile.visibility = View.GONE
            holder.itemView.imageViewAdd.visibility = View.VISIBLE
        } else {
            if (photosList[holder.adapterPosition].potfolioPath!= null && photosList[holder.adapterPosition].potfolioPath!!.isNotEmpty() && Utils.checkStringContainURL(photosList[position].potfolioPath!!)){
                holder.itemView.imgProfile.setImageURI(
                    (photosList[holder.adapterPosition].potfolioPath),
                    context
                )
            }else {
                holder.itemView.imgProfile.setImageURI(
                    (Uri.fromFile(File(photosList[holder.adapterPosition].potfolioPath))),
                    context
                )
            }
        }

        holder.itemView.imgProfile.setOnClickListener {
            val intent: Intent = Intent(context, MediaViewActivity::class.java)
            intent.putExtra(Constant.MEDIA_TYPE,Constant.MEDIA_IMG)
            intent.putExtra(Constant.MEDIA_URL,photosList[position].potfolioPath)
            context.startActivity(intent)
        }

        holder.itemView.imageViewClose.tag = Constant.MEDIA_IMG
        holder.itemView.imageViewClose.setOnClickListener {
            listner.onViewClicked(it, photosList[position], position)
        }

        holder.itemView.imageViewAdd.setOnClickListener {

            listner.onViewClicked(it, photosList[position], position)
        }
    }

    class ArtistViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

    }
}