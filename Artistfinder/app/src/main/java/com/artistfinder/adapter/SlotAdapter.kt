package com.artistfinder.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.artistfinder.R
import com.artistfinder.listners.OnItemClickListener
import kotlinx.android.synthetic.main.item_slot.view.*
import org.json.JSONArray

class SlotAdapter(
    val context: Context,
    var data : JSONArray?,
    val listner: OnItemClickListener?
) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    public var selPos  : Int? =-1;
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return ArtistCategoryAdapter.ArtistViewHolder(
            LayoutInflater.from(parent.context).inflate(
                R.layout.item_slot,
                parent,
                false
            )
        )
    }


    override fun getItemCount(): Int {
        return data?.length()!!
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val viewHolder = holder as ArtistViewHolder

        if(selPos!=-1 && selPos == position){
            holder.itemView.tvSlot.setBackgroundResource(R.drawable.bg_button_rectangle)
        }else{
            holder.itemView.tvSlot.setBackgroundResource(R.drawable.border_back)
        }

        holder.itemView.tvSlot.setText(data?.get(position).toString())
        holder.itemView.tvSlot.setOnClickListener {
            if(selPos!=-1 && selPos == position){
                selPos = -1
            }else{
              selPos = position
            }
            notifyDataSetChanged()
            listner?.onViewClicked(it, data?.get(position).toString(), position)
        }
    }

    class ArtistViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

    }
}