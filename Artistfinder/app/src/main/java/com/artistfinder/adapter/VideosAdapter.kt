package com.artistfinder.adapter

import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.media.MediaMetadataRetriever
import android.net.Uri
import android.os.AsyncTask
import android.os.Build
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.recyclerview.widget.RecyclerView
import com.artistfinder.R
import com.artistfinder.activity.MediaViewActivity
import com.artistfinder.global.Constant
import com.artistfinder.listners.OnItemClickListener
import com.artistfinder.model.Video
import com.artistfinder.utils.Utils
import com.facebook.drawee.view.SimpleDraweeView
import kotlinx.android.synthetic.main.item_artist.view.imgProfile
import kotlinx.android.synthetic.main.item_photo_video.view.*
import java.io.File
import java.lang.ref.WeakReference

class VideosAdapter(
    val context: Context,
    private val videoList: ArrayList<Video>,
    private val listner: OnItemClickListener,
    private val fromProfile: Boolean,
    private val isFromEditScreen: Boolean
) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return ArtistViewHolder(
            LayoutInflater.from(parent.context).inflate(
                R.layout.item_photo_video,
                parent,
                false
            )
        )
    }

    override fun getItemCount(): Int {
        return videoList.size
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val viewHolder = holder as ArtistViewHolder
        holder.itemView.imageViewAdd.tag = Constant.MEDIA_VID
        holder.itemView.imageViewAdd.visibility = View.GONE
        holder.itemView.imgProfile.visibility = View.VISIBLE

        if (isFromEditScreen) {
            holder.itemView.imageViewClose.visibility = View.VISIBLE
        } else {
            holder.itemView.imageViewClose.visibility = View.GONE
        }

        if (!fromProfile && itemCount - 1 == position && videoList[holder.adapterPosition].isAddImageView) {
            holder.itemView.imageViewClose.visibility = View.GONE
            holder.itemView.imgProfile.visibility = View.GONE
            holder.itemView.ivPlay.visibility = View.GONE
            holder.itemView.imageViewAdd.visibility = View.VISIBLE


        } else {
            holder.itemView.ivPlay.visibility = View.VISIBLE

//            val bitmap: Bitmap = MediaController.getInstance().retriveVideoFrameFromVideo(videoList[position].potfolioPath)
//            holder.itemView.imgProfile.setImageBitmap(bitmap)
            if (videoList[viewHolder.adapterPosition].potfolioPath!!.isNotEmpty() && Utils.checkStringContainURL(
                    videoList[viewHolder.adapterPosition].potfolioPath!!
                )
            ) {
                LoadPictureTask(holder.itemView.imgProfile).execute(videoList[viewHolder.adapterPosition].potfolioPath)
//                holder.itemView.imgProfile.setImageURI(videoList[position].potfolioPath, context)
            } else {
                if (videoList[viewHolder.adapterPosition].potfolioPath!!.isNotEmpty()) {
                    holder.itemView.imgProfile.setImageURI(
                        (Uri.fromFile(File(videoList[viewHolder.adapterPosition].potfolioPath))),
                        context
                    )
                }
            }

        }
        holder.itemView.imgProfile.setOnClickListener {
            val intent: Intent = Intent(context, MediaViewActivity::class.java)
            intent.putExtra(Constant.MEDIA_TYPE, Constant.MEDIA_VID)
            intent.putExtra(Constant.MEDIA_URL, videoList[position].potfolioPath)
            context.startActivity(intent)
        }

        holder.itemView.imageViewClose.tag = Constant.MEDIA_VID
        holder.itemView.imageViewClose.setOnClickListener {

            listner.onViewClicked(it, videoList[position], position)
        }

        holder.itemView.imageViewAdd.setOnClickListener {

            listner.onViewClicked(it, videoList[position], position)
        }
    }

    class ArtistViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

    }

    internal inner class LoadPictureTask(imageView: SimpleDraweeView) :
        AsyncTask<String, Void, Bitmap>() {
        private val mImageViewReference: WeakReference<SimpleDraweeView>

        init {
            // Use a WeakReference to ensure the ImageView can be garbage collected
            mImageViewReference = WeakReference(imageView)
        }

        override fun doInBackground(vararg params: String): Bitmap {
            val path = params[0]
            return decodeSampledBitmapFromFile(path)
        }

        override fun onPostExecute(bitmap: Bitmap?) {
            if (mImageViewReference.get() != null && bitmap != null) {
                val imageView = mImageViewReference.get()
                if (imageView != null) {
                    imageView!!.setImageBitmap(bitmap)
                    imageView!!.setVisibility(View.VISIBLE)
                    imageView.scaleType = ImageView.ScaleType.CENTER_CROP
                }
            }
        }

        private fun decodeSampledBitmapFromFile(
            videoPath: String
        ): Bitmap {

            var bitmap: Bitmap? = null
            var mediaMetadataRetriever: MediaMetadataRetriever? = null
            try {
                mediaMetadataRetriever = MediaMetadataRetriever()
                if (Build.VERSION.SDK_INT >= 14)
                    mediaMetadataRetriever.setDataSource(videoPath, HashMap())
                else
                    mediaMetadataRetriever.setDataSource(videoPath)
                //   mediaMetadataRetriever.setDataSource(videoPath);
                bitmap = mediaMetadataRetriever.frameAtTime
            } catch (e: Exception) {
                e.printStackTrace()
                throw Throwable("Exception in retriveVideoFrameFromVideo(String videoPath)" + e.message)

            } finally {
                mediaMetadataRetriever?.release()
            }
            return bitmap!!
        }

    }
}