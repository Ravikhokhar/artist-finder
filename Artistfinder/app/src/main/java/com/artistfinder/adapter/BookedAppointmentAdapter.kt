package com.artistfinder.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.artistfinder.R
import com.artistfinder.listners.OnItemClickListener
import com.artistfinder.model.BookedAppointment
import kotlinx.android.synthetic.main.item_artist_category.view.*
import kotlinx.android.synthetic.main.item_booked_appointment.view.*


class BookedAppointmentAdapter(
    val context: Context,
    var bookedAppointmentList: ArrayList<BookedAppointment>,
    val listner: OnItemClickListener
) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    public fun updateData(list: ArrayList<BookedAppointment>)
    {
        bookedAppointmentList = list
        notifyDataSetChanged()
    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return AppointmentViewHolder(
            LayoutInflater.from(parent.context).inflate(
                R.layout.item_booked_appointment,
                parent,
                false
            )
        )
    }

    override fun getItemCount(): Int {
//        return artistList.size
        return bookedAppointmentList.size
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val viewHolder = holder as AppointmentViewHolder
        viewHolder.itemView.ivProfile.setImageURI(bookedAppointmentList[position].profileImg)
        viewHolder.itemView.tvTitleName.text = bookedAppointmentList[position].name
        viewHolder.itemView.tvDate.text = "Date : " +  bookedAppointmentList[position].date
        viewHolder.itemView.tvAddress.text = "Notes : " + bookedAppointmentList[position].notes
        viewHolder.itemView.tvPhoneNumber.text ="Time : " + bookedAppointmentList[position].time

        holder.itemView.rootView.setOnClickListener {
            listner.onViewClicked(it,bookedAppointmentList[position],position)
        }
    }

    class AppointmentViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

    }
}