package com.artistfinder.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.artistfinder.R
import com.artistfinder.model.BookedAppointment
import com.artistfinder.model.Notification
import kotlinx.android.synthetic.main.item_appointment_req.view.*
import kotlinx.android.synthetic.main.item_artist_category.view.*
import kotlinx.android.synthetic.main.item_booked_appointment.view.*
import kotlinx.android.synthetic.main.item_booked_appointment.view.ivProfile
import kotlinx.android.synthetic.main.item_booked_appointment.view.tvDate
import kotlinx.android.synthetic.main.item_booked_appointment.view.tvTitleName
import kotlinx.android.synthetic.main.item_notification.view.*

class AppointMentReqAdapter(
    val context: Context, var appointmentlist: ArrayList<BookedAppointment>)
    : RecyclerView.Adapter<RecyclerView.ViewHolder>() {


    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        holder.itemView.ivProfile.setImageURI(appointmentlist[position].profileImg)
        holder.itemView.tvTitleName.text = appointmentlist[position].name
        holder.itemView.tvDate.text = "Date : " +  appointmentlist[position].date
        holder.itemView.tvNote.text = "Notes : " + appointmentlist[position].notes
        holder.itemView.tvTime.text ="Time : " + appointmentlist[position].time
    }

    override fun getItemCount(): Int {
        return appointmentlist.size
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return AppointMentHolder(
            LayoutInflater.from(parent.context).inflate(
                R.layout.item_appointment_req,
                parent,
                false
            )
        )
    }
}

class AppointMentHolder(view: View) : RecyclerView.ViewHolder(view) {

}


