package com.artistfinder.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.artistfinder.R
import com.artistfinder.listners.OnItemClickListener
import com.artistfinder.model.Artist
import com.artistfinder.model.ArtistCategory
import kotlinx.android.synthetic.main.item_artist_type.view.*

class ArtistTypeAdapter(var arrayList: ArrayList<ArtistCategory>, val context : Context, val listner : OnItemClickListener) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {



    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return ArtistViewHolder(
            LayoutInflater.from(parent.context).inflate(
                R.layout.item_artist_type,
                parent,
                false
            )
        )
    }

    override fun getItemCount(): Int {
        return arrayList.size
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        holder.itemView.ivCategory.setImageURI(arrayList[position].typeImg)
        holder.itemView.tvCategory.text = arrayList[position].typeName
        holder.itemView.rootMain.setOnClickListener {
            listner.onViewClicked(it,arrayList[position],position)
        }


    }

    class ArtistViewHolder(view: View) : RecyclerView.ViewHolder(view) {

    }
}