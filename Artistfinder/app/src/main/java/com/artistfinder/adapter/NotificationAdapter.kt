package com.artistfinder.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.artistfinder.R
import com.artistfinder.listners.OnItemClickListener
import com.artistfinder.model.Notification
import kotlinx.android.synthetic.main.item_notification.view.*

class NotificationAdapter(
    val context: Context,
    val notificationList: ArrayList<Notification>,
    val itemClickListener : OnItemClickListener
) : RecyclerView.Adapter<RecyclerView.ViewHolder>()
{

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        holder.itemView.txtTitle.text = notificationList[position].name
        holder.itemView.txtNotification.text = notificationList[position].notes
        holder.itemView.txtTime.text = "At "+notificationList[position].date+" On "+notificationList[position].time
        holder.itemView.imgProfile.setImageURI(notificationList[position].profileImg)
        holder.itemView.rootMain.setOnClickListener {
            itemClickListener.onViewClicked(it,notificationList[position],position)
        }

    }

    override fun getItemCount(): Int {
        return notificationList.size
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return NotificationViewHolder(
            LayoutInflater.from(parent.context).inflate(
                R.layout.item_notification,
                parent,
                false
            )
        )
    }
}

class NotificationViewHolder(view: View) : RecyclerView.ViewHolder(view) {

}


