package com.artistfinder.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import com.artistfinder.R
import com.artistfinder.model.DrawerMenu
import kotlinx.android.synthetic.main.item_drawer.view.*
import java.util.ArrayList


class DrawerMenuAdapter(internal var context: Context, myList: ArrayList<DrawerMenu>) : BaseAdapter() {

    internal var myList: ArrayList<DrawerMenu>
    internal var inflater: LayoutInflater


    init {
        this.myList = myList
        inflater = LayoutInflater.from(this.context)
    }

    override fun getCount(): Int {
        return myList.size
    }

    override fun getItem(position: Int): DrawerMenu {
        return myList[position]
    }

    override fun getItemId(position: Int): Long {
        return 0
    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        var convertView = convertView
        val mViewHolder: MyViewHolder

        if (convertView == null) {
            convertView = inflater.inflate(R.layout.item_drawer, parent, false)
            mViewHolder = MyViewHolder(convertView)
            convertView!!.setTag(mViewHolder)
        } else {
            mViewHolder = convertView.tag as MyViewHolder
        }

        val currentListData = getItem(position)

        //mViewHolder.binding.ivMenu.
        mViewHolder.itemView!!.tvName.text = currentListData.name
        mViewHolder.itemView!!.icDrawer.setImageResource(currentListData.iconId)
        /*if (!currentListData.isSelected()) {
            mViewHolder.binding.ivMenu.setImageDrawable(
                context.resources.getDrawable(
                    currentListData.getIconId()
                )
            )
            mViewHolder.binding.tvMenuName.setTextColor(context.resources.getColor(R.color.gray_blue))
        } else {
            mViewHolder.binding.ivMenu.setImageDrawable(
                context.resources.getDrawable(
                    currentListData.getIconIdSelected()
                )
            )
            mViewHolder.binding.tvMenuName.setTextColor(context.resources.getColor(R.color.yellow))
        }*/


        return convertView
    }

    private inner class MyViewHolder(item: View) {

        internal var itemView: View?=null

        init {
            itemView = item
        }
    }
}
