package com.artistfinder.baseclass

import android.app.Activity
import android.content.Context
import android.graphics.drawable.Drawable
import android.net.ConnectivityManager
import android.os.Bundle
import android.util.DisplayMetrics
import android.util.TypedValue
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.app.AppCompatDelegate
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import com.artistfinder.BuildConfig
import com.artistfinder.R
import com.artistfinder.dialog.CustomLoaderDialog
import com.artistfinder.global.Constant
import com.artistfinder.global.Constant.Companion.AUTH_TOKEN
import com.artistfinder.global.Constant.Companion.BASE_URL
import com.artistfinder.model.CommonResponse
import com.artistfinder.retrofitclient.RetroClient2
import com.artistfinder.retrofitclient.RetroFitResponse
import com.artistfinder.utils.Logger
import com.artistfinder.utils.MyApplication.Companion.context
import com.artistfinder.utils.SharedPreferenceUtils
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import okhttp3.MultipartBody
import okhttp3.RequestBody
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.lang.reflect.Type
import java.util.*
import kotlin.collections.ArrayList


open class BaseActivity : AppCompatActivity(), Constant, RetroFitResponse, View.OnClickListener {

    internal var res: Response<String>? = null

    override fun onRetrofitError(MethodName: String, mError: String, responseTag: Any) {
    }

    override fun onResponse(
        MethodName: String,
        responseCode: Int,
        mRes: String,
        responseTag: String
    ) {
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (AppCompatDelegate.getDefaultNightMode() == AppCompatDelegate.MODE_NIGHT_YES) {
            setTheme(R.style.DarkTheme);
        } else {
            setTheme(R.style.LightTheme);
        }

    }

    private var mLastClickTime = System.currentTimeMillis()
    var cm: CustomLoaderDialog? = null

    override fun onClick(v: View?) {
    }


    fun printToastAlert(context: Context, id: Int) {
        try {
            Toast.makeText(context, "" + context.resources.getString(id), Toast.LENGTH_SHORT).show()
        } catch (e: Exception) {
            e.printStackTrace()
        }

    }

    fun printToastAlert(context: Context, string: String) {
        Toast.makeText(context, "" + string, Toast.LENGTH_SHORT).show()
    }


    fun isOnline(cContext: Context): Boolean {
        try {
            val cm = cContext.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
            val netInfo = cm?.activeNetworkInfo
            if (netInfo != null && netInfo.isConnectedOrConnecting) {
                return true
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }

        return false
    }


    fun isOnline(): Boolean {
        try {
            val cm = getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
            val netInfo = cm.activeNetworkInfo
            if (netInfo != null && netInfo.isConnectedOrConnecting) {
                return true
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }

        return false
    }

    /**
     * Check click of application.
     *
     * @return false if click twice
     */
    fun checkTwiceClick(): Boolean {
        val now = System.currentTimeMillis()
        if (now - mLastClickTime < Constant.CLICK_TIME_INTERVAL) {
            return false
        }
        mLastClickTime = now
        return true
    }

    /**
     * Set or add new fragment in to container
     *
     * @param fragment  New fragment to be set
     * @param container Container id to set fragment
     */
    /* public void setFragment(Fragment fragment, int container) {
        getSupportFragmentManager().beginTransaction().add(container, fragment)
                .addToBackStack(fragment.getClass().getName()).commit();
    }*/
    fun setFragment(fragment: Fragment, container: Int, isAdd: Boolean, isBackStack: Boolean,isAnimate: Boolean) {
        val fm = supportFragmentManager
        val ft = fm.beginTransaction()
        if(isAnimate){
            ft.setCustomAnimations(R.anim.enter, R.anim.exit, R.anim.pop_enter, R.anim.pop_exit);
        }

        if (isAdd) {
            ft.add(container, fragment, fragment.javaClass.simpleName)
        } else {
            ft.replace(container, fragment, fragment.javaClass.simpleName)
        }

        if (isBackStack) {
            ft.addToBackStack(null)
        }

        val currentFragment = getCurrentFrg(container)
        if (currentFragment != null) {
            ft.hide(currentFragment)
        }
        ft.commit()
    }


    fun getCurrentFrg(containerID: Int): Fragment? {
        return supportFragmentManager.findFragmentById(containerID)
    }

    /**
     * Hide key board from current focus in activity.
     *
     * @param activity Current activity object.
     */
    fun hideKeyBoardFromCurrentFocus(activity: Activity) {
        // TODO Auto-generated method stub
        try {
            val inputMethodManager =
                getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            inputMethodManager?.hideSoftInputFromWindow(
                Objects.requireNonNull(activity.currentFocus).windowToken,
                0
            )
        } catch (e: Exception) {
            e.printStackTrace()
        }

    }


    fun hideKeyboard(view: View) {
        try {
            val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            imm?.hideSoftInputFromWindow(
                view.windowToken,
                InputMethodManager.RESULT_UNCHANGED_SHOWN
            )
        } catch (e: Exception) {
            e.printStackTrace()
        }

    }

    /**
     * Method for calling web services.
     */
    fun callAPI(
        apiName: String, context: Context, requestMethod: Int, parameters: HashMap<String, String>,
        showDialog: Boolean, isDialogCancelable: Boolean,
        tag: String, files: MultipartBody.Part?, retrofitResponse: RetroFitResponse?
    ) {

        if (!isOnline()) {
            Toast.makeText(this, Constant.MESSAGE_CHECK_INTERNET_CONNECTION, Toast.LENGTH_LONG)
                .show()
            return
        }

        if (showDialog) {
            if (cm == null)
            {
                cm = CustomLoaderDialog(this)
                cm!!.show(isDialogCancelable)
            }
            else
            {
                cm!!.show(isDialogCancelable)
            }
        }

        val headers = HashMap<String, String>()
        headers["Content-Type"] = "application/x-www-form-urlencoded"
        headers["appversionandroid"] = BuildConfig.VERSION_NAME
        headers["language"] = "en"

        headers["accessToken"] =
            SharedPreferenceUtils.getInstance(context).getStringValue(AUTH_TOKEN, "") + ""


        Logger.print(">>", "Request --->> $parameters")
        //            Utils.writeFile(reqHashMap.toString(), methodName +"_Request.txt");

        var call: Call<String>? = null
        if (Constant.POST_API === requestMethod) {
            if (files != null) {
                call = RetroClient2.getClient(BASE_URL)
                    ?.callWebserviceSingleMultipart(apiName, headers, files, parameters)
            } else {
                call =
                    RetroClient2.getClient(BASE_URL)!!.callWebservice(apiName, headers, parameters)
            }

        } else if (Constant.GET_API === requestMethod) {
            call =
                RetroClient2.getClient(BASE_URL)!!.callWebserviceGet(apiName, headers, parameters)
        } else if (Constant.DELETE_API === requestMethod) {
            call = RetroClient2.getClient(BASE_URL)!!.callWebserviceDelete(
                apiName,
                headers,
                parameters
            )
        } else if (Constant.PUT_API === requestMethod) {
            call =
                RetroClient2.getClient(BASE_URL)!!.callWebservicePut(apiName, headers, parameters)
        }

        call!!.enqueue(object : Callback<String> {
            override fun onResponse(call: Call<String>, response: Response<String>) {
                cm!!.hide()
                Logger.print(">>", "response code --->> " + response.code())
                if (retrofitResponse != null) {
                    Logger.print(">>", "response bofu --->> " + response.body())

                    try {
                        if (response.code() == 403) {
                            /*val jsonObject = JSONObject(response.errorBody()!!.string())
                            printToastAlert(context, jsonObject.getString("message"))
                            SharedPreferenceUtils.getInstance(context).clear()
                            val intent = Intent(context, LoginActivity::class.java)
                            intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
                            startActivity(intent)*/

                        } else if (response.code() == 401) {
//                            retrofitResponse.onResponse(apiName, response.code(), response.errorBody().toString(), tag)
                            val jsonObject = JSONObject(response.errorBody()!!.string())
                            printToastAlert(context, jsonObject.getString("message"))
                            if (!apiName.equals(Constant.LOGIN)) {
//                                callForLogout(context as MainActivity)
                            }

                        } else {
                            val jsonObject = JSONObject(response.body())
                            val code = jsonObject.getInt("success")

                            if (code == 105) {
                                val msg: String = jsonObject.getString("message");
//                                showUpdateDialog(msg,context)

                            } else {
                                retrofitResponse.onResponse(
                                    apiName,
                                    response.code(),
                                    response.body()!!,
                                    tag
                                )
                            }

                        }
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }

                }
            }

            override fun onFailure(call: Call<String>, t: Throwable) {

                cm!!.hide()
                retrofitResponse?.onRetrofitError(apiName, t.message!!, tag)
            }
        })
    }

    fun getHeader(): HashMap<String, String> {
        val headers = HashMap<String, String>()

        headers["accessToken"] =
            SharedPreferenceUtils.getInstance(this).getStringValue(AUTH_TOKEN, "") + ""

        return headers
    }

    fun getSelectedThemeIsNightMode() : Boolean {
        return AppCompatDelegate.getDefaultNightMode() == AppCompatDelegate.MODE_NIGHT_YES
    }

    fun getDisplayWidth(): Int {
        val displayMetrics = DisplayMetrics()
        windowManager.defaultDisplay.getMetrics(displayMetrics)
        val width = displayMetrics.widthPixels
        return width
    }

    fun getDisplayHeight(): Int {
        val displayMetrics = DisplayMetrics()
        windowManager.defaultDisplay.getMetrics(displayMetrics)
        val height = displayMetrics.heightPixels
        return height
    }

    /*private fun showUpdateDialog(message: String,context: Context) {
        var dialogs = Dialog(context!!, com.onglai.R.style.Theme_Dialog)
        dialogs!!.requestWindowFeature(Window.FEATURE_NO_TITLE)
//        context!!.window.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        dialogs!!.setCancelable(false)
        dialogs!!.setContentView(com.onglai.R.layout.dialog_app_update)
        dialogs!!.tvAppUpdate.setOnClickListener {
            val appPackageName = packageName // getPackageName() from Context or Activity object
            try {
                startActivity(Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=$appPackageName")))
            } catch (anfe: android.content.ActivityNotFoundException) {
                startActivity(
                    Intent(
                        Intent.ACTION_VIEW,
                        Uri.parse("https://play.google.com/store/apps/details?id=$appPackageName")
                    )
                )
            }


        }
        dialogs!!.tvAppUpdateMsg.setText(message)
        dialogs!!.show()
    }*/

    /*private fun callForLogout(activity: MainActivity) {
        var map: HashMap<String, String> = HashMap()
        callAPI(
            Constant.LOGOUT, this, Constant.GET_API, map, true, false,
            Constant.LOGOUT,
            null, object : RetroFitResponse {

                override fun onResponse(MethodName: String, responseCode: Int, mRes: String, responseTag: String) {

                    try {

                        val jsonObject = JSONObject(mRes)
                        val code = jsonObject.getInt("code")

                        when (responseTag) {


                            Constant.LOGOUT -> {
                                if (code == 1) {
                                    SharedPreferenceUtils.getInstance(activity!!).setValue(Constant.IS_LOGIN, false)
                                    SharedPreferenceUtils.getInstance(activity!!)
                                        .setValue(Constant.LOGINUSERDATA, "")

                                    callForRefreshToken(activity)
                                }
                            }

                            *//*Constant.REFRESH_TOKEN_API -> {


                                if (code == 1) {

                                    val jsonData = jsonObject.getJSONObject("data");
                                    SharedPreferenceUtils.getInstance(activity!!)
                                        .setValue(Constant.AUTH_TOKEN, jsonData.getString("auth_token"))
                                    SharedPreferenceUtils.getInstance(activity!!)
                                        .setValue(Constant.REFRESH_TOKEN, jsonData.getString("refresh_token"))


                                    val fm = activity!!.supportFragmentManager
                                    for (i in 0 until fm.backStackEntryCount) {
                                        fm.popBackStack()
                                    }

                                    activity!!.setFragment(
                                        LoginFragment.newInstance("", ""),
                                        R.id.mainFrame,
                                        true,
                                        false
                                    )
                                }
                            }*//*
                        }
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                }

                override fun onRetrofitError(MethodName: String, mError: String, responseTag: Any) {
                }
            }

        )
    }*/

    /*private fun callForRefreshToken(activity: MainActivity) {
        val hashMap = java.util.HashMap<String, String>()
        hashMap["refresh_token"] = Constant.DEFAULT_REFRESH_TOKEN
        callAPI(
            Constant.REFRESH_TOKEN_API,
            activity!!,
            Constant.POST_API,
            hashMap,
            true,
            false,
            Constant.REFRESH_TOKEN_API,
            null,
            object : RetroFitResponse {

                override fun onResponse(
                    MethodName: String,
                    responseCode: Int,
                    mRes: String,
                    responseTag: String
                ) {

                    try {

                        val jsonObject = JSONObject(mRes)
                        val code = jsonObject.getInt("code")

                        when (responseTag) {


                            Constant.REFRESH_TOKEN_API -> {


                                if (code == 1) {

                                    val jsonData = jsonObject.getJSONObject("data");
                                    SharedPreferenceUtils.getInstance(activity!!)
                                        .setValue(Constant.AUTH_TOKEN, jsonData.getString("auth_token"))
                                    SharedPreferenceUtils.getInstance(activity!!)
                                        .setValue(Constant.REFRESH_TOKEN, jsonData.getString("refresh_token"))


                                    val fm = activity!!.supportFragmentManager
                                    for (i in 0 until fm.backStackEntryCount) {
                                        fm.popBackStack()
                                    }

                                    activity!!.setFragment(
                                        LoginFragment.newInstance("", ""),
                                        R.id.mainFrame,
                                        true,
                                        false
                                    )
                                } else if (code == 105) {
//                            retrofitResponse.onResponse(apiName, response.code(), response.errorBody().toString(), tag)
                                    val msg :String = jsonObject.getString("message");
                                    showUpdateDialog(msg, activity)


                                }
                            }
                        }
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                }

                override fun onRetrofitError(MethodName: String, mError: String, responseTag: Any) {
                }
            }
        )

    }*/

    fun retrofitMultiUploading(
        relativeURL: String, baseFragmentActivity: Activity,
        reqHashMap: HashMap<String, RequestBody>, methodName: String,
        aActivity: RetroFitResponse, isProgressEnable: Boolean,
        isCancelable: Boolean, canCancelOther: Boolean, methodType: Int,
        part: List<MultipartBody.Part>?
    ) {

        if (!isOnline(this)) {
            printToastAlert(this, Constant.MESSAGE_CHECK_INTERNET_CONNECTION)
        } else {
            if (cm == null) {
                cm = CustomLoaderDialog(this)
            }
            if (isProgressEnable)
                cm!!.show(isCancelable)


            val multiPartFileService = RetroClient2.multiFileUploadService()

            val headers = HashMap<String, String>()
            headers["appversionandroid"] = BuildConfig.VERSION_NAME
            headers["language"] = "en"
            headers["auth_token"] =
                SharedPreferenceUtils.getInstance(this).getStringValue(Constant.AUTH_TOKEN, "") + ""

            Logger.print(">>", "Request --->> $reqHashMap")
            //            Utils.writeFile(reqHashMap.toString(), methodName +"_Request.txt");
            Logger.print(">>", "Relative URL --->> $relativeURL")
            var call: Call<String>? = null

            if (Constant.POST_API === methodType) {

                if (part != null) {
                    call = multiPartFileService!!.postFile(relativeURL, part, headers, reqHashMap)
                } else {
                    call = multiPartFileService!!.postFile(relativeURL, headers, reqHashMap)
                }

            } else if (Constant.PUT_API === methodType) {

                if (part != null) {
                    call = multiPartFileService!!.putFile(relativeURL, part, headers, reqHashMap)
                } else {
                    call = multiPartFileService!!.putFile(relativeURL, headers, reqHashMap)
                }
                //                call = RetroClient2.getClient(BASE_URL).callWebservicePut(apiName, headers, parameters);
            }


            call!!.enqueue(object : Callback<String> {
                override fun onResponse(call: Call<String>, response: Response<String>) {


                    cm!!.hide()
                    Logger.print(">>", "response code --->> " + response.code())
                    if (response.code() == 401) {  // Call For Logout
                        val hashMapSignout = HashMap<String, String>()
                    } else {
                        if (response != null) {
                            Logger.print(">>", "response --->> " + response.body()!!)
                            if (response.code() == 403) {
                                try {
                                    Logger.print(
                                        ">>",
                                        "response --->> message = " + response.message() + ", error body = " + response.errorBody()!!.string() + ", raw = " + response.raw()
                                    )

                                } catch (e: Exception) {
                                    e.printStackTrace()
                                }

                            } else {
                                aActivity.onResponse(
                                    relativeURL,
                                    Activity.RESULT_OK,
                                    response.body()!!,
                                    methodName
                                )
                                res = response
                            }

                        } else {
                            Logger.print(">>", "response --->> Null Response")
                            aActivity.onRetrofitError(relativeURL, response.body()!!, methodName)
                            res = null
                        }
                    }
                }

                override fun onFailure(call: Call<String>, t: Throwable) {


                    cm!!.hide()

                    Logger.print("response", "<<onFailure --> " + t.message)
                    aActivity.onRetrofitError(relativeURL, t.message!!, methodName)
                }
            })

        }
    }

    fun <T> getCommonResponse(string: String): CommonResponse<*>? {
        val gson = Gson()
        return gson.fromJson(string, CommonResponse::class.java)
    }

    fun <T> getArrayList(string: String): ArrayList<*>? {
        val gson = Gson()
        return gson.fromJson<ArrayList<T>>(
            string.toString(),
            object : TypeToken<ArrayList<T>>() {
            }.type
        )
    }


    // This method store generic list in preference
    fun <T> setArrayList(context: Context, arrayList: ArrayList<T>) {
        SharedPreferenceUtils.getInstance(context).setValue("ArrayList", "")
        if (arrayList == null || arrayList.size <= 0) {
            return
        }
        val gson = Gson()
        val strGson = gson.toJson(arrayList)
        SharedPreferenceUtils.getInstance(context).setValue("ArrayList", strGson)
    }

    // This method convert generic list to data list
    fun <T> getArrayList(context: Context, type: Type): ArrayList<T> {
        val dataList: ArrayList<T> = ArrayList()
        val strJson = SharedPreferenceUtils.getInstance(context).getStringValue("ArrayList", "")
            ?: return dataList
        // clear preference string
        SharedPreferenceUtils.getInstance(context).setValue("ArrayList", "")
        val gson = Gson()
        return gson.fromJson<ArrayList<T>>(
            strJson,
            type
        )
    }

    inline fun <reified T> genericType() = object : TypeToken<T>() {}.type

    fun getIconFromAttributes(attrId: Int): Drawable {
        val typedValue = TypedValue()
        this.theme?.resolveAttribute(attrId, typedValue, true)
        val imageResId = typedValue.resourceId
        return this.let { ContextCompat.getDrawable(it, imageResId) }
            ?: throw IllegalArgumentException("Cannot load drawable $imageResId")
    }

    fun getColorFromAttribute(attrId: Int): Int{
        val typedValue = TypedValue()
        val theme = this.theme
        theme?.resolveAttribute(attrId, typedValue, true)
        return typedValue.data
    }
}