package com.artistfinder.baseclass

import android.content.Context
import android.graphics.drawable.Drawable
import android.util.TypedValue
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import com.artistfinder.activity.HomeActivity


open class BaseFragment : Fragment() {

    var activity: HomeActivity? = null

    override fun onAttach(context: Context) {
        super.onAttach(context)
        activity = context as HomeActivity
    }

    fun getIconFromAttributes(attrId: Int): Drawable {
        val typedValue = TypedValue()
        context?.theme?.resolveAttribute(attrId, typedValue, true)
        val imageResId = typedValue.resourceId
        return activity?.let { ContextCompat.getDrawable(it, imageResId) }
            ?: throw IllegalArgumentException("Cannot load drawable $imageResId")
    }

    fun getColorFromAttribute(attrId: Int): Int{
        val typedValue = TypedValue()
        val theme = context?.theme
        theme?.resolveAttribute(attrId, typedValue, true)
        return typedValue.data
    }
}