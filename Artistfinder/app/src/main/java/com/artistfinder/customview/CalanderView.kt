package com.artistfinder.customview

import android.content.Context
import android.util.AttributeSet
import android.util.TypedValue
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import com.artistfinder.R
import com.artistfinder.global.Constant
import com.artistfinder.utils.CircularTextView
import com.artistfinder.utils.Logger
import com.artistfinder.utils.Utils
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList
import android.icu.lang.UCharacter.GraphemeClusterBreak.T




class CalendarView : LinearLayout {

    // date format
    private var dateFormat: String? = null

    // current displayed month
    private val currentDate = Calendar.getInstance()
    private var dummyDate = Calendar.getInstance()
    private var lastSelectedDate: Date? = null

    //event handling
    private var eventHandler: EventHandler? = null

    // internal components
    private var header: LinearLayout? = null
    private var btnPrev: TextView? = null
    private var btnNext: TextView? = null
    private var txtDate: TextView? = null
    private var grid: GridView? = null
    private  var isPrevSelect : Boolean = false;
    private  var isFromNotification: Boolean = false;

    // seasons' rainbow
    internal var rainbow = intArrayOf(
        com.artistfinder.R.color.summer,
        com.artistfinder.R.color.fall,
        com.artistfinder.R.color.winter,
        com.artistfinder.R.color.spring
    )

    // month-season association (northern hemisphere, sorry australia :)
    internal var monthSeason = intArrayOf(2, 2, 3, 3, 3, 0, 0, 0, 1, 1, 1, 2)

    constructor(context: Context) : super(context) {}

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs) {
        initControl(context, attrs)
    }

    constructor(context: Context, attrs: AttributeSet, defStyleAttr: Int) : super(
        context,
        attrs,
        defStyleAttr
    ) {
        initControl(context, attrs)
    }

    /**
     * Load control xml layout
     */
    private fun initControl(context: Context, attrs: AttributeSet) {
        val inflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        inflater.inflate(com.artistfinder.R.layout.control_calendar, this)

        loadDateFormat(attrs)
        assignUiElements()
        assignClickHandlers()

        updateCalendar()
    }

    private fun loadDateFormat(attrs: AttributeSet) {
        val ta = context.obtainStyledAttributes(attrs, com.artistfinder.R.styleable.CalendarView)

        try {
            // try to load provided date format, and fallback to default otherwise
            dateFormat = ta.getString(com.artistfinder.R.styleable.CalendarView_dateFormat)
            if (dateFormat == null)
                dateFormat = DATE_FORMAT
        } finally {
            ta.recycle()
        }
    }

    private fun assignUiElements() {
        // layout is inflated, assign local variables to components
        header = findViewById(com.artistfinder.R.id.calendar_header) as LinearLayout
        btnPrev = findViewById(com.artistfinder.R.id.preMonth) as TextView
        btnNext = findViewById(com.artistfinder.R.id.nextMonth) as TextView
        txtDate = findViewById(com.artistfinder.R.id.calendar_date_display) as TextView
        grid = findViewById(com.artistfinder.R.id.calendar_grid) as GridView
    }

    private fun assignClickHandlers() {
        // add one month and refresh UI
        btnNext!!.setOnClickListener(object : View.OnClickListener {
            override fun onClick(v: View) {
                currentDate.add(Calendar.MONTH, 1)
                updateCalendar()
            }
        })

        // subtract one month and refresh UI
        btnPrev!!.setOnClickListener(object : View.OnClickListener {
            override fun onClick(v: View) {
                currentDate.add(Calendar.MONTH, -1)
                updateCalendar()
            }
        })

        // long-pressing a day
        grid!!.onItemLongClickListener = object : AdapterView.OnItemLongClickListener {

            override fun onItemLongClick(
                view: AdapterView<*>,
                cell: View,
                position: Int,
                id: Long
            ): Boolean {
                // handle long-press
                if (eventHandler == null)
                    return false

                eventHandler!!.onDayLongPress(view.getItemAtPosition(position) as Date)
                return true
            }
        }

        grid!!.onItemClickListener = object : AdapterView.OnItemClickListener {

            override fun onItemClick(
                parent: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long
            ) {

                // handle click
                if (eventHandler == null) {
                    return
                }

                Logger.print(
                    ">>",
                    Utils.getDateFromTimeStamp(
                        com.artistfinder.global.Constant.FORMAT5,
                        (parent?.getItemAtPosition(position) as Date).time
                    )
                )

                eventHandler!!.onDaySelectListener(parent?.getItemAtPosition(position) as Date)
            }

        }
    }

    /**
     * Display dates correctly in grid
     */
    @JvmOverloads
    fun updateCalendar(events: HashSet<Date>? = null) {
        val cells = ArrayList<Date>()
        val calendar = currentDate.clone() as Calendar

        // determine the cell for current month's beginning
        calendar.set(Calendar.DAY_OF_MONTH, 1)
        val monthBeginningCell = calendar.get(Calendar.DAY_OF_WEEK) - 1


        // move calendar backwards to the beginning of the week
        calendar.add(Calendar.DAY_OF_MONTH, -monthBeginningCell)

        // fill cells
        while (cells.size < DAYS_COUNT) {
            cells.add(calendar.getTime())
            calendar.add(Calendar.DAY_OF_MONTH, 1)
        }

        // update grid
        grid!!.adapter = CalendarAdapter(context, cells, events)

        // update title
        val sdf = SimpleDateFormat(dateFormat)
        txtDate!!.setText(sdf.format(currentDate.getTime()))
        btnPrev!!.setText(previousMonth(currentDate))
        btnNext!!.setText(nextMonth(currentDate))

        // set header color according to current season
        val month = currentDate.get(Calendar.MONTH)
        val season = monthSeason[month]
        val color = rainbow[season]

//        header!!.setBackgroundColor(resources.getColor(color))
    }


    private inner class CalendarAdapter(
        context: Context, var days: ArrayList<Date>, // days with events
        private val eventDays: HashSet<Date>?
    ) : ArrayAdapter<Date>(context, com.artistfinder.R.layout.control_calendar_day, days) {

        // for view inflation
        private val inflater: LayoutInflater

        init {
            inflater = LayoutInflater.from(context)
        }

        override fun getView(position: Int, view: View?, parent: ViewGroup): View {
            var view = view
            // day in question

            val cal = Calendar.getInstance()


            val date = getItem(position)
            cal.time = date
            val day = cal.get(Calendar.DAY_OF_MONTH)
            val month = cal.get(Calendar.MONTH)
            val year = cal.get(Calendar.YEAR)
//            val day = date!!.getDate()
//            val month = date!!.getMonth()
//            val year = date!!.getYear()

            // today
            val calToday = Calendar.getInstance()
            val today = Date()
            calToday.time = today
            val dayToday = calToday.get(Calendar.DAY_OF_MONTH)
            val monthToday = calToday.get(Calendar.MONTH)
            val yearToday = calToday.get(Calendar.YEAR)
            // inflate item if it does not exist yet
            if (view == null)
                view =
                    inflater.inflate(com.artistfinder.R.layout.control_calendar_day, parent, false)

            // if this day has an event, specify event image
            (view as CircularTextView)!!.setBackgroundResource(0)
            if (eventDays != null) {
                for (eventDate in eventDays) {
                    val date = getItem(position)
                    cal.time = date
                    if (cal.get(Calendar.DAY_OF_MONTH) === day &&
                        cal.get(Calendar.MONTH) === month &&
                        cal.get(Calendar.YEAR) === year
                    ) {
                        // mark this day for event
//                        view!!.setBackgroundResource(R.drawable.reminder)
                        break
                    }
                }
            }

            if (month != currentDate.get(Calendar.MONTH)) {
                (view as CircularTextView).visibility = View.GONE
            } else {
                (view as CircularTextView).visibility = View.VISIBLE


                // clear styling
//            (view as TextView).setTypeface(null, Typeface.NORMAL)
                (view as CircularTextView).setRoundBackground(false)
                (view as CircularTextView).setTextColor(getColorFromAttribute(com.artistfinder.R.attr.themeColor))

//                if(lastSelectedDate == null){
                /* if (month != monthToday || year != yearToday) {
                     // if this day is outside current month, grey it out
                     (view as CircularTextView).setRoundBackground(false)
                     (view as CircularTextView).setTextColor(getColorFromAttribute(R.attr.themeColor))
                 } else if (day == dayToday) {
                     // if it is today, set it to blue/bold
//                    (view as TextView).setTypeface(null, Typeface.BOLD)
                     (view as CircularTextView).setRoundBackground(true)
                     (view as CircularTextView).setTextColor(ContextCompat.getColor(context,R.color.darkBackground))
                 }*/
//                }

                (view).setOnClickListener {

                    var checkCal = Calendar.getInstance()
                    checkCal.time = days[position]

                    val sdf = SimpleDateFormat("dd/MM/yyyy")
                    val getDaysDate = sdf.format(checkCal.time)
                    val dayDate : Date = sdf.parse(getDaysDate)
                    val getCurrentDate = sdf.format(today.time)
                    val currenDayDate : Date = sdf.parse(getCurrentDate)
                    if (dayDate.before(currenDayDate) && !isPrevSelect) {
                        Toast.makeText(
                            context,
                            "Please select today or future date",
                            Toast.LENGTH_SHORT
                        ).show()
                        return@setOnClickListener
                    } else if (checkCal.get(Calendar.DAY_OF_WEEK) == Calendar.SATURDAY) {
                        Toast.makeText(context, "Saturday is off", Toast.LENGTH_SHORT).show()
                        return@setOnClickListener
                    } else if (checkCal.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY) {
                        Toast.makeText(context, "Sunday is off", Toast.LENGTH_SHORT).show()
                        return@setOnClickListener
                    }

                    lastSelectedDate = days[position]
                    eventHandler!!.onDaySelectListener(days[position])
                    notifyDataSetChanged()
                }

                if (days[position] == lastSelectedDate) {
                    (view as CircularTextView).setRoundBackground(true)
//                    (view as CircularTextView).setTextColor(ContextCompat.getColor(context,R.color.red))
                } else {
//                    if (day == dayToday) {
//                        // if it is today, set it to blue/bold
////                    (view as TextView).setTypeface(null, Typeface.BOLD)
//                        (view as CircularTextView).setRoundBackground(true)
//                        (view as CircularTextView).setTextColor(ContextCompat.getColor(context,R.color.darkBackground))
//                    }else {
                    (view as CircularTextView).setRoundBackground(false)
                    (view as CircularTextView).setTextColor(getColorFromAttribute(R.attr.themeColor))
//                    }
                }

                if (lastSelectedDate == null) {

                    if(isFromNotification){
                        if (Utils.getDateFromTimeStamp(
                                Constant.FORMAT4,
                                days[position].time
                            ).equals(Utils.getDateFromTimeStamp(Constant.FORMAT4, currentDate.time.time))
                        ) {
                            (view as CircularTextView).setRoundBackground(true)
                        }
                    }else{
                        if (Utils.getDateFromTimeStamp(
                                Constant.FORMAT4,
                                days[position].time
                            ).equals(Utils.getDateFromTimeStamp(Constant.FORMAT4, today.time))
                        ) {
                            (view as CircularTextView).setRoundBackground(true)
                        }
                    }


                }


                // set text
                (view as CircularTextView).text = "$day"

            }

            return view
        }
    }

    /**
     * Assign event handler to be passed needed events
     */
    fun setEventHandler(eventHandler: EventHandler) {
        this.eventHandler = eventHandler
    }

    fun setPreviSeceted(make: Boolean) {
        isPrevSelect = make
    }


    fun setDate(date : Date) {
        val cells = ArrayList<Date>()
        currentDate.time = date
        val calendar = currentDate.clone() as Calendar
        isPrevSelect = true
        isFromNotification = true
//        calendar.time = date
        // determine the cell for current month's beginning
        calendar.set(Calendar.DAY_OF_MONTH, 1)
        val monthBeginningCell = calendar.get(Calendar.DAY_OF_WEEK) - 1


        // move calendar backwards to the beginning of the week
        calendar.add(Calendar.DAY_OF_MONTH, -monthBeginningCell)

        // fill cells
        while (cells.size < DAYS_COUNT) {
            cells.add(calendar.getTime())
            calendar.add(Calendar.DAY_OF_MONTH, 1)
        }

        // update grid
        grid!!.adapter = CalendarAdapter(context, cells, HashSet<Date>())

        // update title
        val sdf = SimpleDateFormat(dateFormat)
        txtDate!!.setText(sdf.format(currentDate.getTime()))
        btnPrev!!.setText(previousMonth(currentDate))
        btnNext!!.setText(nextMonth(currentDate))

    }


    /**
     * This interface defines what events to be reported to
     * the outside world
     */
    interface EventHandler {
        fun onDayLongPress(date: Date)
        fun onDaySelectListener(date: Date)
    }

    companion object {
        // for logging
        private val LOGTAG = "Calendar View"

        // how many days to show, defaults to six weeks, 42 days
        private val DAYS_COUNT = 42

        // default date format
        private val DATE_FORMAT = "MMM yyyy"
    }

    private fun previousMonth(calendar: Calendar): String {
        dummyDate = calendar.clone() as Calendar
        dummyDate.add(Calendar.MONTH, -1)
        //format it to MMM // January
        return SimpleDateFormat(dateFormat).format(dummyDate.time)
    }

    private fun nextMonth(calendar: Calendar): String {
        dummyDate = calendar.clone() as Calendar
        dummyDate.add(Calendar.MONTH, 1)
        //format it to MMM // January
        return SimpleDateFormat(dateFormat).format(dummyDate.time)
    }

    private fun getColorFromAttribute(attrId: Int): Int {
        val typedValue = TypedValue()
        val theme = context?.theme
        theme?.resolveAttribute(attrId, typedValue, true)
        return typedValue.data
    }
}
/**
 * Display dates correctly in grid
 */