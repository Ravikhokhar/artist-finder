package com.artistfinder.fragment


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.artistfinder.R
import com.artistfinder.baseclass.BaseFragment
import com.artistfinder.global.Constant
import com.artistfinder.utils.SharedPreferenceUtils
import kotlinx.android.synthetic.main.fragment_artist.*
import kotlinx.android.synthetic.main.fragment_artist.toolbar
import kotlinx.android.synthetic.main.fragment_setting.*
import kotlinx.android.synthetic.main.generic_toolbar.*
import kotlinx.android.synthetic.main.generic_toolbar.view.*

/**
 * A simple [Fragment] subclass.
 */
class SettingFragment : BaseFragment(), View.OnClickListener {


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_setting, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        initViews()
        super.onViewCreated(view, savedInstanceState)
    }

    private fun initViews(){
        setUpToolbar()
        setOnClickListener()
    }


    private fun setUpToolbar() {

        toolbar.tvTitle.text = activity?.resources?.getString(R.string.settings)
        toolbar.ivNotification?.visibility = View.GONE
        toolbar.ivSearch?.visibility = View.GONE

        // set back icon
        toolbar.ivHome?.visibility = View.VISIBLE
        toolbar.ivHome?.setImageDrawable(getIconFromAttributes(R.attr.menuIcon))
    }

    private fun setOnClickListener() {
        ivHome?.setOnClickListener(this)
        ivSearch?.setOnClickListener(this)

        txtAboutUs?.setOnClickListener(this)
        txtProfile?.setOnClickListener(this)
        txtTerms?.setOnClickListener(this)
        txtResetPass?.setOnClickListener(this)
        txtLogout?.setOnClickListener(this)
    }

    override fun onClick(v: View?) {
        when (v!!.id) {
            R.id.ivHome -> {
                activity!!.openDrawer()
            }
            R.id.txtAboutUs -> {
//                activity!!.setFragment(
//                    WebViewFragment.newInstance(0),
//                    R.id.frContainer,
//                    true,
//                    true,true
//                )
            }
            R.id.txtProfile -> {
                activity!!.setFragment(
                    ProfileFragment(),
                    R.id.frContainer,
                    true,
                    true,true
                )
            }
            R.id.txtTerms -> {
                val  isNightModeEnabled = SharedPreferenceUtils.getInstance(activity!!).getBoolanValue(
                    Constant.IS_DARK_MODE,false)
                val termsURL = if (isNightModeEnabled){
                    Constant.URL_TERMS_CONDITION_BLACK
                }else {
                    Constant.URL_TERMS_CONDITION_WHITE
                }

                activity!!.setFragment(
                    WebViewFragment.newInstance(1,termsURL),
                    R.id.frContainer,
                    true,
                    true,true
                )
            }
            R.id.txtResetPass -> {

            }
            R.id.txtLogout -> {
                    activity!!.showLogoutAlert()
            }
        }
    }


}
