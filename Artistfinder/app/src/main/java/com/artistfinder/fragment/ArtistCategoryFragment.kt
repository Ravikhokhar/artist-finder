package com.artistfinder.fragment

import android.content.Context
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.artistfinder.R
import com.artistfinder.activity.HomeActivity
import com.artistfinder.adapter.ArtistCategoryAdapter
import com.artistfinder.global.Constant
import com.artistfinder.listners.OnItemClickListener
import com.artistfinder.model.ArtistCategory
import com.artistfinder.retrofitclient.RetroFitResponse
import kotlinx.android.synthetic.main.fragment_artist_category.*
import kotlinx.android.synthetic.main.generic_toolbar.*
import kotlinx.android.synthetic.main.generic_toolbar.view.*

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

class ArtistCategoryFragment : Fragment(), View.OnClickListener, RetroFitResponse,
    OnItemClickListener {

    // TODO: Rename and change types of parameters
    private var param1: String? = null
    private var param2: String? = null
    private var activity: HomeActivity? = null
    private var adapter: ArtistCategoryAdapter? = null
    private var dataCategory: ArrayList<ArtistCategory> = ArrayList()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_artist_category, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        toolbar.ivHome.setOnClickListener(this)
        toolbar.ivSearch.setOnClickListener(this)
        toolbar.ivNotification.setOnClickListener(this)
        toolbar.tvTitle.setText(getString(R.string.artist_appoint_ttl))
        callForArtistType()
        setAdapter(dataCategory!!)

        toolbar.etSearch.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
                val dataSearch = etSearch.getText().toString() + ""
                if (dataSearch.trim({ it <= ' ' }).length > 0) {
                    setAdapter(searchText(dataSearch.trim({ it <= ' ' })))
                } else {
                    setAdapter(dataCategory!!)
                }
            }

            override fun afterTextChanged(s: Editable) {

            }
        })
    }

    private fun searchText(dataSearch: String): java.util.ArrayList<ArtistCategory> {
        val searchList = java.util.ArrayList<ArtistCategory>()
        for (artist in dataCategory!!) {
            if (artist.typeName!!.toLowerCase().contains(dataSearch.toLowerCase())) {
                searchList.add(artist)
            }
        }
        return searchList
    }

    private fun callForArtistType() {
        val map = HashMap<String, String>()
        activity?.callAPI(
            Constant.ARTIST_TYPE,
            activity!!,
            Constant.POST_API,
            map,
            true,
            false,
            Constant.ARTIST_TYPE,
            null,
            this
        )
    }

    private fun setAdapter(list: ArrayList<ArtistCategory>) {
        if (list != null) {
            adapter = ArtistCategoryAdapter(list!!, activity!!, this)
            reyCategory.adapter = adapter
        }

        /*if (adapter == null) {
            adapter = ArtistCategoryAdapter(this.dataCategory!!, activity!!, this)
            reyCategory.adapter = adapter
        } else {
            adapter!!.notifyDataSetChanged()
        }*/
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        activity = context as HomeActivity
    }

    override fun onClick(v: View?) {
        when (v!!.id) {
            R.id.ivHome -> {
                activity!!.openDrawer()
            }

            R.id.ivSearch -> {
                if (toolbar.etSearch.visibility == View.VISIBLE) {
                    toolbar.etSearch.visibility = View.GONE
                    toolbar.tvTitle.visibility = View.VISIBLE
                    activity!!.hideKeyboard(etSearch)
                } else {
                    toolbar.etSearch.visibility = View.VISIBLE
                    toolbar.tvTitle.visibility = View.GONE

                }
            }

            R.id.ivNotification -> {
                activity!!.setFragment(
                    NotificationFragment.newInstance(true),
                    R.id.frContainer,
                    true,
                    true,true
                )
            }
        }
    }

    override fun onViewClicked(v: View, obj: Any, position: Int) {
        activity!!.setFragment(
            ArtistFragment.newInstance(
                (obj as ArtistCategory).typeName!!,
                (obj as ArtistCategory).typeID.toString()
            ),
            R.id.frContainer,
            true,
            true,true
        )
    }


    override fun onResponse(
        MethodName: String,
        responseCode: Int,
        mRes: String,
        responseTag: String
    ) {
        try {
            val commonResponse = activity!!.getCommonResponse<ArrayList<ArtistCategory>>(mRes)
            activity!!.setArrayList(
                activity!!.applicationContext,
                commonResponse!!.result as ArrayList<*>
            )
            val code = commonResponse!!.success

            when (responseTag) {
                Constant.ARTIST_TYPE -> {

                    if (code == 1) {

                        dataCategory.addAll(
                            activity!!.getArrayList(
                                activity!!.applicationContext,
                                activity!!.genericType<ArrayList<ArtistCategory>>()
                            )
                        )

                        activity!!.runOnUiThread {
                            setAdapter(dataCategory!!)
                        }
                    }

                }
            }

        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    override fun onRetrofitError(MethodName: String, mError: String, responseTag: Any) {
    }

    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment ArtistCategoryFragment.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
            ArtistCategoryFragment().apply {
                arguments = Bundle().apply {
                    putString(ARG_PARAM1, param1)
                    putString(ARG_PARAM2, param2)
                }
            }
    }
}
