package com.artistfinder.fragment

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.graphics.Bitmap
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.PorterDuff
import android.graphics.drawable.Drawable
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.annotation.DrawableRes
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import com.artistfinder.R
import com.artistfinder.adapter.PhotosAdapter
import com.artistfinder.adapter.VideosAdapter
import com.artistfinder.baseclass.BaseFragment
import com.artistfinder.customview.WorkaroundMapFragment
import com.artistfinder.global.Constant
import com.artistfinder.listners.OnItemClickListener
import com.artistfinder.model.ArtistDetail
import com.artistfinder.model.ArtistDetailCommonResp
import com.artistfinder.model.Photos
import com.artistfinder.model.Video
import com.artistfinder.retrofitclient.RetroFitResponse
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.model.BitmapDescriptor
import com.google.android.gms.maps.model.BitmapDescriptorFactory
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import com.google.gson.Gson
import kotlinx.android.synthetic.main.fragment_artist.toolbar
import kotlinx.android.synthetic.main.fragment_artist_detail.*
import kotlinx.android.synthetic.main.generic_toolbar.*
import kotlinx.android.synthetic.main.generic_toolbar.view.*


private const val USER_ID = "param2"

/**
 * A simple [Fragment] subclass.
 * Activities that contain this fragment must implement the
 * [ArtistDetailFragment.OnFragmentInteractionListener] interface
 * to handle interaction events.
 * Use the [ArtistDetailFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class ArtistDetailFragment : BaseFragment(), View.OnClickListener, OnItemClickListener,
    RetroFitResponse, OnMapReadyCallback {


    private var data: ArtistDetail? = null
    // TODO: Rename and change types of parameters
    private var userId: String? = null
    private var artistType: String? = null

    private var photosList: ArrayList<Photos> = arrayListOf()
    private var photosAdapter: PhotosAdapter? = null

    private var videosList: ArrayList<Video> = arrayListOf()
    private var videosAdapter: VideosAdapter? = null
    private var mGoogleMap: GoogleMap? = null
    private var receiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context?, intent: Intent?) {
            if (intent != null && intent.action.equals(Constant.APPOINTMENT_BOOKED)) {
                activity!!.onBackPressed()
            }
        }

    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            userId = it.getString(USER_ID)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_artist_detail, container, false)
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        initViews()
        super.onViewCreated(view, savedInstanceState)
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        var mapFragment: WorkaroundMapFragment? = null
        mapFragment =
            childFragmentManager.findFragmentById(R.id.map) as WorkaroundMapFragment?
        mapFragment?.setListener(object : WorkaroundMapFragment.OnTouchListener {
            override fun onTouch() {
                scrollView.requestDisallowInterceptTouchEvent(true)
            }

        })
        mapFragment?.getMapAsync(this)
        LocalBroadcastManager.getInstance(activity!!)
            .registerReceiver(receiver, IntentFilter(Constant.APPOINTMENT_BOOKED))
    }

    override fun onMapReady(mMap: GoogleMap?) {
// Add a marker in Sydney and move the camera
        mGoogleMap = mMap
        callForArtistDetail()


    }

    override fun onResume() {
        super.onResume()
        activity!!.lockUnlockDrawer(true)
    }

    override fun onDestroy() {
        super.onDestroy()
        activity!!.lockUnlockDrawer(false)
    }

    private fun initViews() {

        setUpToolbar()
        setOnClickListener()
        setPhotosAdapter()
        setVideosAdapter()
    }

    private fun callForArtistDetail() {
        val map = HashMap<String, String>()
        map["userID"] = userId!!
        activity?.callAPI(
            Constant.ARTIST_DETAIL,
            activity!!,
            Constant.POST_API,
            map,
            true,
            false,
            Constant.ARTIST_DETAIL,
            null,
            this
        )
    }

    private fun setUpToolbar() {

        toolbar.ivNotification?.visibility = View.GONE
        tvTitle.text = activity?.resources?.getString(R.string.details)
        ivSearch?.visibility = View.GONE

        // set back icon
        ivHome?.visibility = View.VISIBLE
        ivHome?.setImageDrawable(getIconFromAttributes(R.attr.backIcon))
    }

    private fun setOnClickListener() {
        ivHome?.setOnClickListener(this)
        btnBookNow?.setOnClickListener(this)
    }

    override fun onClick(v: View?) {
        when (v!!.id) {
            R.id.ivHome -> {
                activity!!.onBackPressed()
            }
            R.id.btnBookNow -> {
                activity!!.setFragment(
                    Booking.newInstance(data!!, ""),
                    R.id.frContainer,
                    true,
                    true, true
                )
            }
        }
    }

    private fun setPhotosAdapter() {
        if (photosAdapter == null) {
            photosAdapter = PhotosAdapter(activity!!, photosList, this, true, false)
            recyclerViewImages.adapter = photosAdapter
        } else {
            photosAdapter!!.notifyDataSetChanged()
        }
    }

    private fun setVideosAdapter() {
        if (videosAdapter == null) {
            videosAdapter = VideosAdapter(activity!!, videosList, this, true, false)
            recyclerViewVideos.adapter = videosAdapter
        } else {
            videosAdapter!!.notifyDataSetChanged()
        }
    }

    override fun onViewClicked(v: View, obj: Any, position: Int) {
    }

    override fun onResponse(
        MethodName: String,
        responseCode: Int,
        mRes: String,
        responseTag: String
    ) {
        try {
            val commonResponse = Gson().fromJson(mRes, ArtistDetailCommonResp::class.java)
            val code = commonResponse!!.success

            when (responseTag) {
                Constant.ARTIST_DETAIL -> {

                    if (code == 1) {

                        data = commonResponse!!.result
                        tvName.setText(data?.name)
                        tvAddress.setText(data?.address)
                        tvCity.setText(data?.city)
                        tvCountry.setText(data?.country)
                        tvNumber.setText(data?.phone)
                        tvEmail.setText(data?.email)
                        imgProfile.setImageURI(data?.profileImg)
                        artistType = data?.artistType.toString()

                        if (data!!.potfolio_image != null && data!!.potfolio_image!!.size > 0) {
                            tvNoImages.visibility = View.GONE
                            recyclerViewImages.visibility = View.VISIBLE
                            photosList.addAll(data!!.potfolio_image!!)
                            setPhotosAdapter()
                        }else {
                            recyclerViewImages.visibility = View.GONE
                            tvNoImages.visibility = View.VISIBLE
                        }
                        if (data!!.potfolio_vid != null && data!!.potfolio_vid!!.size > 0) {
                            tvNoVideos.visibility = View.GONE
                            recyclerViewVideos.visibility = View.VISIBLE
                            videosList.addAll(data!!.potfolio_vid!!)
                            setVideosAdapter()
                        }else{
                            recyclerViewVideos.visibility = View.GONE
                            tvNoVideos.visibility = View.VISIBLE

                        }

                        if (data!!.latlong != null && data!!.latlong!!.length > 0) {
                            val parts = data!!.latlong!!.split(",")
                            val points = LatLng(parts[0].toDouble(), parts[1].toDouble())
                            mGoogleMap!!.addMarker(
                                MarkerOptions().position(points).title(data!!.name).icon(
                                    bitmapDescriptorFromVector(activity!!,R.drawable.img_pin))
                            )
                            mGoogleMap!!.moveCamera(CameraUpdateFactory.newLatLng(points))
                            mGoogleMap!!.animateCamera(CameraUpdateFactory.zoomTo(15.0f))
                        }
                    }
                }
            }

        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    override fun onRetrofitError(MethodName: String, mError: String, responseTag: Any) {

    }

    override fun onDestroyView() {
        super.onDestroyView()
        LocalBroadcastManager.getInstance(activity!!).unregisterReceiver(receiver)

    }

    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @return A new instance of fragment ArtistDetailFragment.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance(param1: String) =
            ArtistDetailFragment().apply {
                arguments = Bundle().apply {
                    putString(USER_ID, param1)
                }
            }
    }
    private fun bitmapDescriptorFromVector (context: Context,@DrawableRes vectorResId: Int) : BitmapDescriptor {
        val vectorDrawable = ContextCompat.getDrawable(context, vectorResId)
        vectorDrawable!!.setBounds(0, 0, vectorDrawable.getIntrinsicWidth(), vectorDrawable.getIntrinsicHeight())
        val bitmap = Bitmap.createBitmap(vectorDrawable.getIntrinsicWidth(), vectorDrawable.getIntrinsicHeight(), Bitmap.Config.ARGB_8888)
        val canvas = Canvas(bitmap)
        vectorDrawable.draw(canvas)
        return BitmapDescriptorFactory.fromBitmap(bitmap)
    }




}
