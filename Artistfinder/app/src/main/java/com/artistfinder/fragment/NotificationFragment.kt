package com.artistfinder.fragment

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.artistfinder.R
import com.artistfinder.activity.HomeActivity
import com.artistfinder.adapter.NotificationAdapter
import com.artistfinder.baseclass.BaseFragment
import com.artistfinder.global.Constant
import com.artistfinder.listners.OnItemClickListener
import com.artistfinder.model.Notification
import com.artistfinder.model.UserInfo
import com.artistfinder.retrofitclient.RetroFitResponse
import com.artistfinder.utils.SharedPreferenceUtils
import kotlinx.android.synthetic.main.fragment_artist.toolbar
import kotlinx.android.synthetic.main.fragment_notification.*
import kotlinx.android.synthetic.main.generic_toolbar.*
import kotlinx.android.synthetic.main.generic_toolbar.view.*

private const val IS_FOR = "param2"

class NotificationFragment : BaseFragment(), View.OnClickListener, RetroFitResponse,OnItemClickListener {


    private var notificationAdapter: NotificationAdapter? = null
    public var notificationList: ArrayList<Notification> = ArrayList()
    private var userInfo: UserInfo? = null
    private var userId: Int? = null
    private var isFromHome : Boolean =false;
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            isFromHome = it.getBoolean(IS_FOR)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_notification, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initViews(view)
    }

    private fun initViews(view: View) {
        setUpToolbar(view)
        notificationData()
        setAdapter()
        userInfo = SharedPreferenceUtils.getInstance(activity!!)
            .getObject(Constant.LOGINUSERDATA, UserInfo::class.java)
        userId = userInfo?.userID
        if (userId != null) {
            callForGetNotificationList(userId)
        }
    }

    override fun onViewClicked(v: View, obj: Any, position: Int) {

        when(v.id){
            R.id.rootMain ->{
                activity!!.setFragment(
                    ApointmnetReqFragment.newInstance(true,(obj as Notification).date),
                    R.id.frContainer,
                    true,
                    true, true
                )
            }
        }
    }


    private fun setAdapter() {
        if (notificationAdapter == null) {
            notificationAdapter = NotificationAdapter(activity!!, notificationList,this)
            recyclerNotification.adapter = notificationAdapter
        } else {
            notificationAdapter!!.notifyDataSetChanged()
        }

        if(notificationList!=null && notificationList.size>0){
            recyclerNotification.visibility = View.VISIBLE
            tvNoData.visibility = View.GONE
        }else{
            recyclerNotification.visibility = View.GONE
            tvNoData.visibility = View.VISIBLE
        }

    }

    override fun onResume() {
        super.onResume()
//        activity!!.lockUnlockDrawer(true)
    }

    override fun onDestroy() {
        super.onDestroy()
//        activity!!.lockUnlockDrawer(false)
    }


    private fun setUpToolbar(view: View) {


        if (isFromHome) {
            ivHome?.setImageDrawable(getIconFromAttributes(R.attr.backIcon))
        }
        toolbar.tvTitle.text = activity?.resources?.getString(R.string.notifications)
        ivSearch?.visibility = View.GONE

        // set back icon
        ivHome?.visibility = View.VISIBLE
        toolbar.ivNotification?.visibility = View.GONE
//        ivHome?.setImageDrawable(getIconFromAttributes(R.attr.backIcon))
        ivHome?.setOnClickListener(this)

    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        activity = context as HomeActivity
    }

    private fun notificationData() {
    }

    override fun onClick(v: View?) {
        when (v!!.id) {
            R.id.ivHome -> {
//                activity!!.onBackPressed()
                if(isFromHome){
                    activity!!.onBackPressed()
                }else{
                    activity!!.openDrawer()
                }
            }
        }
    }

    private fun callForGetNotificationList(userId: Int?) {
        val map = HashMap<String, String>()
        map["userID"] = "$userId"
        activity!!.callAPI(
            Constant.GET_NOTIFICATION,
            activity!!,
            Constant.POST_API,
            map,
            true,
            false,
            Constant.GET_NOTIFICATION,
            null,
            this
        )
    }

    override fun onResponse(
        MethodName: String,
        responseCode: Int,
        mRes: String,
        responseTag: String
    ) {
        if (responseCode == 200) {
            try {
                val commonResponse = activity!!.getCommonResponse<ArrayList<Notification>>(mRes)
                activity!!.setArrayList(
                    activity!!.applicationContext,
                    commonResponse!!.result as ArrayList<*>
                )
                val code = commonResponse.success

                if (code == 1) {
                    notificationList.addAll(
                        activity!!.getArrayList(
                            activity!!.applicationContext,
                            activity!!.genericType<ArrayList<Notification>>()
                        )
                    )
                    activity!!.runOnUiThread {
                        setAdapter()
                    }
                }

            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
    }

    override fun onRetrofitError(MethodName: String, mError: String, responseTag: Any) {

    }

    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @return A new instance of fragment ArtistDetailFragment.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance(param1: Boolean) =
            NotificationFragment().apply {
                arguments = Bundle().apply {
                    putBoolean(IS_FOR, param1)
                }
            }
    }
}
