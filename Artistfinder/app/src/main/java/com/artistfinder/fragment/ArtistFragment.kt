package com.artistfinder.fragment


import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import com.artistfinder.R
import com.artistfinder.adapter.ArtistAdapter
import com.artistfinder.baseclass.BaseFragment
import com.artistfinder.global.Constant
import com.artistfinder.listners.OnItemClickListener
import com.artistfinder.model.Artist
import com.artistfinder.model.ArtistCategory
import com.artistfinder.model.UserInfo
import com.artistfinder.retrofitclient.RetroFitResponse
import com.artistfinder.utils.SharedPreferenceUtils
import kotlinx.android.synthetic.main.fragment_artist.*
import kotlinx.android.synthetic.main.fragment_artist.toolbar
import kotlinx.android.synthetic.main.fragment_artist_category.*
import kotlinx.android.synthetic.main.generic_toolbar.*
import kotlinx.android.synthetic.main.generic_toolbar.view.*

/**
 * A simple [Fragment] subclass.
 */

private const val DATA = "data"
private const val ArtistId = "artistId"

class ArtistFragment : BaseFragment(), View.OnClickListener,OnItemClickListener,RetroFitResponse {

    private var name: String? = null
    private var artistId : String? = null
    private var artistList: ArrayList<Artist> = ArrayList()
    private var artistAdapter :ArtistAdapter?=null
    private var userInfo: UserInfo? = null
    private var receiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context?, intent: Intent?) {
            if(intent!=null && intent.action.equals(Constant.APPOINTMENT_BOOKED)){
                activity!!.onBackPressed()
            }
        }

    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            name = it.getString(DATA)
            artistId = it.getString(ArtistId)
        }
    }
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_artist, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        userInfo = SharedPreferenceUtils.getInstance(activity!!)
            .getObject(Constant.LOGINUSERDATA, UserInfo::class.java)
        callForArtist()
        LocalBroadcastManager.getInstance(activity!!).registerReceiver(receiver, IntentFilter(Constant.APPOINTMENT_BOOKED))

        toolbar.etSearch.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
                val dataSearch = etSearch.getText().toString() + ""
                if (dataSearch.trim({ it <= ' ' }).length > 0) {
                    setAdapter(searchText(dataSearch.trim({ it <= ' ' })))
                } else {
                    setAdapter(artistList!!)
                }
            }

            override fun afterTextChanged(s: Editable) {

            }
        })
    }

    private fun searchText(dataSearch: String): java.util.ArrayList<Artist> {
        val searchList = java.util.ArrayList<Artist>()
        for (artist in artistList!!) {
            if (artist.name!!.toLowerCase().contains(dataSearch.toLowerCase())) {
                searchList.add(artist)
            }
        }
        return searchList
    }

    private fun callForArtist() {
        val map = HashMap<String, String>()
        map["typeID"] = artistId!!
        activity?.callAPI(
            Constant.ARTIST_LISTING,
            activity!!,
            Constant.POST_API,
            map,
            true,
            false,
            Constant.ARTIST_LISTING,
            null,
            this
        )
    }

    override fun onResponse(
        MethodName: String,
        responseCode: Int,
        mRes: String,
        responseTag: String
    ) {
        try {
            val commonResponse = activity!!.getCommonResponse<ArrayList<Artist>>(mRes)
            activity!!.setArrayList(activity!!.applicationContext, commonResponse!!.result as ArrayList<*>)
            val code = commonResponse.success

            when (responseTag) {
                Constant.ARTIST_LISTING -> {

                    if (code == 1) {
                        val artistLists : ArrayList<Artist> = activity!!.getArrayList(activity!!.applicationContext, activity!!.genericType<ArrayList<Artist>>())
                        if (artistLists != null && artistLists.size>0) {
                            artistList.addAll(artistLists)
                        }
                        if (userInfo?.userID != null && artistList.size>0) {
                            for (i in artistList.size - 1 downTo 0) {
                                if (artistList[i].userID == userInfo!!.userID) {
                                    artistList.removeAt(i)
                                }
                            }
                        }

                        activity!!.runOnUiThread {
                            setAdapter(artistList!!)
                        }
                    }

                }
            }

        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    override fun onRetrofitError(MethodName: String, mError: String, responseTag: Any) {
    }



    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        initViews(view)
        super.onViewCreated(view, savedInstanceState)
    }

    override fun onResume() {
        super.onResume()
        activity!!.lockUnlockDrawer(true)
    }

    override fun onDestroy() {
        super.onDestroy()
        activity!!.lockUnlockDrawer(false)
    }
    private fun initViews(view: View) {

        setUpToolbar(view)
        setOnClickListener()

    }

    private fun setAdapter(list: ArrayList<Artist>) {
        if(list != null)
        {
            if(artistAdapter == null){
                artistAdapter = ArtistAdapter(activity!!,list,this)
                recyclerViewArtist.adapter = artistAdapter
            }else{
                artistAdapter!!.updateData(list)
            }
            if(list.size>0){
                recyclerViewArtist.visibility = View.VISIBLE
                tvNoData.visibility = View.GONE
            }else{
                recyclerViewArtist.visibility = View.GONE
                tvNoData.visibility = View.VISIBLE
            }


        }
    }



    private fun setUpToolbar(view: View) {

        toolbar.ivNotification?.visibility = View.GONE
        tvTitle.text = name
        ivSearch?.visibility = View.VISIBLE

        // set back icon
        ivHome?.visibility = View.VISIBLE
        ivHome?.setImageDrawable(getIconFromAttributes(R.attr.backIcon))
    }

    private fun setOnClickListener() {
        ivHome?.setOnClickListener(this)
        ivSearch?.setOnClickListener(this)
    }



    override fun onClick(v: View?) {
        when (v!!.id) {
            R.id.ivHome -> {
                activity!!.onBackPressed()
            }
            R.id.ivSearch -> {
                if (toolbar.etSearch.visibility == View.VISIBLE) {
                    toolbar.etSearch.visibility = View.GONE
                    toolbar.tvTitle.visibility = View.VISIBLE
                    activity!!.hideKeyboard(etSearch)
                } else {
                    toolbar.etSearch.visibility = View.VISIBLE
                    toolbar.tvTitle.visibility = View.GONE

                }
            }
        }
    }

    override fun onViewClicked(v: View, obj: Any, position: Int) {
        activity!!.setFragment(
            ArtistDetailFragment.newInstance((obj as Artist).userID.toString()),
            R.id.frContainer,
            true,
            true,true
        )
    }

    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment ArtistCategoryFragment.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance(param1: String,typeId : String) =
            ArtistFragment().apply {
                arguments = Bundle().apply {
                    putString(DATA, param1)
                    putString(ArtistId,typeId)
                }
            }
    }
    override fun onDestroyView() {
        super.onDestroyView()
        LocalBroadcastManager.getInstance(activity!!).unregisterReceiver(receiver)

    }

}
