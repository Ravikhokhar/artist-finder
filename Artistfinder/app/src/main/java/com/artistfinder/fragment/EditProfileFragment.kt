package com.artistfinder.fragment


import android.annotation.SuppressLint
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.AsyncTask
import android.os.Bundle
import android.os.Environment
import android.os.Environment.getExternalStoragePublicDirectory
import android.os.Handler
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Toast
import androidx.fragment.app.Fragment
import com.artistfinder.R
import com.artistfinder.activity.ArtistTypeActivity
import com.artistfinder.adapter.PhotosAdapter
import com.artistfinder.adapter.VideosAdapter
import com.artistfinder.baseclass.BaseFragment
import com.artistfinder.dialog.CustomLoaderDialog
import com.artistfinder.global.Constant
import com.artistfinder.listners.OnItemClickListener
import com.artistfinder.listners.OnMultiMediaListener
import com.artistfinder.model.Photos
import com.artistfinder.model.UserInfo
import com.artistfinder.model.Video
import com.artistfinder.retrofitclient.RetroClient2
import com.artistfinder.retrofitclient.RetroFitResponse
import com.artistfinder.utils.*
import com.artistfinder.videocompression.MediaController
import com.kbeanie.multipicker.api.entity.ChosenImage
import com.kbeanie.multipicker.api.entity.ChosenVideo
import com.theartofdev.edmodo.cropper.CropImage
import com.vincent.filepicker.filter.entity.ImageFile
import com.vincent.filepicker.filter.entity.VideoFile
import kotlinx.android.synthetic.main.fragment_artist.toolbar
import kotlinx.android.synthetic.main.fragment_edit_profile.*
import kotlinx.android.synthetic.main.generic_toolbar.*
import kotlinx.android.synthetic.main.generic_toolbar.view.*
import kotlinx.android.synthetic.main.layout_slot_time.*
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.File
import java.util.*
import kotlin.collections.ArrayList
import kotlin.collections.LinkedHashSet


// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"

/**
 * A simple [Fragment] subclass.
 * Use the [EditProfileFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class EditProfileFragment : BaseFragment(), View.OnClickListener, CameraUtils.OnCameraResult,
    AdapterView.OnItemSelectedListener,
    OnItemClickListener, OnMultiMediaListener, RetroFitResponse {


    // TODO: Rename and change types of parameters
    private var param1: String? = null
    private var photosList: ArrayList<Photos> = arrayListOf()
    private var photosAdapter: PhotosAdapter? = null
    private var cameraUtils: CameraUtils? = null
    private var selectedImageFile: File? = null
    private var loader: CustomLoaderDialog? = null
    private var videosList: ArrayList<Video> = arrayListOf()
    private var videosAdapter: VideosAdapter? = null
    private var userInfo: UserInfo? = null

    var size: Int = 0
    private var selectedArtistType: String = "Artist Type"
    private var artistId: Int = -1
    private var artistTypeName: String = ""

    private var selectedTimeSlotList: ArrayList<String> = ArrayList()
    private var isButton1 = false
    private var isButton2 = false
    private var isButton3 = false
    private var isButton4 = false
    private var isButton5 = false
    private var isButton6 = false
    private var isButton7 = false
    private var isButton8 = false
    private var isButton9 = false
    private var selectedTimeSlot: String = ""
    private var artistType: String = ""
    private var other: String = ""
    private var otherArtist: String = ""
    private var isFirstTimeClear: Boolean = false
    private var lat: Double = 0.0
    private var long: Double = 0.0
    private var deleteMediaPos = -1
    private var deleteMedialType = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(com.artistfinder.R.layout.fragment_edit_profile, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        initViews()
        super.onViewCreated(view, savedInstanceState)
        userInfo = SharedPreferenceUtils.getInstance(activity!!)
            .getObject(Constant.LOGINUSERDATA, UserInfo::class.java)

        setUI()
    }

    private fun setUI() {
        if (userInfo != null) {
            imgProfile.setImageURI(userInfo!!.profileImg)
            edtName.setText(userInfo!!.name)
            edtUserName.setText(userInfo!!.username)
            edtUserEmail.setText(userInfo!!.email)
            edtAddress.setText(userInfo!!.address)
            edtNumber.setText(userInfo!!.phone)
            edtCountry.setText(userInfo!!.country)
            edtCity.setText(userInfo!!.city)
            if (userInfo!!.potfolio_image != null && userInfo!!.potfolio_image.size > 0) {
                photosList.addAll(userInfo!!.potfolio_image)
                setPhotosAdapter()
            }
            if (userInfo!!.potfolio_vid != null && userInfo!!.potfolio_vid.size > 0) {
                videosList.addAll(userInfo!!.potfolio_vid)
                setVideosAdapter()
            }
        }

        if (photosList.size < 5) {
            val photos = Photos()
            photos.isAddImageView = true
            photosList.add(photos)
            setPhotosAdapter()
        }
        if (videosList.size < 2) {
            val video = Video()
            video.isAddImageView = true
            videosList.add(video)
            setVideosAdapter()
        }
        spinnerSetUp()
    }

    override fun onResume() {
        super.onResume()
        activity!!.lockUnlockDrawer(true)
    }

    override fun onDestroy() {
        super.onDestroy()
        activity!!.lockUnlockDrawer(false)
    }

    private fun initViews() {
        cameraUtils = CameraUtils(activity!!, this, this)
        loader = CustomLoaderDialog(activity!!)
        setUpToolbar()
        setOnClickListener()

    }

    private fun setUpToolbar() {

        toolbar.ivNotification?.visibility = View.GONE
        toolbar.tvTitle.text =
            activity?.resources?.getString(com.artistfinder.R.string.edit_profile)
        toolbar.ivSearch?.visibility = View.GONE

        // set back icon
        toolbar.ivHome?.visibility = View.VISIBLE
        toolbar.ivHome?.setImageDrawable(getIconFromAttributes(com.artistfinder.R.attr.backIcon))
    }

    private fun setOnClickListener() {
        imgProfile.setOnClickListener(this)
        ivHome?.setOnClickListener(this)
        txtImages?.setOnClickListener(this)
        txtVideos?.setOnClickListener(this)
        btnUpdateNow?.setOnClickListener(this)
        tvArtistName?.setOnClickListener(this)
        btn1.setOnClickListener(this)
        btn2.setOnClickListener(this)
        btn3.setOnClickListener(this)
        btn4.setOnClickListener(this)
        btn5.setOnClickListener(this)
        btn6.setOnClickListener(this)
        btn7.setOnClickListener(this)
        btn8.setOnClickListener(this)
        btn9.setOnClickListener(this)

        // email enable false
        edtUserEmail.isEnabled = false
    }

    override fun onClick(v: View?) {
        when (v!!.id) {
            R.id.ivHome -> {
                activity!!.onBackPressed()
            }

            R.id.imgProfile -> {
                cameraUtils!!.openCameraGallery(true, false)
            }


            R.id.btnUpdateNow -> {
                callForLatLong()
            }

            R.id.tvArtistName -> {
                spArtistType.performClick()
            }
            R.id.btn1 -> {
                setButton1()
            }

            R.id.btn2 -> {
                setButton2()
            }

            R.id.btn3 -> {
                setButton3()
            }

            R.id.btn4 -> {
                setButton4()
            }

            R.id.btn5 -> {
                setButton5()
            }

            R.id.btn6 -> {
                setButton6()
            }
            R.id.btn7 -> {
                setButton7()
            }
            R.id.btn8 -> {
                setButton8()
            }
            R.id.btn9 -> {
                setButton9()
            }

        }
    }

    private fun setPhotosAdapter() {
        if (photosAdapter == null) {
            photosAdapter = PhotosAdapter(activity!!, photosList, this, false, true)
            recyclerViewImages.adapter = photosAdapter
        } else {
            photosAdapter!!.notifyDataSetChanged()
        }
    }

    private fun setVideosAdapter() {
        if (videosAdapter == null) {
            videosAdapter = VideosAdapter(activity!!, videosList, this, false, true)
            recyclerViewVideos.adapter = videosAdapter
        } else {
            videosAdapter!!.notifyDataSetChanged()
        }
    }


    override fun onViewClicked(v: View, obj: Any, position: Int) {
        // Both video and photos adapter listener
        when (v.id) {
            R.id.imageViewAdd -> {
                // cameraUtils!!.openCameraGallery(true, true)
                if (v.tag != null) {
                    if (v.tag == Constant.MEDIA_IMG) {
                        var size: Int
                        var limit = 5
                        if (photosList != null && photosList.size > 0) {
                            size = photosList.size - 1
                            limit = 5 - size
                        }
                        cameraUtils!!.openMultiMedia(
                            true,
                            activity!!.getSelectedThemeIsNightMode(),
                            limit
                        )
                    } else if (v.tag == Constant.MEDIA_VID) {
                        var size: Int
                        var limit = 5
                        if (videosList != null && videosList.size > 0) {
                            size = videosList.size - 1
                            limit = 2 - size
                        }
                        cameraUtils!!.openMultiMedia(
                            false,
                            activity!!.getSelectedThemeIsNightMode(),
                            limit
                        )
                    }
                }
            }

            R.id.imageViewClose -> {


                if (v.tag != null) {
                    if (v.tag == Constant.MEDIA_IMG) {
                        if (photosList.get(position).potfolioID == null){
                            photosList.removeAt(position)
                            setPhotosAdapter()
                        }else {
                            deleteMediaPos = photosList.get(position).potfolioID!!
                            deleteMedialType = Constant.MEDIA_IMG
                            val portID = photosList.get(position).potfolioID
                            val userID = photosList.get(position).userID

                            photosList.removeAt(position)
                            setPhotosAdapter()
                            callForRemove(portID, userID)
                        }
                        val size = photosList.size
                        if (size == 4 && !photosList[3].isAddImageView) {
                            val photos = Photos()
                            photos.isAddImageView = true
                            photosList.add(photos)
                            setPhotosAdapter()
                        }
                    } else {
                        if ( videosList.get(position).potfolioID== null){
                            videosList.removeAt(position)
                            setVideosAdapter()
                        }else {
                            deleteMediaPos = videosList.get(position).potfolioID!!
                            deleteMedialType = Constant.MEDIA_VID
                            val portID = videosList.get(position).potfolioID
                            val userID = videosList.get(position).userID
                            videosList.removeAt(position)
                            setVideosAdapter()
                            callForRemove(portID, userID)
                        }
                        val size = videosList.size
                        if (size == 1 && !videosList[0].isAddImageView) {
                            val video = Video()
                            video.isAddImageView = true
                            videosList.add(video)
                            setPhotosAdapter()
                        }
                    }
                }
            }
        }
    }

    private fun callForRemove(potfolioID: Int?, userId: Int?) {
        val map = HashMap<String, String>()
        map["userID"] = "${userId}"
        map["potfolioID"] = "${potfolioID}"
        activity!!.callAPI(
            Constant.REMOVE_PORTFOLIO,
            activity!!,
            Constant.POST_API,
            map,
            true,
            false,
            Constant.REMOVE_PORTFOLIO,
            null,
            this
        )
    }

    private fun isValidate(): Boolean {
        if (edtName.text.toString().trim().isEmpty()) {
            activity!!.printToastAlert(activity!!, getString(R.string.valid_enter_name))
            return false
        } else if (edtUserName.text.toString().trim().isEmpty()) {
            activity!!.printToastAlert(activity!!, getString(R.string.valid_enter_username))
            return false
        } else if (edtCountry.text.toString().trim().isEmpty()) {
            activity!!.printToastAlert(activity!!, getString(R.string.valid_country))
            return false
        } else if (edtCity.text.toString().trim().isEmpty()) {
            activity!!.printToastAlert(activity!!, getString(R.string.valid_city))
            return false
        } else if (edtAddress.text.toString().trim().isEmpty()) {
            activity!!.printToastAlert(activity!!, getString(R.string.validate_address))
            return false
        } else if (lat == 0.0 && long == 0.0) {
            activity!!.printToastAlert(activity!!, getString(R.string.validate_proper_address))
            return false
        } else if (selectedArtistType.equals("Artist Type")) {
            activity!!.printToastAlert(activity!!, getString(R.string.validate_spinner))
            return false
        } else if (selectedArtistType.equals("Other") && edtOtherArtistType.text.toString().trim().isEmpty()) {

            activity!!.printToastAlert(activity!!, getString(R.string.validate_other_artist))
            return false
        } else if (selectedArtistType == "Other" && !checkTimeSlotIsSelected()) {
            activity!!.printToastAlert(activity!!, getString(R.string.validate_time_slot))
            return false

        } else if (selectedArtistType == "Artist" && artistId == -1) {
            activity!!.printToastAlert(activity!!, getString(R.string.validate_artist))
            return false

        } else if (selectedArtistType == "Artist" && !checkTimeSlotIsSelected()) {
            activity!!.printToastAlert(activity!!, getString(R.string.validate_time_slot))
            return false

        } else if (edtNumber.text.toString().trim().isEmpty()) {
            activity!!.printToastAlert(activity!!, getString(R.string.validate_phone))
            return false
        } else if (edtNumber.text.toString().trim().length < 8 || edtNumber.text.toString().trim().length > 11) {
            activity!!.printToastAlert(activity!!, getString(R.string.validate_valid_phone))
            return false
        }
        return true
    }

    override fun onError(error: String) {
        activity!!.printToastAlert(activity!!, error)
    }

    override fun onSuccess(images: List<ChosenImage>) {
        Logger.print("onSuccess>>", images.size.toString())

        if (!cameraUtils!!.isAllowMultiple()) {
            // from profile image changes

            if (images != null && images.size > 0) {
                selectedImageFile = File(images[0].thumbnailPath)
                //binding.ivProfilePic.setImageURI(Uri.fromFile(selectedImageFile));
                CropImage.activity(Uri.fromFile(selectedImageFile))
                    .setAllowFlipping(false)
                    .setFlipHorizontally(false)
                    .setFlipVertically(false)
                    .setFixAspectRatio(true)
                    .start(activity!!)
            }
        } else {
            // from portfolio image changes

            val tempList = ArrayList<Photos>()

            for (photos: ChosenImage in images) {
                val imagedata = Photos()
                imagedata.potfolioPath = photos.thumbnailPath
                tempList.add(imagedata)
            }

            photosList.addAll(0, tempList)
            setPhotosAdapter()
        }


    }

    override fun onVideoSuccess(videos: List<ChosenVideo>) {
        Logger.print("onVideoSuccess>>", videos!!.size.toString())
    }


    override fun onMultiImageListener(imageList: ArrayList<ImageFile>) {
//        ImageCompressionAsync(activity!!, imageList, onImagePreparedListener = object :
//            ImageCompressionAsync.OnImagePreparedListener {
//            override fun onImageCompressed(arrayList: ArrayList<Photos>) {
//                photosList.addAll(0, arrayList)
//                setPhotosAdapter()
//            }
//
//        }).execute()

        val tempList = ArrayList<Photos>()
        for (photos: ImageFile in imageList) {
            val image = Photos()
            val newFilePath = photos.path
            image.potfolioPath = newFilePath
            tempList.add(image)
        }

        photosList.addAll(0, tempList)
        setPhotosAdapter()
        if (photosList != null && photosList.size == 6) {
            photosList.removeAt(5)
            setPhotosAdapter()
        }
    }

    override fun onMultiVideoListener(videoList: ArrayList<VideoFile>) {
//        VideoCompressionAsync(activity!!, videoList, onVideoPreparedListener = object :
//            VideoCompressionAsync.OnVideoPreparedListener {
//            override fun onVideoCompressed(arrayList: ArrayList<Video>) {
//                videosList.addAll(0, arrayList)
//                setVideosAdapter()
//            }
//
//        }).execute()

        val tempList = ArrayList<Video>()

        for (videoFile: VideoFile in videoList) {
            val video = Video()
            video.potfolioPath = videoFile.path
            video.videoThumbnail = videoFile.path
            tempList.add(video)
        }

        videosList.addAll(0, tempList)
        setVideosAdapter()
        if (videosList != null && videosList.size == 3) {
            videosList.removeAt(2)
            setVideosAdapter()
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        cameraUtils!!.onActivityResult(requestCode, resultCode, data)
        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            val result = CropImage.getActivityResult(data)
            if (resultCode == Activity.RESULT_OK) {
                val resultUri = result.uri
                imgProfile.setImageURI(resultUri, activity!!)
                selectedImageFile = File(resultUri.path!!)
                /*filePath = result.getUri().getPath();
                binding.svImage.setImageURI(resultUri);*/
            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                val error = result.error
            }
        } else if (requestCode == 105) {
            Logger.print(">>GOTa")
            if (data != null && data.hasExtra("id")) {
                artistId = data.getIntExtra("id", -1)
                artistTypeName = data.getStringExtra("name")
                if (artistId == -1 && artistTypeName == "") {
                    if (userInfo != null && userInfo!!.typeName.isNotEmpty()) {
                        setSpinnerData()
                    } else {
                        spArtistType!!.setSelection(size)
                        tvArtistName.text = ""
                        timeSlot.visibility = View.GONE
                    }
                } else {
                    if (data.getStringExtra("name").equals("Other",true)){
                        edtOtherArtistType.visibility = View.VISIBLE
                        rlOtherArtistType.visibility = View.VISIBLE
                        selectedArtistType = data.getStringExtra("name")
                    }else {
                        edtOtherArtistType.visibility = View.GONE
                        rlOtherArtistType.visibility = View.GONE
                    }
                    tvArtistName.text = data.getStringExtra("name")
                    timeSlot.visibility = View.VISIBLE
                }
            }
        }


    }


    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String>,
        grantResults: IntArray
    ) {
        cameraUtils!!.onRequestPermissionsResult(requestCode, permissions, grantResults)

    }

    private fun spinnerSetUp() {
        spArtistType!!.onItemSelectedListener = this
        // remove space
        spArtistType.post { spArtistType.setPadding(0, 0, 0, 0) }

        val stringList: ArrayList<String> = ArrayList()
        stringList.add("Artist")
        stringList.add("Non Artist")
//        stringList.add("Other")
        stringList.add("Artist Type")
        size = stringList.size - 1


        // Create an ArrayAdapter using a simple spinner layout and languages array
        val spAdapter =
            SpinnerArrayAdapter(
                activity,
                R.layout.item_spinner,
                stringList
            )
        spAdapter.setDropDownViewResource(R.layout.spinner_dropdown_item)
        // Set Adapter to Spinner
        spArtistType!!.adapter = spAdapter

        spArtistType!!.setSelection(size)
    }

    @SuppressLint("SetTextI18n")
    private fun setSpinnerData() {
        if (userInfo != null) {
            when {
                userInfo!!.artistType == "" -> {
                    // This means user selected non artist
                    timeSlot.visibility = View.GONE
                    edtOtherArtistType.visibility = View.GONE
                    rlOtherArtistType.visibility = View.GONE
                    selectedArtistType = "Non Artist"
                    tvArtistName.text = selectedArtistType
                }
                userInfo!!.artistType == "10.0" -> {
                    // This means user selected other artist
                    edtOtherArtistType.visibility = View.VISIBLE
                    rlOtherArtistType.visibility = View.VISIBLE
                    edtOtherArtistType.setText(userInfo!!.other)
                    timeSlot.visibility = View.VISIBLE
                    selectedArtistType = "Other"
                    tvArtistName.text = selectedArtistType
                    setSelectedTimeSlot()
                }
                else -> {
                    // This means user selected artist
                    timeSlot.visibility = View.VISIBLE
                    edtOtherArtistType.visibility = View.GONE
                    rlOtherArtistType.visibility = View.GONE
                    selectedArtistType = "Artist"
                    artistId = (userInfo!!.artistType.toDouble()).toInt()
                    tvArtistName.text = userInfo!!.typeName
                    setSelectedTimeSlot()
                }
            }
        }
        if (selectedTimeSlotList.size > 0) {
            for (i in selectedTimeSlotList.indices) {
                setButtonSelection(selectedTimeSlotList[i].trim())
            }
        }
    }

    private fun setSelectedTimeSlot() {
        if (userInfo!!.availableSlot.isNotEmpty()) {
            selectedTimeSlotList =
                userInfo!!.availableSlot.split(",").map { it.trim() } as ArrayList<String>
        }
    }

    class SpinnerArrayAdapter(context: Context?, val resource: Int, val list: ArrayList<String>) :
        ArrayAdapter<String>(context, resource, list) {
        override fun getCount(): Int {
            return list.size - 1
        }
    }

    override fun onNothingSelected(parent: AdapterView<*>?) {

    }

    override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
        selectedArtistType = spArtistType.getItemAtPosition(position).toString()
        tvArtistName.text = selectedArtistType
        clearSelection()

        when (position) {
            0 -> {
                edtOtherArtistType.visibility = View.GONE
                rlOtherArtistType.visibility = View.GONE
                val i = Intent(activity, ArtistTypeActivity::class.java)
                startActivityForResult(i, 105)
            }
//            2 -> {
//                edtOtherArtistType.visibility = View.VISIBLE
//                rlOtherArtistType.visibility = View.VISIBLE
//                timeSlot.visibility = View.VISIBLE
//            }
            else -> {
                timeSlot.visibility = View.GONE
                edtOtherArtistType.visibility = View.GONE
                rlOtherArtistType.visibility = View.GONE
            }
        }
        if (!isFirstTimeClear) {
            isFirstTimeClear = true
            setSpinnerData()
        }
    }

    private fun setButton1() {
        isButton1 = if (isButton1) {
            btn1.setBackgroundResource(R.drawable.border_back)
            false
        } else {
            btn1.setBackgroundResource(R.drawable.bg_button_rectangle)
            true
        }
    }

    private fun setButton2() {
        isButton2 = if (isButton2) {
            btn2.setBackgroundResource(R.drawable.border_back)
            false
        } else {
            btn2.setBackgroundResource(R.drawable.bg_button_rectangle)
            true
        }
    }

    private fun setButton3() {
        isButton3 = if (isButton3) {
            btn3.setBackgroundResource(R.drawable.border_back)
            false
        } else {
            btn3.setBackgroundResource(R.drawable.bg_button_rectangle)
            true
        }
    }

    private fun setButton4() {
        isButton4 = if (isButton4) {
            btn4.setBackgroundResource(R.drawable.border_back)
            false
        } else {
            btn4.setBackgroundResource(R.drawable.bg_button_rectangle)
            true
        }
    }

    private fun setButton5() {
        isButton5 = if (isButton5) {
            btn5.setBackgroundResource(R.drawable.border_back)
            false
        } else {
            btn5.setBackgroundResource(R.drawable.bg_button_rectangle)
            true
        }
    }

    private fun setButton6() {
        isButton6 = if (isButton6) {
            btn6.setBackgroundResource(R.drawable.border_back)
            false
        } else {
            btn6.setBackgroundResource(R.drawable.bg_button_rectangle)
            true
        }
    }

    private fun setButton7() {
        isButton7 = if (isButton7) {
            btn7.setBackgroundResource(R.drawable.border_back)
            false
        } else {
            btn7.setBackgroundResource(R.drawable.bg_button_rectangle)
            true
        }
    }

    private fun setButton8() {
        isButton8 = if (isButton8) {
            btn8.setBackgroundResource(R.drawable.border_back)
            false
        } else {
            btn8.setBackgroundResource(R.drawable.bg_button_rectangle)
            true
        }
    }

    private fun setButton9() {
        isButton9 = if (isButton9) {
            btn9.setBackgroundResource(R.drawable.border_back)
            false
        } else {
            btn9.setBackgroundResource(R.drawable.bg_button_rectangle)
            true
        }
    }


    private fun clearSelection() {
        isButton1 = false
        isButton2 = false
        isButton3 = false
        isButton4 = false
        isButton5 = false
        isButton6 = false
        isButton7 = false
        isButton8 = false
        isButton9 = false
        btn1.setBackgroundResource(R.drawable.border_back)
        btn2.setBackgroundResource(R.drawable.border_back)
        btn3.setBackgroundResource(R.drawable.border_back)
        btn4.setBackgroundResource(R.drawable.border_back)
        btn5.setBackgroundResource(R.drawable.border_back)
        btn6.setBackgroundResource(R.drawable.border_back)
        btn7.setBackgroundResource(R.drawable.border_back)
        btn8.setBackgroundResource(R.drawable.border_back)
        btn9.setBackgroundResource(R.drawable.border_back)

        selectedTimeSlotList.clear()
        selectedTimeSlot = ""
    }

    /**
     * This method set all the value of artist type weather user selected artist or no artist or other
     */
    private fun checkArtistType() {
        when (selectedArtistType) {
            "Artist" -> {
                artistType = artistId.toString()
                otherArtist = ""
                setTimeSlot()
            }
            "Non Artist" -> {
                artistType = ""
                otherArtist = ""
                selectedTimeSlot = ""
            }
            "Other" -> {
                artistType = "10"
                otherArtist = edtOtherArtistType.text.toString().trim()
                setTimeSlot()
            }
        }
    }

    private fun setTimeSlot() {
        selectedTimeSlot = ""
        if (selectedTimeSlotList != null && selectedTimeSlotList.size > 0) {
            val noRepeatedList: ArrayList<String> =
                ArrayList(LinkedHashSet<String>(selectedTimeSlotList))
            for (i in noRepeatedList.indices) {
                if (i == (noRepeatedList.size - 1)) {
                    selectedTimeSlot += noRepeatedList[i]
                } else {
                    selectedTimeSlot = selectedTimeSlot + noRepeatedList[i] + ", "
                }
            }
        }
    }

    private fun checkTimeSlotIsSelected(): Boolean {
        selectedTimeSlotList.clear()

        var check = 0
        if (isButton1) {
            selectedTimeSlotList.add(btn1.text.toString())
            check++
        }

        if (isButton2) {
            selectedTimeSlotList.add(btn2.text.toString())
            check++
        }
        if (isButton3) {
            selectedTimeSlotList.add(btn3.text.toString())
            check++
        }
        if (isButton4) {
            selectedTimeSlotList.add(btn4.text.toString())
            check++
        }
        if (isButton5) {
            selectedTimeSlotList.add(btn5.text.toString())
            check++
        }
        if (isButton6) {
            selectedTimeSlotList.add(btn6.text.toString())
            check++
        }
        if (isButton7) {
            selectedTimeSlotList.add(btn7.text.toString())
            check++
        }
        if (isButton8) {
            selectedTimeSlotList.add(btn8.text.toString())
            check++
        }
        if (isButton9) {
            selectedTimeSlotList.add(btn9.text.toString())
            check++
        }


        return check != 0
    }

    private fun setButtonSelection(selectedText: String) {
        when (selectedText) {
            btn1.text -> {
                setButton1()
            }
            btn2.text -> {
                setButton2()
            }
            btn3.text -> {
                setButton3()
            }
            btn4.text -> {
                setButton4()
            }
            btn5.text -> {
                setButton5()
            }
            btn6.text -> {
                setButton6()
            }
            btn7.text -> {
                setButton7()
            }
            btn8.text -> {
                setButton8()
            }
            btn9.text -> {
                setButton9()
            }

        }
    }

    private fun callForEditProfile() {

        loader!!.show(true)
        val map = HashMap<String, RequestBody>()

        val userId = RequestBody.create(MultipartBody.FORM, userInfo!!.userID.toString())
        val name = RequestBody.create(MultipartBody.FORM, edtName.text.toString().trim())
        val username = RequestBody.create(MultipartBody.FORM, edtUserName.text.toString().trim())
        val gender = RequestBody.create(MultipartBody.FORM, "")
        val country = RequestBody.create(MultipartBody.FORM, edtCountry.text.toString().trim())
        val city = RequestBody.create(MultipartBody.FORM, edtCity.text.toString().trim())
        val address = RequestBody.create(MultipartBody.FORM, edtAddress.text.toString().trim())
        val latlong = RequestBody.create(MultipartBody.FORM, "${lat.toString()},${long.toString()}")
        val phone = RequestBody.create(MultipartBody.FORM, edtNumber.text.toString().trim())


        var body: MultipartBody.Part? = null
        if (selectedImageFile != null) {

            val selectedFile: File? = selectedImageFile
            val ext = Utils!!.getExtensionFromFilePath(selectedFile!!.path)

            System.out.println("file name$ext")

            val reqFile = RequestBody.create(MediaType.parse("image/*"), selectedFile)
            body = MultipartBody.Part.createFormData(
                "profileImg",
                selectedFile.getName(),
                reqFile
            )

        } else {
            body = MultipartBody.Part.createFormData(
                "profileImg",
                ""
            )
        }
        checkArtistType()
        val artistType = RequestBody.create(MultipartBody.FORM, this.artistType)
        val availableSlot = RequestBody.create(MultipartBody.FORM, this.selectedTimeSlot)
        val otherArtist = RequestBody.create(MultipartBody.FORM, this.otherArtist)


        var call: Call<String>? = null

        call = RetroClient2.getClient(Constant.BASE_URL)
            ?.callForEditProfile(
                userID = userId,
                name = name,
                username = username,
                gender = gender,
                country = country,
                city = city,
                address = address,
                latlong = latlong,
                artistType = artistType,
                availableSlot = availableSlot,
                phone = phone,
                files = body!!,
                other = otherArtist,
                headers = activity!!.getHeader()
            )

        call!!.enqueue(object : Callback<String> {
            override fun onResponse(call: Call<String>, response: Response<String>) {
                Logger.print(">>", "response code --->> " + response.code())
                Logger.print(">>", "response bofu --->> " + response.body())
                loader!!.hide()
                try {
                    if (response.code() == 200) {


                        try {
                            val commonResponse =
                                activity?.getCommonResponse<UserInfo>(response.body().toString())
                            val userInfo = commonResponse!!.result
                            val code = commonResponse.success
                            activity?.printToastAlert(activity!!, commonResponse.message)
                            if (code == 1) {

                                SharedPreferenceUtils.getInstance(activity!!)
                                    .setObject(Constant.LOGINUSERDATA, userInfo)
                                activity!!.onBackPressed()
                            }

                        } catch (e: Exception) {
                            e.printStackTrace()
                        }

                    }
                } catch (e: Exception) {
                    e.printStackTrace()
                }

            }

            override fun onFailure(call: Call<String>, t: Throwable) {
                loader!!.hide()
            }
        })
    }


    private fun callForImageUploading(mediaType: String) {

        if (mediaType == Constant.MEDIA_VID) {
            for (i in videosList.indices) {
                if (videosList[i].potfolioPath != null && videosList[i].potfolioPath!!.isNotEmpty() && !(videosList[i].potfolioPath!!.startsWith(
                        "http://"
                    ) || videosList[i].potfolioPath!!.startsWith("https://") || videosList[i].potfolioPath!!.startsWith(
                        "ftp://"
                    ))
                ) {
                    val selectedFile = File(videosList[i].potfolioPath)
                    val file_size = Integer.parseInt((selectedFile.length() / 1024).toString())
                    val fileInMB : Int = file_size/1024
                    if (fileInMB > 20) {
                        Toast.makeText(context, "" + "Video should not be more than 20MB", Toast.LENGTH_SHORT).show()
                        return
                    }
                }
            }
        }
        loader!!.show(true)

        val userId = RequestBody.create(MultipartBody.FORM, userInfo!!.userID.toString())
        val potfolioType = RequestBody.create(MultipartBody.FORM, mediaType)

        val imageList = ArrayList<MultipartBody.Part>()

        if (mediaType == Constant.MEDIA_IMG) {
            for (i in photosList.indices) {
                if (photosList[i].potfolioPath != null && photosList[i].potfolioPath!!.isNotEmpty() &&
                    !((photosList[i].potfolioPath!!.startsWith("http://") || photosList[i].potfolioPath!!.startsWith(
                        "https://"
                    ) || photosList[i].potfolioPath!!.startsWith("ftp://")))
                ) {
                    val selectedFile = File(photosList[i].potfolioPath)
                    val ext = Utils.getExtensionFromFilePath(selectedFile.path)

                    System.out.println("file name$ext")

                    val reqFile = RequestBody.create(MediaType.parse("image/*"), selectedFile)
                    val body = MultipartBody.Part.createFormData(
                        "potfolio[]",
                        selectedFile.name,
                        reqFile
                    )
                    imageList.add(body)
                }
            }
        } else {
            for (i in videosList.indices) {
                if (videosList[i].potfolioPath != null && videosList[i].potfolioPath!!.isNotEmpty() &&
                    !(videosList[i].potfolioPath!!.startsWith("http://")
                            || videosList[i].potfolioPath!!.startsWith("https://")
                            || videosList[i].potfolioPath!!.startsWith("ftp://"))
                ) {
                    val selectedFile = File(videosList[i].potfolioPath)
                    val ext = Utils.getExtensionFromFilePath(selectedFile.path)

                    System.out.println("file name$ext")

                    val reqFile = RequestBody.create(MediaType.parse("video/*"), selectedFile)
                    val body = MultipartBody.Part.createFormData(
                        "potfolio[]",
                        selectedFile.name,
                        reqFile
                    )
                    imageList.add(body)
                }
            }
        }

        if (imageList.size > 0) {
            var call: Call<String>? = null

            call = RetroClient2.getClient(Constant.BASE_URL)
                ?.callForUploadImagesAndVideo(
                    userID = userId,
                    potfolioType = potfolioType,
                    files = imageList,
                    headers = activity!!.getHeader()
                )
            call!!.enqueue(object : Callback<String> {
                override fun onResponse(call: Call<String>, response: Response<String>) {
                    loader!!.hide()
                    try {
                        Logger.print(">>", "response bofu --->> " + response.body())
                        if (response.code() == 200) {
                            try {
                                val commonResponse =
                                    activity?.getCommonResponse<UserInfo>(response.body().toString())
                                val code = commonResponse!!.success
                                activity?.printToastAlert(activity!!, commonResponse.message)
                                if (code == 1) {
                                    if (mediaType == Constant.MEDIA_IMG) {
                                        if (videosList != null && videosList.size > 0) {
                                            callForImageUploading(Constant.MEDIA_VID)
                                        }
                                    } else if (mediaType == Constant.MEDIA_VID) {
                                        callForEditProfile()
                                    }
                                }

                            } catch (e: Exception) {
                                e.printStackTrace()
                            }

                        }
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                }

                override fun onFailure(call: Call<String>, t: Throwable) {
                    loader!!.hide()
                }
            })
        } else {
            if (mediaType == Constant.MEDIA_IMG) {
                callForImageUploading(Constant.MEDIA_VID)
            } else if (mediaType == Constant.MEDIA_VID) {
                callForEditProfile()
            }
        }
    }

    private fun callForLatLong() {
        val locationGeo: GeocodingLocation = GeocodingLocation()
        locationGeo.getAddressFromLocation(
            "${edtAddress.text.toString().trim()},${edtCity.text.trim()},${edtCountry.text.trim()}",
            activity!!,
            Handler() {

                val bundle = it!!.data
                lat = bundle.getDouble("latittude")
                long = bundle.getDouble("longitude")
                Logger.print(">>", lat.toString() + " " + long)

                if (isValidate()) {
                    if (photosList != null && photosList.size > 1 && isFilePicked(Constant.MEDIA_IMG)) {
                        callForImageUploading(Constant.MEDIA_IMG)
                    } else if (videosList != null && videosList.size > 1 && isFilePicked(
                            Constant.MEDIA_VID
                        )
                    ) {
                        callForImageUploading(Constant.MEDIA_VID)
                    } else {
                        callForEditProfile()
                    }
                }

                return@Handler true

            })

    }

    override fun onResponse(
        MethodName: String,
        responseCode: Int,
        mRes: String,
        responseTag: String
    ) {
        if (responseCode == 200) {
            if (deleteMediaPos != -1) {
                if (deleteMedialType == Constant.MEDIA_IMG) {

                    if (userInfo!!.potfolio_image.size > 0) {
                        for (i in userInfo!!.potfolio_image.indices) {
                            if (userInfo!!.potfolio_image[i].potfolioID == deleteMediaPos) {
                                userInfo!!.potfolio_image.removeAt(i)
                                break
                            }
                        }
                    }
                    deleteMedialType = ""
                    deleteMediaPos = -1
                    SharedPreferenceUtils.getInstance(activity!!)
                        .setObject(Constant.LOGINUSERDATA, userInfo)
                } else if (deleteMedialType == Constant.MEDIA_VID) {
                    if (userInfo!!.potfolio_vid.size > 0) {
                        for (i in userInfo!!.potfolio_vid.indices) {
                            if (userInfo!!.potfolio_vid[i].potfolioID == deleteMediaPos) {
                                userInfo!!.potfolio_vid.removeAt(i)
                                break
                            }
                        }
                    }
                    deleteMedialType = ""
                    deleteMediaPos = -1
                    SharedPreferenceUtils.getInstance(activity!!)
                        .setObject(Constant.LOGINUSERDATA, userInfo)
                }
            }
        }
    }

    override fun onRetrofitError(MethodName: String, mError: String, responseTag: Any) {
    }

    private fun isFilePicked(string: String): Boolean {
        if (string == Constant.MEDIA_IMG) {

            for (i in photosList.indices) {
                if (photosList[i].potfolioPath != null && photosList[i].potfolioPath!!.isNotEmpty() && !photosList[i].potfolioPath!!.startsWith(
                        "http://"
                    )
                ) {
                    return true
                }
            }
        } else {
            for (i in videosList.indices) {
                if (videosList[i].potfolioPath != null && videosList[i].potfolioPath!!.isNotEmpty() && !videosList[i].potfolioPath!!.startsWith(
                        "http://"
                    )
                ) {
                    return true
                }
            }
        }
        return false
    }

    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment EditProfileFragment.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance(param1: String) =
            EditProfileFragment().apply {
                arguments = Bundle().apply {
                    putString(ARG_PARAM1, param1)
                }
            }
    }

    class ImageCompressionAsync(
        contextActivity: Context, arrayImageList: ArrayList<ImageFile>,
        onImagePreparedListener: OnImagePreparedListener
    ) :
        AsyncTask<Void, Void, ArrayList<Photos>>() {
        val context = contextActivity
        var imageList = arrayImageList
        var onImagePreparedListener: OnImagePreparedListener = onImagePreparedListener
        val directory =
            getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES).toString()
        var fileImage = File("$directory/Artist/images")

        interface OnImagePreparedListener {
            fun onImageCompressed(arrayList: ArrayList<Photos>)
        }

        override fun onPreExecute() {
            super.onPreExecute()
        }

        override fun doInBackground(vararg params: Void?): ArrayList<Photos> {
            val tempList = ArrayList<Photos>()
//
//            for (photos: ImageFile in imageList) {
//                val image = Photos()
//                val newFilePath = SiliCompressor.with(context).compress(photos.path, File(fileImage.path))
//                image.potfolioPath = newFilePath
//                Logger.print("EditProfile", newFilePath)
//                tempList.add(image)
//            }
            return tempList
        }


        override fun onPostExecute(result: ArrayList<Photos>?) {
            if (result != null) {
                onImagePreparedListener.onImageCompressed(result)
            }
            super.onPostExecute(result)
        }


    }

    class VideoCompressionAsync(
        contextActivity: Context, arrayImageList: ArrayList<VideoFile>,
        onVideoPreparedListener: OnVideoPreparedListener
    ) :
        AsyncTask<Void, Void, ArrayList<Video>>() {
        val context = contextActivity
        var imageList = arrayImageList
        var onVideoPreparedListener: OnVideoPreparedListener = onVideoPreparedListener
        val directory =
            getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES).toString()
        var fileImage = File("$directory/Artist/images")
        val APP_DIR: String = "Artist finder"
        val COMPRESSED_VIDEOS_DIR: String = "/Compressed Videos/"
        val TEMP_DIR: String = "/Temp/"

        interface OnVideoPreparedListener {
            fun onVideoCompressed(arrayList: ArrayList<Video>)
        }

        override fun onPreExecute() {

            super.onPreExecute()
        }

        override fun doInBackground(vararg params: Void?): ArrayList<Video> {
            val tempList = ArrayList<Video>()
            var i = 1
            for (videoFile: VideoFile in imageList) {
                val video = Video()
                var outPath = ""
                i++
                val isCompress = MediaController.getInstance().convertVideo(videoFile.path)
                if (isCompress) {
                    outPath = MediaController.cachedFile.path
                }
                video.potfolioPath = outPath
                video.videoThumbnail = outPath
                tempList.add(video)
            }
            return tempList
        }


        override fun onPostExecute(result: ArrayList<Video>?) {
            if (result != null) {
                onVideoPreparedListener.onVideoCompressed(result)
            }
            super.onPostExecute(result)
        }


    }

}
