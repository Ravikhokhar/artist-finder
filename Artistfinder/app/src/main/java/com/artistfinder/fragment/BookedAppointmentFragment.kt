package com.artistfinder.fragment


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.artistfinder.R
import com.artistfinder.adapter.BookedAppointmentAdapter
import com.artistfinder.baseclass.BaseFragment
import com.artistfinder.global.Constant
import com.artistfinder.listners.OnItemClickListener
import com.artistfinder.model.Artist
import com.artistfinder.model.BookAppointmentListing
import com.artistfinder.model.BookedAppointment
import com.artistfinder.model.UserInfo
import com.artistfinder.retrofitclient.RetroFitResponse
import com.artistfinder.utils.SharedPreferenceUtils
import com.google.gson.Gson
import kotlinx.android.synthetic.main.fragment_artist.toolbar
import kotlinx.android.synthetic.main.fragment_booked_appointment.*
import kotlinx.android.synthetic.main.generic_toolbar.*
import kotlinx.android.synthetic.main.generic_toolbar.view.*
import java.util.*


/**
 * A simple [Fragment] subclass.
 * Use the [BookedAppointmentFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class BookedAppointmentFragment : BaseFragment(), View.OnClickListener, OnItemClickListener,
    RetroFitResponse {


    //    private var bookedAppointmentList: ArrayList<BookedAppointment> = arrayListOf()
    private var bookedAppointmentAdapter: BookedAppointmentAdapter? = null
    private var isPending: Boolean = false
    private var appointmentPendingList: ArrayList<BookedAppointment> = arrayListOf()
    private var appointmentCompletedList: ArrayList<BookedAppointment> = arrayListOf()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_booked_appointment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        initViews()
        super.onViewCreated(view, savedInstanceState)
    }

    private fun initViews() {
        setUpToolbar()
//        setAdapter()
        setBookedAppointmentByStatus()
        callForAppointRequest()
    }

    private fun setUpToolbar() {

        toolbar.ivNotification?.visibility = View.GONE
        toolbar.tvTitle.text = activity?.resources?.getString(R.string.booked_appointment)
        ivSearch?.visibility = View.GONE

        // set back icon
        ivHome?.visibility = View.VISIBLE


        //  click listener
        ivHome?.setOnClickListener(this)
        txtPending?.setOnClickListener(this)
        txtCompleted?.setOnClickListener(this)

    }

    private fun setAdapter(list: ArrayList<BookedAppointment>) {
        if (bookedAppointmentAdapter == null) {
            bookedAppointmentAdapter =
                BookedAppointmentAdapter(activity!!, list, this)
            recyclerAppointment.adapter = bookedAppointmentAdapter
        } else {
            bookedAppointmentAdapter!!.notifyDataSetChanged()
        }
    }

    override fun onClick(v: View?) {
        when (v!!.id) {
            R.id.ivHome -> {
                activity!!.openDrawer()
            }

            R.id.txtPending -> {
                isPending = true
                setBookedAppointmentByStatus()
            }

            R.id.txtCompleted -> {
                isPending = false
                setBookedAppointmentByStatus()
            }
        }
    }

    override fun onViewClicked(v: View, obj: Any, position: Int) {
        activity!!.setFragment(
            ArtistDetailFragment.newInstance((obj as BookedAppointment).artistID.toString()),
            R.id.frContainer,
            true,
            true,true
        )
    }

    private fun callForAppointRequest() {

        val userInfo = SharedPreferenceUtils.getInstance(activity!!)
            .getObject(Constant.LOGINUSERDATA, UserInfo::class.java)

        val map = HashMap<String, String>()
        map["userID"] = "${userInfo!!.userID}"
        activity?.callAPI(
            Constant.GETSENT_APPOINTMENT,
            activity!!,
            Constant.POST_API,
            map,
            true,
            false,
            Constant.GETSENT_APPOINTMENT,
            null,
            this
        )
    }

    override fun onResponse(
        MethodName: String,
        responseCode: Int,
        mRes: String,
        responseTag: String
    ) {

        val commonResponse = Gson().fromJson(mRes, BookAppointmentListing::class.java)
        val code = commonResponse!!.success

        when (responseTag) {
            Constant.GETSENT_APPOINTMENT -> {

                if (code == 1) {

                    val data = commonResponse.result
                    if (data.pending!=null && data.pending.size>0){
                        appointmentPendingList.addAll(data.pending)
                    }
                   if (data.completed!=null &&data.completed.size>0 ) {
                       appointmentCompletedList.addAll(data.completed)
                   }
                    if (appointmentCompletedList.size > 0) {
                        emptyText.visibility = View.GONE
                    } else {
                        emptyText.visibility = View.VISIBLE
                    }

                    activity!!.runOnUiThread {
                        setAdapter(appointmentCompletedList!!)
                    }
                }
            }
        }
    }

    override fun onRetrofitError(MethodName: String, mError: String, responseTag: Any) {
    }


    private fun setBookedAppointmentByStatus() {
        if (isPending) {
            // pending selected
            txtPending.setBackgroundResource((R.drawable.bg_button_rectangle))
            txtCompleted.setBackgroundResource(0)
            txtPending.setTextColor(activity!!.resources.getColor(R.color.darkBackground))
            txtCompleted.setTextColor((getColorFromAttribute(R.attr.titleColor)))
            bookedAppointmentAdapter?.updateData(appointmentPendingList!!);

            if (appointmentPendingList!!.size > 0) {
                emptyText.visibility = View.GONE;
            } else {
                emptyText.visibility = View.VISIBLE;
            }
        } else {
            // completed selected
            txtCompleted.setBackgroundResource((R.drawable.bg_button_rectangle))
            txtPending.setBackgroundResource(0)
            txtCompleted.setTextColor(activity!!.resources.getColor(R.color.darkBackground))
            txtPending.setTextColor(getColorFromAttribute(R.attr.titleColor))
            bookedAppointmentAdapter?.updateData(appointmentCompletedList!!);

            if (appointmentCompletedList!!.size > 0) {
                emptyText.visibility = View.GONE;
            } else {
                emptyText.visibility = View.VISIBLE;
            }
        }
    }


}
