package com.artistfinder.fragment

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.artistfinder.R
import com.artistfinder.adapter.PhotosAdapter
import com.artistfinder.adapter.VideosAdapter
import com.artistfinder.baseclass.BaseFragment
import com.artistfinder.global.Constant
import com.artistfinder.listners.OnItemClickListener
import com.artistfinder.model.Photos
import com.artistfinder.model.UserInfo
import com.artistfinder.model.Video
import com.artistfinder.utils.SharedPreferenceUtils
import kotlinx.android.synthetic.main.fragment_artist.toolbar
import kotlinx.android.synthetic.main.fragment_profile.*
import kotlinx.android.synthetic.main.generic_toolbar.*
import kotlinx.android.synthetic.main.generic_toolbar.view.*
import android.graphics.BitmapFactory
import android.graphics.Bitmap
import android.os.AsyncTask
import android.widget.ImageView
import java.lang.ref.WeakReference
import android.os.Build
import android.media.MediaMetadataRetriever




// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Activities that contain this fragment must implement the
 * [ProfileFragment.OnFragmentInteractionListener] interface
 * to handle interaction events.
 * Use the [ProfileFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class ProfileFragment : BaseFragment(), View.OnClickListener, OnItemClickListener {


    // TODO: Rename and change types of parameters
    private var param1: String? = null
    private var param2: String? = null

    private var photosList: ArrayList<Photos> = arrayListOf()
    private var photosAdapter: PhotosAdapter? = null
    private var userInfo: UserInfo? = null
    private var videosList: ArrayList<Video> = arrayListOf()
    private var videosAdapter: VideosAdapter? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_profile, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        initViews()
        super.onViewCreated(view, savedInstanceState)
        setUI()
    }

    @SuppressLint("SetTextI18n")
    private fun setUI() {
        userInfo = SharedPreferenceUtils.getInstance(activity!!)
            .getObject(Constant.LOGINUSERDATA, UserInfo::class.java)
        if (userInfo != null) {
            imgProfile.setImageURI(userInfo!!.profileImg)
            tvName.text = userInfo!!.name
            tvUserName.text = userInfo!!.username
            tvEmail.text = userInfo!!.email
            tvCountry.text = userInfo!!.country
            tvCity.text = userInfo!!.city
            tvAddress.text = userInfo!!.address
            tvNumber.text = userInfo!!.phone
            tvArtistType.text = userInfo!!.typeName
            if (userInfo!!.typeName == "Other" && userInfo!!.other.isNotEmpty()){
                tvArtistType.text = userInfo!!.other
            }else if (userInfo!!.typeName.isEmpty()){
                tvArtistType.text = "Non Artist"
            }

            if (userInfo!!.potfolio_image != null && userInfo!!.potfolio_image.size > 0) {
                tvNoImages.visibility = View.GONE
                recyclerViewImages.visibility = View.VISIBLE
                photosList.addAll(userInfo!!.potfolio_image)
                setPhotosAdapter()
            }else {
                recyclerViewImages.visibility = View.GONE
                tvNoImages.visibility = View.VISIBLE
            }

            if (userInfo!!.potfolio_vid != null && userInfo!!.potfolio_vid.size > 0) {
                tvNoVideos.visibility = View.GONE
                recyclerViewVideos.visibility = View.VISIBLE
                videosList.addAll(userInfo!!.potfolio_vid)
                setVideosAdapter()
            }else{
                recyclerViewVideos.visibility = View.GONE
                tvNoVideos.visibility = View.VISIBLE

            }
        }
    }


    override fun onResume() {
        super.onResume()
        activity!!.lockUnlockDrawer(true)
    }

    override fun onDestroy() {
        super.onDestroy()
        activity!!.lockUnlockDrawer(false)
    }


    private fun initViews() {

        setUpToolbar()
        setOnClickListener()
        setPhotosAdapter()
        setVideosAdapter()
    }

    private fun setUpToolbar() {

        toolbar.ivNotification?.visibility = View.GONE
        toolbar.tvTitle.text = activity?.resources?.getString(R.string.profile)
        toolbar.ivSearch?.visibility = View.GONE

        // set back icon
        toolbar.ivHome?.visibility = View.VISIBLE
        toolbar.ivHome?.setImageDrawable(getIconFromAttributes(R.attr.backIcon))
    }

    private fun setOnClickListener() {
        ivHome?.setOnClickListener(this)
        btnEdit?.setOnClickListener(this)
    }

    override fun onClick(v: View?) {
        when (v!!.id) {
            R.id.ivHome -> {
                activity!!.onBackPressed()
            }
            R.id.btnEdit -> {
                activity!!.setFragment(
                    EditProfileFragment.newInstance(""),
                    R.id.frContainer,
                    true,
                    true
                    , true
                )
            }
        }
    }

    private fun setPhotosAdapter() {
        if (photosAdapter == null) {
            photosAdapter = PhotosAdapter(activity!!, photosList, this, true,false)
            recyclerViewImages.adapter = photosAdapter
        } else {
            photosAdapter!!.notifyDataSetChanged()
        }
    }

    private fun setVideosAdapter() {
        if (videosAdapter == null) {
            videosAdapter = VideosAdapter(activity!!, videosList, this, true,false)
            recyclerViewVideos.adapter = videosAdapter
        } else {
            videosAdapter!!.notifyDataSetChanged()
        }
    }

    override fun onViewClicked(v: View, obj: Any, position: Int) {
        // Both video and photos adapter listener
    }

    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment ProfileFragment.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
            ProfileFragment().apply {
                arguments = Bundle().apply {
                    putString(ARG_PARAM1, param1)
                    putString(ARG_PARAM2, param2)
                }
            }
    }

    override fun onHiddenChanged(hidden: Boolean) {
        if (!hidden){
            photosList.clear()
            videosList.clear()
            setPhotosAdapter()
            setVideosAdapter()
            setUI()
        }
        super.onHiddenChanged(hidden)
    }


}
