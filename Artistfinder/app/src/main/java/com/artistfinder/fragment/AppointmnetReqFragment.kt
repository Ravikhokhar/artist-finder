package com.artistfinder.fragment

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.artistfinder.R
import com.artistfinder.activity.HomeActivity
import com.artistfinder.adapter.AppointMentReqAdapter
import com.artistfinder.baseclass.BaseFragment
import com.artistfinder.customview.CalendarView
import com.artistfinder.global.Constant
import com.artistfinder.model.AppointmentRequest
import com.artistfinder.model.BookedAppointment
import com.artistfinder.model.UserInfo
import com.artistfinder.retrofitclient.RetroFitResponse
import com.artistfinder.utils.SharedPreferenceUtils
import com.artistfinder.utils.Utils
import com.google.gson.Gson
import kotlinx.android.synthetic.main.fragment_apointmnet_req.*
import kotlinx.android.synthetic.main.generic_toolbar.*
import kotlinx.android.synthetic.main.generic_toolbar.view.*
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

private var adapter: AppointMentReqAdapter? = null

class ApointmnetReqFragment : BaseFragment(), View.OnClickListener, CalendarView.EventHandler,
    RetroFitResponse {


    // TODO: Rename and change types of parameters
    private var selectDate: String? = null
    private var appointmentList: ArrayList<BookedAppointment> = arrayListOf()
    private var isFromNoti: Boolean = false
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            isFromNoti = it.getBoolean(ARG_PARAM1)
            selectDate = it.getString(ARG_PARAM2)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_apointmnet_req, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setToolBar()
//        setAdapter()
        calander.setEventHandler(this)
        if(isFromNoti){
            val format = SimpleDateFormat(Constant.FORMAT4)
            try {
                val date = format.parse(selectDate)
                calander.setDate(date)
                callForAppointList(date)
            } catch (e: ParseException) {
                e.printStackTrace()
            }

        }else{
            callForAppointList(Date())
        }


    }

    private fun setToolBar() {
        toolbar.ivHome.setOnClickListener(this)
        if (isFromNoti) {
            ivHome?.setImageDrawable(getIconFromAttributes(R.attr.backIcon))
        }
        toolbar.ivSearch.visibility = View.GONE
        toolbar.ivNotification.visibility = View.GONE
        toolbar.tvTitle.text = "Appointment Request"
    }

    private fun setAdapter() {
//        if (adapter == null) {
            adapter = AppointMentReqAdapter(activity!!, appointmentList!!)
            reyAppointMent.adapter = adapter
//        } else {
//            adapter!!.notifyDataSetChanged()
//        }
        if (appointmentList!!.size > 0) {
            emptyText.visibility = View.GONE;
        } else {
            emptyText.visibility = View.VISIBLE;
        }
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        activity = (context) as HomeActivity
    }

    override fun onClick(v: View?) {
        when (v!!.id) {
            R.id.ivHome -> {
                if (isFromNoti) {
                    activity!!.onBackPressed()
                } else {
                    activity!!.openDrawer()
                }
            }
        }
    }

    override fun onDayLongPress(date: Date) {

    }

    override fun onDaySelectListener(date: Date) {
        appointmentList!!.clear()
        callForAppointList(date)
    }

    private fun callForAppointList(date: Date) {

        val userInfo = SharedPreferenceUtils.getInstance(activity!!)
            .getObject(Constant.LOGINUSERDATA, UserInfo::class.java)

        val map = HashMap<String, String>()
        map["artistID"] = "${userInfo!!.userID}"
        map["date"] = Utils.getDateFromTimeStamp(Constant.FORMAT4, date.time)
        activity?.callAPI(
            Constant.GETAPPOINTMENT,
            activity!!,
            Constant.POST_API,
            map,
            true,
            false,
            Constant.GETAPPOINTMENT,
            null,
            this
        )
    }

    override fun onResponse(
        MethodName: String,
        responseCode: Int,
        mRes: String,
        responseTag: String
    ) {

        val commonResponse = Gson().fromJson(mRes, AppointmentRequest::class.java)
        val code = commonResponse!!.success

        when (responseTag) {
            Constant.GETAPPOINTMENT -> {

                if (code == 1) {

                    val data = commonResponse!!.result;

                    activity!!.runOnUiThread {
                        appointmentList!!.addAll(data);
                        setAdapter()
                    }
                }
            }
        }

    }

    override fun onRetrofitError(MethodName: String, mError: String, responseTag: Any) {
    }

    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment ApointmnetReqFragment.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance(param1: Boolean, param2: String) =
            ApointmnetReqFragment().apply {
                arguments = Bundle().apply {
                    putBoolean(ARG_PARAM1, param1)
                    putString(ARG_PARAM2, param2)
                }
            }
    }
}
