package com.artistfinder.fragment

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import androidx.recyclerview.widget.GridLayoutManager
import com.artistfinder.R
import com.artistfinder.activity.HomeActivity
import com.artistfinder.adapter.SlotAdapter
import com.artistfinder.baseclass.BaseFragment
import com.artistfinder.customview.CalendarView
import com.artistfinder.global.Constant
import com.artistfinder.listners.OnItemClickListener
import com.artistfinder.model.ArtistDetail
import com.artistfinder.model.UserInfo
import com.artistfinder.retrofitclient.RetroFitResponse
import com.artistfinder.utils.Logger
import com.artistfinder.utils.SharedPreferenceUtils
import com.artistfinder.utils.Utils
import kotlinx.android.synthetic.main.fragment_booking.*
import kotlinx.android.synthetic.main.generic_toolbar.view.*
import org.json.JSONArray
import org.json.JSONObject
import java.text.SimpleDateFormat
import java.util.*

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val USERID = "param1"
private const val ARTISTTYPE = "param2"

class Booking : BaseFragment(), View.OnClickListener, CalendarView.EventHandler,
    OnItemClickListener, RetroFitResponse {

    private var data: JSONArray? = null
    // TODO: Rename and change types of parameters
//    private var userId: String? = null
    private var artistType: String? = null
    var selectedDate: String? = null
    private var isButton1 = false
    private var isButton2 = false
    private var isButton3 = false
    private var isButton4 = false
    private var isButton5 = false
    private var isButton6 = false
    private var isButton7 = false
    private var isButton8 = false
    private var isButton9 = false
    var selectedTimeSlot: String = ""
    private var dataArtist: ArtistDetail? = null
    private var adapter: SlotAdapter? = null
    private var artistTimeSlotList: ArrayList<String> = ArrayList()
    private var userInfo: UserInfo? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            dataArtist = it.getParcelable(USERID)
        }
        setTimeSlot()
    }

    private fun setTimeSlot() {
        if (dataArtist!=null && dataArtist!!.availableSlot!=null &&  dataArtist!!.availableSlot!!.isNotEmpty()) {
            artistTimeSlotList =
                dataArtist!!.availableSlot!!.split(",").map { it.trim() } as ArrayList<String>
        }
    }

    private fun setUI() {
        tvName.setText(dataArtist?.name)
        tvAddress.setText(dataArtist?.address)
        imgProfile.setImageURI(dataArtist?.profileImg)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_booking, container, false)
    }

    @SuppressLint("SimpleDateFormat")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        userInfo = SharedPreferenceUtils.getInstance(activity!!)
            .getObject(Constant.LOGINUSERDATA, UserInfo::class.java)
        setToolBar()
        setListner()
        setUI()

        selectedDate =Utils.getDateFromTimeStamp(com.artistfinder.global.Constant.FORMAT4, Date().time)
        Logger.print("today>>",selectedDate.toString())
        callForFreeSlot()


    }

    override fun onViewClicked(v: View, obj: Any, position: Int) {
        when (v.id) {
            R.id.tvSlot -> {
                selectedTimeSlot = obj as String
            }
        }
    }


    private fun setListner() {
        calander.setEventHandler(this)
//        btn1.setOnClickListener(this)
//        btn2.setOnClickListener(this)
//        btn3.setOnClickListener(this)
//        btn4.setOnClickListener(this)
//        btn5.setOnClickListener(this)
//        btn6.setOnClickListener(this)
//        btn7.setOnClickListener(this)
//        btn8.setOnClickListener(this)
//        btn9.setOnClickListener(this)
        btnDone.setOnClickListener(this)
    }

    override fun onClick(v: View?) {
        when (v!!.id) {
            R.id.ivHome -> {
                activity!!.onBackPressed()

            }
            R.id.btnDone -> {
                callForBooking()
            }

            /* R.id.btn1 -> {
                 // btn1.isActivated = !btn1.isActivated
                 setButton1()
             }

             R.id.btn2 -> {
                 //btn2.isActivated = !btn2.isActivated
                 setButton2()
             }

             R.id.btn3 -> {
                 // btn3.isActivated = !btn3.isActivated
                 setButton3()
             }

             R.id.btn4 -> {
                 //btn4.isActivated = !btn4.isActivated
                 setButton4()
             }

             R.id.btn5 -> {
                 //btn5.isActivated = !btn5.isActivated
                 setButton5()
             }

             R.id.btn6 -> {
                 // btn6.isActivated = !btn6.isActivated
                 setButton6()
             }
             R.id.btn7 -> {
                 //btn6.isActivated = !btn6.isActivated
                 setButton7()
             }
             R.id.btn8 -> {
                 // btn6.isActivated = !btn6.isActivated
                 setButton8()
             }
             R.id.btn9 -> {
                 //btn6.isActivated = !btn6.isActivated
                 setButton9()
             }*/
        }
    }

    /* private fun setButton1() {
         isButton1 = if (isButton1) {
             btn1.setBackgroundResource(R.drawable.border_back)
             false
         } else {
             btn1.setBackgroundResource(R.drawable.bg_button_rectangle)
             true
         }
     }

     private fun setButton2() {
         isButton2 = if (isButton2) {
             btn2.setBackgroundResource(R.drawable.border_back)
             false
         } else {
             btn2.setBackgroundResource(R.drawable.bg_button_rectangle)
             true
         }
     }

     private fun setButton3() {
         isButton3 = if (isButton3) {
             btn3.setBackgroundResource(R.drawable.border_back)
             false
         } else {
             btn3.setBackgroundResource(R.drawable.bg_button_rectangle)
             true
         }
     }

     private fun setButton4() {
         isButton4 = if (isButton4) {
             btn4.setBackgroundResource(R.drawable.border_back)
             false
         } else {
             btn4.setBackgroundResource(R.drawable.bg_button_rectangle)
             true
         }
     }

     private fun setButton5() {
         isButton5 = if (isButton5) {
             btn5.setBackgroundResource(R.drawable.border_back)
             false
         } else {
             btn5.setBackgroundResource(R.drawable.bg_button_rectangle)
             true
         }
     }

     private fun setButton6() {
         isButton6 = if (isButton6) {
             btn6.setBackgroundResource(R.drawable.border_back)
             false
         } else {
             btn6.setBackgroundResource(R.drawable.bg_button_rectangle)
             true
         }
     }

     private fun setButton7() {
         isButton7 = if (isButton7) {
             btn7.setBackgroundResource(R.drawable.border_back)
             false
         } else {
             btn7.setBackgroundResource(R.drawable.bg_button_rectangle)
             true
         }
     }

     private fun setButton8() {
         isButton8 = if (isButton8) {
             btn8.setBackgroundResource(R.drawable.border_back)
             false
         } else {
             btn8.setBackgroundResource(R.drawable.bg_button_rectangle)
             true
         }
     }

     private fun setButton9() {
         isButton9 = if (isButton9) {
             btn9.setBackgroundResource(R.drawable.border_back)
             false
         } else {
             btn9.setBackgroundResource(R.drawable.bg_button_rectangle)
             true
         }
     }

     private fun checkTimeSlotIsSelected(): Boolean {
         var check = 0
         if (isButton1) {
             selectedTimeSlotList.add(btn1.text.toString())
             check++
         }

         if (isButton2) {
             selectedTimeSlotList.add(btn2.text.toString())
             check++
         }
         if (isButton3) {
             selectedTimeSlotList.add(btn3.text.toString())
             check++
         }
         if (isButton4) {
             selectedTimeSlotList.add(btn4.text.toString())
             check++
         }
         if (isButton5) {
             selectedTimeSlotList.add(btn5.text.toString())
             check++
         }
         if (isButton6) {
             selectedTimeSlotList.add(btn6.text.toString())
             check++
         }
         if (isButton7) {
             selectedTimeSlotList.add(btn7.text.toString())
             check++
         }
         if (isButton8) {
             selectedTimeSlotList.add(btn8.text.toString())
             check++
         }
         if (isButton9) {
             selectedTimeSlotList.add(btn9.text.toString())
             check++
         }


         return check != 0
     }

     private fun setTimeSlot(){
         selectedTimeSlot = ""
         if (selectedTimeSlotList != null && selectedTimeSlotList.size > 0) {
             val noRepeatedList: ArrayList<String> = ArrayList(LinkedHashSet<String>(selectedTimeSlotList))
             for (i in noRepeatedList.indices) {
                 if (i == (noRepeatedList.size - 1)) {
                     selectedTimeSlot += noRepeatedList[i]
                 } else {
                     selectedTimeSlot = selectedTimeSlot + noRepeatedList[i] + ", "
                 }
             }
         }
     }*/
    private fun setToolBar() {
        toolbar.ivHome.setOnClickListener(this)
        toolbar.ivSearch.visibility = View.GONE
        toolbar.ivNotification.visibility = View.GONE
        toolbar.tvTitle.text = "Booking"
        toolbar.ivHome?.setImageDrawable(getIconFromAttributes(R.attr.backIcon))
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        activity = (context) as HomeActivity
    }

    private fun callForBooking() {

        /*if (!checkTimeSlotIsSelected()) {
            activity!!.printToastAlert(activity!!, getString(R.string.validate_time_slot))
            return;
        };
*/
//        setTimeSlot()

        if (adapter != null && adapter!!.selPos == -1) {
            selectedTimeSlot = ""
        }


        if (selectedTimeSlot.equals("")) {
            activity!!.printToastAlert(activity!!, getString(R.string.validate_time_slot))
            return;
        };

        val map = HashMap<String, String>()
        map["userID"] = userInfo!!.userID.toString()
        map["artistID"] = dataArtist!!.userID.toString()
        map["date"] = selectedDate.toString()
        map["time"] = selectedTimeSlot
        map["notes"] = txtMessage.text.toString()
        activity?.callAPI(
            com.artistfinder.global.Constant.BOOK_APOINTMENT,
            activity!!,
            com.artistfinder.global.Constant.POST_API,
            map,
            true,
            false,
            com.artistfinder.global.Constant.BOOK_APOINTMENT,
            null,
            this
        )
    }

    override fun onResponse(
        MethodName: String,
        responseCode: Int,
        mRes: String,
        responseTag: String
    ) {
        try {
            when (responseTag) {
                Constant.GET_FREE_SHOT -> {
                    val respose = JSONObject(mRes)
                    if (respose.has("result")) {
                        data = respose.getJSONArray("result")
                    }else{
                        data = JSONArray(artistTimeSlotList)
                    }
                    setAdapter()
                }

                Constant.BOOK_APOINTMENT -> {
                    val commonResponse = activity!!.getCommonResponse<UserInfo>(mRes)
                    val code = commonResponse!!.success
                    if (code == 1) {
                        activity!!.printToastAlert(activity!!, commonResponse.message)
                        LocalBroadcastManager.getInstance(activity!!)
                            .sendBroadcast(Intent(Constant.APPOINTMENT_BOOKED))
                        activity!!.onBackPressed()
                    }
                }
            }


        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    private fun setAdapter() {
        if (data != null && data?.length()!! > 0) {
            adapter = SlotAdapter(activity!!, data!!, this)
            reySlot.layoutManager = GridLayoutManager(activity, 3)
            reySlot.adapter = adapter

            tvNoData.visibility = View.GONE
            group.visibility = View.VISIBLE
        } else {
            tvNoData.visibility = View.VISIBLE
            group.visibility = View.INVISIBLE
        }


    }

    override fun onRetrofitError(MethodName: String, mError: String, responseTag: Any) {
    }


    override fun onDayLongPress(date: Date) {

    }

    override fun onDaySelectListener(date: Date) {

 /*       if(date.compareTo(Calendar.getInstance().time) < 0){
            Toast.makeText(context,"Please select today or future date", Toast.LENGTH_SHORT).show()
            return
        }else if()
*/

        selectedDate =
            Utils.getDateFromTimeStamp(com.artistfinder.global.Constant.FORMAT4, date.time)
        Logger.print(
            ">>",
            Utils.getDateFromTimeStamp(com.artistfinder.global.Constant.FORMAT4, date.time)
        )

        selectedTimeSlot = ""
        data = null
        callForFreeSlot()
    }

    private fun callForFreeSlot() {
        val map = HashMap<String, String>()
        map["artistID"] = dataArtist!!.userID.toString()
        map["date"] = selectedDate.toString()
        activity?.callAPI(
            com.artistfinder.global.Constant.GET_FREE_SHOT,
            activity!!,
            com.artistfinder.global.Constant.POST_API,
            map,
            true,
            false,
            com.artistfinder.global.Constant.GET_FREE_SHOT,
            null,
            this
        )
    }


    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment Booking.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance(param1: ArtistDetail, param2: String) =
            Booking().apply {
                arguments = Bundle().apply {
                    putParcelable(USERID, param1)
                }
            }
    }
}
