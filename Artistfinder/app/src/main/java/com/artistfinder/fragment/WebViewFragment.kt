package com.artistfinder.fragment

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.webkit.WebView
import android.webkit.WebViewClient
import androidx.fragment.app.Fragment
import com.artistfinder.R
import com.artistfinder.baseclass.BaseFragment
import com.artistfinder.global.Constant
import com.artistfinder.utils.SharedPreferenceUtils
import kotlinx.android.synthetic.main.fragment_web_view.*
import kotlinx.android.synthetic.main.generic_toolbar.*
import kotlinx.android.synthetic.main.generic_toolbar.view.*

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Activities that contain this fragment must implement the
 * [WebViewFragment.OnFragmentInteractionListener] interface
 * to handle interaction events.
 * Use the [WebViewFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class WebViewFragment : BaseFragment(), View.OnClickListener {


    // TODO: Rename and change types of parameters
    private var param1: String? = null
    private var param2: String? = null
    private var isFor: Int = 0
    private var url = ""
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            isFor = it.getInt(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_web_view, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initView(view)
    }

    private fun initView(view: View) {
        setUpToolbar()
        setOnClickListener()

        url = param2.toString()

        val webView = view.findViewById<WebView>(R.id.webView)
        webView.settings.javaScriptEnabled = true
        webView.settings.loadWithOverviewMode = true
        webView.webViewClient = object : WebViewClient() {
            override fun shouldOverrideUrlLoading(view: WebView?, url: String?): Boolean {
                view?.loadUrl(url)
                return true
            }
        }
        webView.loadUrl(url)
    }

    private fun setOnClickListener() {
        ivHome?.setOnClickListener(this)
    }

    override fun onClick(v: View?) {
        when (v!!.id) {
            R.id.ivHome -> {
                activity!!.onBackPressed()
            }
        }
    }

    private fun setUpToolbar() {

        toolbar.ivNotification?.visibility = View.GONE
        if (isFor == 0) {
            toolbar.tvTitle.text = activity?.resources?.getString(R.string.privacy_policy)
        } else {
            toolbar.tvTitle.text = activity?.resources?.getString(R.string.terms_condition)

        }
        toolbar.ivSearch?.visibility = View.GONE

        // set back icon
        toolbar.ivHome?.visibility = View.VISIBLE
        toolbar.ivHome?.setImageDrawable(getIconFromAttributes(R.attr.backIcon))
    }


    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment WebViewFragment.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance(param1: Int,param2: String) =
            WebViewFragment().apply {
                arguments = Bundle().apply {
                    putInt(ARG_PARAM1, param1)
                    putString(ARG_PARAM2, param2)
                }
            }
    }

    override fun onResume() {
        super.onResume()
        activity!!.lockUnlockDrawer(true)
    }

    override fun onDestroy() {
        super.onDestroy()
        activity!!.lockUnlockDrawer(false)
    }
}
