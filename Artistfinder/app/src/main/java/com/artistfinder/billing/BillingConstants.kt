/*
 * Copyright 2017 Google Inc. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.artistfinder.billing

import java.util.Arrays

/**
 * Static fields and methods useful for billing
 */
object BillingConstants {
    // SKUs for our products: (consumable)
    val SKU_MONTHLY = "com.artistfinder.plandemo"



    private val IN_APP_SKUS = arrayOf(
        SKU_MONTHLY
    )

    /**
     * Returns the list of all SKUs for the billing type specified
     */
    val skuList: List<String>
        get() = Arrays.asList(*IN_APP_SKUS)
}

