package com.artistfinder.model

data class BookAppointmentListing (

    val success : Int,
    val message : String,
    val result : Result
)

data class Result (

    val completed : ArrayList<BookedAppointment>,
    val pending : ArrayList<BookedAppointment>
)