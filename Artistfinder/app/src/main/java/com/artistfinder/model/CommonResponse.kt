package com.artistfinder.model

data class CommonResponse<T>(
    val message: String,
    val result: T,
    val success: Int
)

data class UserInfo(
    val accessToken: String,
    val address: String,
    val artistType: String,
    val other: String,
    val availableSlot: String,
    val city: String,
    val country: String,
    val email: String,
    val gender: String,
    val latlong: String,
    val name: String,
    val phone: String,
    val profileImg: String,
    val typeName: String,
    val userID: Int,
    val is_subscribed: Int,
    val username: String,
    val potfolio_image: ArrayList<Photos>,
    val potfolio_vid: ArrayList<Video>,
    val orderID: String,
    val productType: String,
    val purchaseToken: String,
    val exp_date: String,
    val start_date: String
)

data class SubscriptionInfo(
    val accessToken: String,
    val address: String,
    val artistType: String,
    val other: String,
    val availableSlot: String,
    val city: String,
    val country: String,
    val email: String,
    val gender: String,
    val latlong: String,
    val name: String,
    val phone: String,
    val profileImg: String,
    val typeName: String,
    val userID: Int,
    val is_subscribed: Int,
    val username: String,
    val potfolio_image: ArrayList<Photos>,
    val potfolio_vid: ArrayList<Video>,
    val orderID: String,
    val productType: String,
    val purchaseToken: String,
    val exp_date: String,
    val start_date: String
)