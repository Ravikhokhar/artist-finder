package com.artistfinder.model

class Video{
    val potfolioID: Int? = null
    var videoThumbnail: String? = null
    var potfolioPath: String? = ""
    var potfolioType: String? = null
    var userID: Int? = null
    var isAddImageView : Boolean = false
}