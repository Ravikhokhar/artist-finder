package com.artistfinder.model

class BookedAppointment {
    var appointmentID: Int ?= null
    var artistID: Int ?= null
    var userID: Int ?= null
    var status: Int ?= null
    var date: String? ?= null
    var time: String? ?= null
    var notes: String? ?= null
    var profileImg: String? ?= null
    var name: String? ?= null
}