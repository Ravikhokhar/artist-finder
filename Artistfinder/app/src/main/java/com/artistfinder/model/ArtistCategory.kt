package com.artistfinder.model

class ArtistCategory() {
    var typeID: Int ?= null
    var typeName: String? ?= null
    var typeImg: String? ?= null
}