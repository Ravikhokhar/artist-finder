package com.artistfinder.model

import com.artistfinder.R
import java.util.ArrayList


class DrawerMenu {

    var iconId: Int = 0
    var iconIdSelected: Int = 0
    var name: String? = null
    var id: Int = 0
    var isSelected: Boolean = false

    companion object {

        val userMenu: ArrayList<DrawerMenu>
            get() {
                val list = ArrayList<DrawerMenu>()

                var drawerMenu = DrawerMenu()
                drawerMenu.id = 0
                drawerMenu.iconId = R.drawable.category_icon
                drawerMenu.iconIdSelected = R.drawable.category_icon
                drawerMenu.name = "Dashboard"
                list.add(drawerMenu)

                drawerMenu = DrawerMenu()
                drawerMenu.id = 1
                drawerMenu.iconId = R.drawable.caleder_icon
                drawerMenu.iconIdSelected = R.drawable.caleder_icon
                drawerMenu.name = "Booked Appointment"
                list.add(drawerMenu)

                drawerMenu = DrawerMenu()
                drawerMenu.id = 2
                drawerMenu.iconId = R.drawable.calander_dark
                drawerMenu.iconIdSelected = R.drawable.calander_dark
                drawerMenu.name = "Appointment Request"
                list.add(drawerMenu)

                drawerMenu = DrawerMenu()
                drawerMenu.id = 3
                drawerMenu.iconId = R.drawable.notification
                drawerMenu.iconIdSelected = R.drawable.notification
                drawerMenu.name = "Notification"
                list.add(drawerMenu)

                drawerMenu = DrawerMenu()
                drawerMenu.id = 4
                drawerMenu.iconId = R.drawable.setting_icon
                drawerMenu.iconIdSelected = R.drawable.setting_icon
                drawerMenu.name = "Settings"
                list.add(drawerMenu)

                drawerMenu = DrawerMenu()
                drawerMenu.id = 5
                drawerMenu.iconId = R.drawable.logout
                drawerMenu.iconIdSelected = R.drawable.logout
                drawerMenu.name = "Logout"
                list.add(drawerMenu)


                return list
            }


        val artistMenu: ArrayList<DrawerMenu>
            get() {
                val list = ArrayList<DrawerMenu>()

                var drawerMenu = DrawerMenu()
                drawerMenu.id = 0
                drawerMenu.iconId = R.drawable.category_icon
                drawerMenu.iconIdSelected = R.drawable.category_icon
                drawerMenu.name = "Dashboard"
                list.add(drawerMenu)

                drawerMenu = DrawerMenu()
                drawerMenu.id = 1
                drawerMenu.iconId = R.attr.bookedAppoint
                drawerMenu.iconIdSelected = R.attr.bookedAppoint
                drawerMenu.name = "Appointment Request"
                list.add(drawerMenu)

                drawerMenu = DrawerMenu()
                drawerMenu.id = 2
                drawerMenu.iconId = R.drawable.notification
                drawerMenu.iconIdSelected = R.drawable.notification
                drawerMenu.name = "Notification"
                list.add(drawerMenu)

                drawerMenu = DrawerMenu()
                drawerMenu.id = 3
                drawerMenu.iconId = R.drawable.setting_icon
                drawerMenu.iconIdSelected = R.drawable.setting_icon
                drawerMenu.name = "Settings"
                list.add(drawerMenu)

                drawerMenu = DrawerMenu()
                drawerMenu.id = 4
                drawerMenu.iconId = R.drawable.logout
                drawerMenu.iconIdSelected = R.drawable.logout
                drawerMenu.name = "Logout"
                list.add(drawerMenu)

                return list
            }
    }
}