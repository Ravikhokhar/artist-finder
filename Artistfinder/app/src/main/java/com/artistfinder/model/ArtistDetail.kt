package com.artistfinder.model

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName

class ArtistDetail() : Parcelable {

    @SerializedName("userID") val userID : Int?= null
    @SerializedName("email") val email : String?= null
    @SerializedName("username") val username : String?= null
    @SerializedName("name") val name : String?= null
    @SerializedName("profileImg") val profileImg : String?= null
    @SerializedName("gender") val gender : String?= null
    @SerializedName("city") val city : String?= null
    @SerializedName("country") val country : String?= null
    @SerializedName("phone") val phone : String?= null
    @SerializedName("address") val address : String?= null
    @SerializedName("latlong") val latlong : String?= null
    @SerializedName("artistType") val artistType : Int?= null
    @SerializedName("typeName") val typeName : String?= null
    @SerializedName("availableSlot") val availableSlot : String?=  null
    @SerializedName("potfolio_image") val potfolio_image : ArrayList<Photos>?= null
    @SerializedName("potfolio_vid") val potfolio_vid : ArrayList<Video>?= null

    constructor(parcel: Parcel) : this() {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {

    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<ArtistDetail> {
        override fun createFromParcel(parcel: Parcel): ArtistDetail {
            return ArtistDetail(parcel)
        }

        override fun newArray(size: Int): Array<ArtistDetail?> {
            return arrayOfNulls(size)
        }
    }
}

class ArtistDetailCommonResp() : Parcelable {
    @SerializedName("success") val success : Int?= null
    @SerializedName("message") val message : String?= null
    @SerializedName("result") val result : ArtistDetail?= null

    constructor(parcel: Parcel) : this() {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {

    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<ArtistDetailCommonResp> {
        override fun createFromParcel(parcel: Parcel): ArtistDetailCommonResp {
            return ArtistDetailCommonResp(parcel)
        }

        override fun newArray(size: Int): Array<ArtistDetailCommonResp?> {
            return arrayOfNulls(size)
        }
    }
}

