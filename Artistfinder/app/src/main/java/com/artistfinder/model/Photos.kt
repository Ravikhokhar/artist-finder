package com.artistfinder.model

class Photos {
    val potfolioID: Int? = null
    var potfolioPath: String? = ""
    var potfolioType: String? = null
    var userID: Int? = null
    var isAddImageView : Boolean = false
}