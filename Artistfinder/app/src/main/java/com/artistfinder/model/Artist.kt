package com.artistfinder.model


class Artist {

    val userID: Int? = null
    val email: String? = null
    val username: String? = null
    val name: String? = null
    val profileImg: String? = null
    val gender: String? = null
    val city: String? = null
    val country: String? = null
    val phone: String? = null
    val address: String? = null
    val latlong: String? = null
    val artistType: Int? = null

}