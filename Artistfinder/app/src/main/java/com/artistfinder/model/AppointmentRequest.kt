package com.artistfinder.model

data class AppointmentRequest (

    val success : Int,
    val message : String,
    val result : List<BookedAppointment>
)