package com.artistfinder.model

class Notification() {
    var appointmentID: Int ?= null
    var artistID: Int ?= null
    var userID: Int ?= null
    var status: Int ?= null
    var date: String = ""
    var time: String = ""
    var notes: String = ""
    var name: String = ""
    var profileImg: String = ""
    var crtDate: String = ""
}